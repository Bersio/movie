<?php

namespace app\controllers;

use app\messages\Localization;
use app\models\Audio;
use app\models\Country;
use app\models\Drink;
use app\models\form\LoginForm;
use app\models\Genre;
use app\models\Movie;
use app\models\Person;
use app\models\SpecialShowing;
use app\models\Theater;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Response;

class SiteController extends \yii\web\Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout', 'prepare-sort-counts', 'prepare-average-ratings'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['get', 'post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionPrepareSortCounts()
    {
        Genre::prepareSortCounts();
        Country::prepareSortCounts();
        Person::prepareSortCounts();
        Movie::prepareSortCounts();
        Audio::prepareSortCounts();
        Drink::prepareSortCounts();
        Theater::prepareSortCounts();
        SpecialShowing::prepareSortCounts();

        Yii::$app->session->setFlash('success', 'Сортировка успешно обновлена');

        return $this->goHome();
    }

    public function actionPrepareAverageRatings(?int $id = null)
    {
        Movie::prepareAverageRatings($id);

        Yii::$app->session->setFlash('success', 'Усредненный рейтинг успешно обновлен');

        return $this->goHome();
    }

    public function actionSetLocale(string $locale = null)
    {
        Localization::setLocale($locale);

        return $this->goHome();
    }
}
