<?php

namespace app\controllers;

use Exception;
use Yii;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use app\models\Genre;
use app\models\Movie;
use app\widgets\Menu;

class GenreController extends \yii\web\Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['create', 'edit', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        Menu::setActive(Menu::MENU_GENRE);

        return parent::beforeAction($action);
    }

    public function actionIndex(string $sort = null)
    {
        if (Yii::$app->request->isPost) {
            $url = Url::to([
                '/genre/index',
                'sort' => Yii::$app->request->post('sort'),
            ]);
            return $this->redirect($url)->send();
        }

        $genre_query = Genre::find()->prepareSorting($sort);

        $count_query = clone $genre_query;
        $genre_total_count = $count_query->count();

        return $this->render('index', [
            'genre_query' => $genre_query,
            'genre_total_count' => $genre_total_count,
            'sort' => $sort,
            'sort_list' => Genre::sortList(),
        ]);
    }

    public function actionDetails(int $id, int $is_favorite = 0, string $sort = null)
    {
        $model = Genre::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Genre by id: $id");
        }

        if (Yii::$app->request->isPost) {
            $url = Url::to([
                '/genre/details',
                'id' => $model->id,
                'is_favorite' => Yii::$app->request->post('is_favorite', 0),
                'sort' => Yii::$app->request->post('sort'),
            ]);
            return $this->redirect($url)->send();
        }

        $movie_query = Movie::find()
            ->innerJoin('movie_genre', 'movie_genre.movie_id = movie.id')
            ->with(['views'])
            ->where(['movie_genre.genre_id' => $model->id])
            ->groupBy(['movie.id'])
            ->prepareSorting($sort, Movie::SORT_TYPE_MOVIE);

        if ($is_favorite) {
            $movie_query->andWhere(['movie.is_favorite' => true]);
        }

        $count_query = clone $movie_query;
        $movie_total_count = $count_query->count();

        return $this->render('details', [
            'model' => $model,
            'movie_query' => $movie_query,
            'movie_total_count' => $movie_total_count,
            'is_favorite' => $is_favorite,
            'sort' => $sort,
            'sort_list' => Movie::sortList(),
        ]);
    }

    public function actionCreate()
    {
        $model = new Genre();

        if (Yii::$app->request->isPost) {
            $load_result = $model->load(Yii::$app->request->post());
            if (!$load_result) {
                throw new Exception('Failed to load Genre');
            }

            $save_result = $model->save();
            if (!$save_result) {
                throw new Exception('Failed to save Genre: ' . $model->getErrorsAsString());
            }

            Yii::$app->session->setFlash('success', 'Жанр успешно сохранен');

            return $this->redirect('/genre/index');
        }

        return $this->render('create', [
            'model' => $model
        ]);
    }

    public function actionEdit(int $id)
    {
        $model = Genre::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Genre by id: $id");
        }

        if (Yii::$app->request->isPost) {
            $load_result = $model->load(Yii::$app->request->post());
            if (!$load_result) {
                throw new Exception('Failed to load Genre');
            }

            $save_result = $model->save();
            if (!$save_result) {
                throw new Exception('Failed to save Genre: ' . $model->getErrorsAsString());
            }

            Yii::$app->session->setFlash('success', 'Жанр успешно сохранен');

            return $this->redirect('/genre/details?id=' . $model->id);
        }

        return $this->render('edit', [
            'model' => $model
        ]);
    }

    public function actionDelete(int $id)
    {
        $model = Genre::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Genre by id: $id");
        }

        $model->delete();

        Yii::$app->session->setFlash('success', 'Жанр успешно удален');

        return $this->redirect('/genre/index');
    }
}
