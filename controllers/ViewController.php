<?php

namespace app\controllers;

use DateInterval;
use DateTime;
use Exception;
use Yii;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use app\models\Audio;
use app\models\Award;
use app\models\Drink;
use app\models\Genre;
use app\models\Movie;
use app\models\Theater;
use app\models\SpecialShowing;
use app\models\Queue;
use app\models\View;
use app\widgets\Menu;

class ViewController extends \yii\web\Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['create', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        Menu::setActive(Menu::MENU_VIEW);

        return parent::beforeAction($action);
    }

    public function actionIndex(
        int $genre_id = 0,
        int $award_id = 0,
        int $theater_id = 0,
        int $special_showing_id = 0,
        int $is_favorite = 0,
        string $sort = null
    ) {
        if (Yii::$app->request->isPost) {
            $url = Url::to([
                '/view/index',
                'genre_id' => Yii::$app->request->post('genre_id', 0),
                'award_id' => Yii::$app->request->post('award_id', 0),
                'theater_id' => Yii::$app->request->post('theater_id', 0),
                'special_showing_id' => Yii::$app->request->post('special_showing_id', 0),
                'is_favorite' => Yii::$app->request->post('is_favorite', 0),
                'sort' => Yii::$app->request->post('sort'),
            ]);
            return $this->redirect($url)->send();
        }

        $view_query = View::find()
            ->innerJoin('movie', 'view.movie_id = movie.id')
            ->with(['movie'])
            ->where(['>', 'movie.id', 0]);

        if ($genre_id > 0) {
            $view_query
                ->innerJoin('movie_genre', 'movie.id = movie_genre.movie_id')
                ->andWhere(['movie_genre.genre_id' => $genre_id]);
        }

        if ($award_id > 0) {
            $view_query
                ->innerJoin('movie_award', 'movie.id = movie_award.movie_id')
                ->andWhere(['movie_award.award_id' => $award_id]);
        }

        if ($theater_id > 0) {
            $view_query
                ->innerJoin('theater', 'view.theater_id = theater.id')
                ->andWhere(['view.theater_id' => $theater_id]);
        } elseif ($theater_id === -1) {
            $view_query->andWhere(['IS NOT', 'view.theater_id', null]);
        }

        if ($special_showing_id > 0) {
            $view_query
                ->innerJoin('special_showing', 'view.special_showing_id = special_showing.id')
                ->andWhere(['view.special_showing_id' => $special_showing_id]);
        } elseif ($special_showing_id === -1) {
            $view_query->andWhere(['IS NOT', 'view.special_showing_id', null]);
        }

        if ($is_favorite > 0) {
            $view_query->andWhere(['movie.is_favorite' => true]);
        }

        $view_query->groupBy(['view.id']);
        $view_query->prepareSorting($sort, Movie::SORT_TYPE_VIEW);

        $count_query = clone $view_query;
        $view_total_count = $count_query->count();

        return $this->render('index', [
            'view_query' => $view_query,
            'view_total_count' => $view_total_count,
            'genre_id' => $genre_id,
            'genre_list' => Genre::getGenresForViewsFilters(),
            'award_id' => $award_id,
            'award_list' => Award::getAwardsForViewsFilters(),
            'theater_id' => $theater_id,
            'theater_list' => Theater::getTheatersForViewsFilters(),
            'special_showing_id' => $special_showing_id,
            'special_showing_list' => SpecialShowing::getSpecialShowingForViewsFilters(),
            'is_favorite' => $is_favorite,
            'sort' => $sort,
            'sort_list' => View::sortList(),
        ]);
    }

    public function actionCreate()
    {
        $model = new View();
        $model->created_at = (new DateTime())->sub(new DateInterval('PT3H'))->format('Y-m-d');

        if (Yii::$app->request->isPost) {
            $load_result = $model->load(Yii::$app->request->post());
            if (!$load_result) {
                throw new Exception('Failed to load View');
            }

            $view_save_result = $model->save();
            if (!$view_save_result) {
                throw new Exception('Failed to save View: ' . $model->getErrorsAsString());
            }

            $model->movie->prepareAverageRating();

            Queue::deleteAll(['movie_id' => $model->movie_id]);

            Yii::$app->session->setFlash('success', 'Просмотр успешно сохранен');

            return $this->redirect('/prepare-sort-counts');
        }

        $rating_list = [];
        for ($i = 1; $i <= 10; $i++) {
            $rating_list[$i] = $i;
        }
        unset($i);

        return $this->render('create', [
            'model' => $model,
            'movie_list' => Movie::getAllMoviesForFilters(),
            'audio_list' => Audio::getAllAudioForFilters(),
            'drink_list' => Drink::getAllDrinksForFilters(),
            'theater_list' => Theater::getAllTheatersForFilters(),
            'special_showing_list' => SpecialShowing::getAllSpecialShowingForFilters(),
            'rating_list' => $rating_list
        ]);
    }

    public function actionDelete(int $id)
    {
        $model = View::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get View by id: $id");
        }

        $model->delete();

        $model->movie->prepareAverageRating();

        Yii::$app->session->setFlash('success', 'Просмотр успешно удален');

        // TODO: prepare-sort-counts

        return $this->redirect('/movie/edit?id=' . $model->movie_id . '#view');
    }
}
