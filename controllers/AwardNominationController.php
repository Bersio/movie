<?php

namespace app\controllers;

use Exception;
use Yii;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use app\models\Award;
use app\models\AwardNomination;
use app\models\MovieAward;
use app\models\PersonAward;
use app\widgets\Menu;

class AwardNominationController extends \yii\web\Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['create', 'edit', 'delete', 'delete-movie-award', 'delete-person-award'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action): bool
    {
        Menu::setActive(Menu::MENU_AWARD);

        return parent::beforeAction($action);
    }

    public function actionCreate(int $id)
    {
        $award = Award::findOne($id);
        if (empty($award)) {
            throw new NotFoundHttpException("Failed to get Award by id: $id");
        }

        $model = new AwardNomination();
        $model->award_id = $award->id;

        if (Yii::$app->request->isPost) {
            $load_result = $model->load(Yii::$app->request->post());
            if (!$load_result) {
                throw new Exception('Failed to load Award Nomination');
            }

            $save_result = $model->save();
            if (!$save_result) {
                throw new Exception('Failed to save Award Nomination: ' . $model->getErrorsAsString());
            }

            Yii::$app->session->setFlash('success', 'Номинация успешно сохранена');

            return $this->redirect('/award/details?id=' . $award->id);
        }

        return $this->render('create', [
            'model' => $model,
            'type_list' => AwardNomination::typesList(),
        ]);
    }

    public function actionEdit(int $id)
    {
        $model = AwardNomination::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Award Nomination by id: $id");
        }

        if (Yii::$app->request->isPost) {
            $load_result = $model->load(Yii::$app->request->post());
            if (!$load_result) {
                throw new Exception('Failed to load Award Nomination');
            }

            $save_result = $model->save();
            if (!$save_result) {
                throw new Exception('Failed to save Award Nomination: ' . $model->getErrorsAsString());
            }

            Yii::$app->session->setFlash('success', 'Номинация успешно сохранена');

            return $this->redirect('/award-nomination/details?id=' . $model->id);
        }

        return $this->render('edit', [
            'model' => $model,
            'years_list' => $model->award->getYearsList(),
            'type_list' => AwardNomination::typesList(),
        ]);
    }

    public function actionDetails(int $id, int $year = 0, int $only_winner = 0, int $only_without_view = 0)
    {
        if (Yii::$app->request->isPost) {
            $url = Url::to([
                '/award-nomination/details',
                'id' => $id,
                'year' => Yii::$app->request->post('year', 0),
                'only_winner' => Yii::$app->request->post('only_winner', 0),
                'only_without_view' => Yii::$app->request->post('only_without_view', 0),
            ]);
            return $this->redirect($url)->send();
        }

        $model = AwardNomination::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Award Nomination by id: $id");
        }

        $years_list = $model->award->getYearsList();

        if ($year === 0) {
            // год не выбран, заполняем все доступные
            $selected_years_list = $years_list;
        } else {
            // год выбран
            $selected_years_list[$year] = $year;
        }

        return $this->render('details', [
            'model' => $model,
            'year' => $year,
            'years_list' => $years_list,
            'selected_years_list' => $selected_years_list,
            'only_winner' => $only_winner,
            'only_without_view' => $only_without_view,
        ]);
    }

    public function actionDelete(int $id)
    {
        $award_nomination = AwardNomination::findOne($id);
        if (empty($award_nomination)) {
            throw new NotFoundHttpException("Failed to get Award Nomination by id: $id");
        }

        $award_nomination->delete();

        Yii::$app->session->setFlash('success', 'Номинация успешно удалена');

        return $this->redirect('/award/details?id=' . $award_nomination->award_id);
    }

    public function actionDeleteMovieAward(int $movie_award_id)
    {
        $movie_award = MovieAward::findOne($movie_award_id);
        if (empty($movie_award)) {
            throw new NotFoundHttpException("Failed to get Movie Award by id: $movie_award_id");
        }

        $movie_award->delete();

        Yii::$app->session->setFlash('success', 'Награда фильма успешно удалена');

        return $this->redirect('/award-nomination/edit?id=' . $movie_award->award_nomination_id);
    }

    public function actionDeletePersonAward(int $person_award_id)
    {
        $person_award = PersonAward::findOne($person_award_id);
        if (empty($person_award)) {
            throw new NotFoundHttpException("Failed to get Person Award by id: $person_award_id");
        }

        $person_award->delete();

        Yii::$app->session->setFlash('success', 'Награда персоны успешно удалена');

        return $this->redirect('/award-nomination/edit?id=' . $person_award->award_nomination_id);
    }
}
