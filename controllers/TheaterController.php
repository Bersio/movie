<?php

namespace app\controllers;

use Exception;
use Yii;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use app\models\Movie;
use app\models\Theater;
use app\models\View;
use app\widgets\Menu;

class TheaterController extends \yii\web\Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['create', 'edit', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        Menu::setActive(Menu::MENU_THEATER);

        return parent::beforeAction($action);
    }

    public function actionIndex(string $sort = null)
    {
        if (Yii::$app->request->isPost) {
            $url = Url::to([
                '/theater/index',
                'sort' => Yii::$app->request->post('sort'),
            ]);
            return $this->redirect($url)->send();
        }

        $theater_query = Theater::find()->prepareSorting($sort);

        $count_query = clone $theater_query;
        $theater_total_count = $count_query->count();

        return $this->render('index', [
            'theater_query' => $theater_query,
            'theater_total_count' => $theater_total_count,
            'sort' => $sort,
        ]);
    }

    public function actionDetails(int $id, int $is_favorite = 0, string $sort = null)
    {
        $model = Theater::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Theater by id: $id");
        }

        if (Yii::$app->request->isPost) {
            $url = Url::to([
                '/theater/details',
                'id' => $model->id,
                'is_favorite' => Yii::$app->request->post('is_favorite', 0),
                'sort' => Yii::$app->request->post('sort'),
            ]);
            return $this->redirect($url)->send();
        }

        $view_query = View::find()
            ->innerJoin('movie', 'view.movie_id = movie.id')
            ->with(['movie'])
            ->where(['view.theater_id' => $model->id])
            ->groupBy(['view.id'])
            ->prepareSorting($sort, Movie::SORT_TYPE_VIEW);

        if ($is_favorite) {
            $view_query->andWhere(['movie.is_favorite' => true]);
        }

        return $this->render('details', [
            'model' => $model,
            'view_query' => $view_query,
            'is_favorite' => $is_favorite,
            'sort' => $sort,
        ]);
    }

    public function actionCreate()
    {
        $model = new Theater();

        if (Yii::$app->request->isPost) {
            $load_result = $model->load(Yii::$app->request->post());
            if (!$load_result) {
                throw new Exception('Failed to load Theater');
            }

            $save_result = $model->save();
            if (!$save_result) {
                throw new Exception('Failed to save Theater: ' . $model->getErrorsAsString());
            }

            Yii::$app->session->setFlash('success', 'Кинотеатр успешно сохранен');

            return $this->redirect('/theater/index');
        }

        return $this->render('create', [
            'model' => $model
        ]);
    }

    public function actionEdit(int $id)
    {
        $model = Theater::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Theater by id: $id");
        }

        if (Yii::$app->request->isPost) {
            $load_result = $model->load(Yii::$app->request->post());
            if (!$load_result) {
                throw new Exception('Failed to load Theater');
            }

            $save_result = $model->save();
            if (!$save_result) {
                throw new Exception('Failed to save Theater: ' . $model->getErrorsAsString());
            }

            Yii::$app->session->setFlash('success', 'Кинотеатр успешно сохранен');

            return $this->redirect('/theater/details?id=' . $model->id);
        }

        return $this->render('edit', [
            'model' => $model
        ]);
    }

    public function actionDelete(int $id)
    {
        $model = Theater::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Theater by id: $id");
        }

        $model->delete();

        Yii::$app->session->setFlash('success', 'Кинотеатр успешно удален');

        return $this->redirect('/theater/index');
    }
}
