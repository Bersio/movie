<?php

namespace app\controllers;

use DateInterval;
use DateTime;
use Yii;
use app\models\Calendar;
use app\models\View;
use app\widgets\Menu;

class CalendarController extends \yii\web\Controller
{
    public function beforeAction($action): bool
    {
        Menu::setActive(Menu::MENU_CALENDAR);

        return parent::beforeAction($action);
    }

    public function actionIndex(int $year = null, string $month = null): string
    {
        if (!empty($year) && $year > 0 && !empty($month) && $month > 0) {
            $date_now = new DateTime($year . '-' . $month);
            $date_prew_m = new DateTime($year . '-' . $month);
            $date_prew_m->sub(new DateInterval('P1M'));
            $date_next_m = new DateTime($year . '-' . $month);
            $date_next_m->add(new DateInterval('P1M'));
        } else {
            $date_now = new DateTime();
            $date_prew_m = new DateTime();
            $date_prew_m->sub(new DateInterval('P1M'));
            $date_next_m = new DateTime();
            $date_next_m->add(new DateInterval('P1M'));
        }

        $first_day_of_month_date = new DateTime($date_now->format('Y-m-01'));

        $first_day_of_next_month_date = new DateTime($date_now->format('Y-m-01'));
        $first_day_of_next_month_date->add(new DateInterval('P1M'));
        $viewed_movies_count = View::getViewsCountByPeriod(
            $first_day_of_month_date->format('Y-m-d'),
            $first_day_of_next_month_date->format('Y-m-d')
        );

        return $this->render('index', [
            'date_now' => $date_now,
            'first_day_of_month_number' => intval($first_day_of_month_date->format('N')),
            'days_in_month_count' => intval($date_now->format('t')),
            'previous_month_date' => $date_prew_m,
            'next_month_date' => $date_next_m,
            'viewed_movies_count' => $viewed_movies_count
        ]);
    }

    public function actionYear(int $year = null): string
    {
        if (!empty($year) && $year > 0) {
            $date_now = new DateTime($year . '-01-01');
            $date_prew_y = new DateTime($year . '-01-01');
            $date_prew_y->sub(new DateInterval('P1Y'));
            $date_next_y = new DateTime($year . '-01-01');
            $date_next_y->add(new DateInterval('P1Y'));
        } else {
            $date_now = new DateTime();
            $date_prew_y = new DateTime();
            $date_prew_y->sub(new DateInterval('P1Y'));
            $date_next_y = new DateTime();
            $date_next_y->add(new DateInterval('P1Y'));
        }

        $calendar = new Calendar(['year' => $date_now->format('Y')]);

        return $this->render('year', [
            'date_now' => $date_now,
            'previous_year_date' => $date_prew_y,
            'next_year_date' => $date_next_y,
            'calendar' => $calendar,
        ]);
    }
}
