<?php

namespace app\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use app\models\Country;
use app\models\Movie;
use app\models\View;
use app\widgets\Menu;

class CountryController extends \yii\web\Controller
{
    public function beforeAction($action)
    {
        Menu::setActive(Menu::MENU_COUNTRY);

        return parent::beforeAction($action);
    }

    public function actionIndex(string $sort = null)
    {
        if (Yii::$app->request->isPost) {
            $url = Url::to([
                '/country/index',
                'sort' => Yii::$app->request->post('sort'),
            ]);
            return $this->redirect($url)->send();
        }

        $country_query = Country::find()
            ->innerJoin('movie_country', 'country.id = movie_country.country_id')
            ->groupBy(['country.id'])
            ->prepareSorting($sort);

        $count_query = clone $country_query;
        $country_total_count = $count_query->count();

        return $this->render('index', [
            'country_query' => $country_query,
            'country_total_count' => $country_total_count,
            'sort' => $sort,
            'sort_list' => Country::sortList(),
        ]);
    }

    public function actionDetails(int $id, int $is_favorite = 0, string $sort = null)
    {
        $model = Country::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Country by id: $id");
        }

        if (Yii::$app->request->isPost) {
            $url = Url::to([
                '/country/details',
                'id' => $model->id,
                'is_favorite' => Yii::$app->request->post('is_favorite', 0),
                'sort' => Yii::$app->request->post('sort'),
            ]);
            return $this->redirect($url)->send();
        }

        $movie_query = Movie::find()
            ->innerJoin('movie_country', 'movie.id = movie_country.movie_id')
            ->with(['views'])
            ->where(['movie_country.country_id' => $model->id])
            ->groupBy(['movie.id'])
            ->prepareSorting($sort, Movie::SORT_TYPE_MOVIE);

        if ($is_favorite) {
            $movie_query->andWhere(['movie.is_favorite' => true]);
        }

        return $this->render('details', [
            'model' => $model,
            'movie_query' => $movie_query,
            'is_favorite' => $is_favorite,
            'sort' => $sort,
            'sort_list' => Movie::sortList(),
        ]);
    }
}
