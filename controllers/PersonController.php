<?php

namespace app\controllers;

use app\models\MovieActor;
use app\models\MovieDirector;
use Exception;
use Yii;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use app\models\Award;
use app\models\Comment;
use app\models\Movie;
use app\models\Person;
use app\models\PersonAward;
use app\models\image\ImageUploadForm;
use app\widgets\Menu;

class PersonController extends \yii\web\Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['create', 'edit', 'delete', 'update-image', 'set-sex', 'award-without-movie'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        Menu::setActive(Menu::MENU_PERSON);

        return parent::beforeAction($action);
    }

    public function actionIndex(
        int $award_id = 0,
        string $type = null,
        string $sex = null, 
        int $viewed_movies = 0,
        string $sort = null
    )
    {
        if (Yii::$app->request->isPost) {
            $url = Url::to([
                '/person/index',
                'type' => Yii::$app->request->post('type'),
                'award_id' => Yii::$app->request->post('award_id', 0),
                'sex' => Yii::$app->request->post('sex'),
                'viewed_movies' => Yii::$app->request->post('viewed_movies', 0),
                'sort' => Yii::$app->request->post('sort'),
            ]);
            return $this->redirect($url)->send();
        }

        $person_query = Person::find()
                ->where(['>', 'person.id', 0]);

        if (!empty($type)) {
            if ($type === 'actors') {
                $person_query
                    ->innerJoin('movie_actor', 'person.id = movie_actor.person_id')
                    ->andWhere(['is not', 'movie_actor.person_id', null]);
            } elseif ($type === 'directors') {
                $person_query
                    ->innerJoin('movie_director', 'person.id = movie_director.person_id')
                    ->andWhere(['is not', 'movie_director.person_id', null]);
            }
        }

        if ($award_id > 0) {
            $person_query
                ->innerJoin('person_award', 'person.id = person_award.person_id')
                ->andWhere(['person_award.award_id' => $award_id]);
        }

        if (!empty($sex)) {
            if ($sex === 'male') {
                $person_query->andWhere(['person.sex' => Person::SEX_MALE]);
            } elseif ($sex === 'female') {
                $person_query->andWhere(['person.sex' => Person::SEX_FEMALE]);
            }
        }

        if ($viewed_movies == 1) {
            $person_query->andWhere(['>', 'person.viewed_movies_count', 0]);
        }

        $person_query->groupBy(['person.id']);
        $person_query->prepareSorting($sort);

        $count_query = clone $person_query;
        $person_total_count = $count_query->count();

        return $this->render('index', [
            'person_query' => $person_query,
            'person_total_count' => $person_total_count,
            'type' => $type,
            'type_list' => Person::typeList(),
            'award_id' => $award_id,
            'award_list' => Award::getAwardsForPersonFilters(),
            'viewed_movies' => $viewed_movies,
            'sex' => $sex,
            'sex_list' => Person::sexList(),
            'sort' => $sort,
            'sort_list' => Person::sortList(),
        ]);
    }

    public function actionDetails(int $id)
    {
        $model = Person::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Person by id: $id");
        }

        $movie_actor_query = Movie::find()
            ->innerJoin('movie_actor', 'movie.id = movie_actor.movie_id')
            ->where(['movie_actor.person_id' => $model->id])
            ->groupBy(['movie.id'])
            ->sortMoviesByDefault();

        $movie_director_query = Movie::find()
            ->innerJoin('movie_director', 'movie.id = movie_director.movie_id')
            ->where(['movie_director.person_id' => $model->id])
            ->groupBy(['movie.id'])
            ->sortMoviesByDefault();

        $comment_query = Comment::find()
            ->where(['person_id' => $model->id])
            ->orderBy(['created_at' => SORT_DESC]);

        $person_award_query = PersonAward::find()
            ->with(['award', 'movie', 'awardNomination'])
            ->where(['person_id' => $model->id])
            ->orderBy(['id' => SORT_ASC]);

        return $this->render('details', [
            'model' => $model,
            'movie_actor_query' => $movie_actor_query,
            'movie_director_query' => $movie_director_query,
            'comment_query' => $comment_query,
            'person_award_query' => $person_award_query
        ]);
    }

    public function actionCreate()
    {
        $model = new Person();
        $ImageUploadForm = new ImageUploadForm();

        if (Yii::$app->request->isPost) {
            $load_result = $model->load(Yii::$app->request->post());
            if (!$load_result) {
                throw new Exception('Failed to load Person');
            }

            // saving for id
            if (!$model->save()) {
                throw new Exception('Failed to save Person: ' . $model->getErrorsAsString());
            }

            if (!$ImageUploadForm->load(Yii::$app->request->post())) {
                throw new Exception('Failed to load image form');
            }

            $model->image = $ImageUploadForm->upload($model);

            // another saving
            if (!$model->save()) {
                throw new Exception('Failed to save Person: ' . $model->getErrorsAsString());
            }

            Yii::$app->session->setFlash('success', 'Персона успешно сохранена');

            return $this->redirect('/person/edit?id=' . $model->id);
        }

        $sex_list = [
            Person::SEX_FEMALE => 'Женский',
            Person::SEX_MALE => 'Мужской',
            Person::SEX_OTHER => 'Другое',
        ];

        return $this->render('create', [
            'model' => $model,
            'sex_list' => $sex_list,
            'ImageUploadForm' => $ImageUploadForm,
        ]);
    }

    public function actionEdit(int $id)
    {
        $model = Person::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Person by id: $id");
        }

        if (Yii::$app->request->isPost) {
            $load_result = $model->load(Yii::$app->request->post());
            if (!$load_result) {
                throw new Exception('Failed to load Person');
            }

            $save_result = $model->save();
            if (!$save_result) {
                throw new Exception('Failed to save Person: ' . $model->getErrorsAsString());
            }

            Yii::$app->session->setFlash('success', 'Персона успешно сохранена');

            return $this->redirect('/person/details?id=' . $model->id);
        }

        $sex_list = [
            Person::SEX_FEMALE => 'Женский',
            Person::SEX_MALE => 'Мужской',
            Person::SEX_OTHER => 'Другое',
        ];

        return $this->render('edit', [
            'model' => $model,
            'sex_list' => $sex_list,
            'ImageUploadForm' => new ImageUploadForm(),
        ]);
    }

    public function actionDelete(int $id)
    {
        $person = Person::findOne($id);
        if (empty($person)) {
            throw new NotFoundHttpException("Failed to get Person by id: $id");
        }

        $person->deleteImageFolder();

        $person->delete();

        Yii::$app->session->setFlash('success', 'Персона успешно удалена');

        return $this->redirect('/person/index');
    }

    public function actionUpdateImage(int $id)
    {
        $model = Person::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Person by id: $id");
        }

        $ImageUploadForm = new ImageUploadForm();

        if (Yii::$app->request->isPost) {
            if (!empty($model->image)) {
                $old_image_path = Yii::getAlias('@webroot') . '/images/person/' . $model->image;

                if (file_exists($old_image_path)) {
                    unlink($old_image_path);
                }
            }

            if (!$ImageUploadForm->load(Yii::$app->request->post())) {
                throw new Exception('Failed to load image form');
            }

            $model->image = $ImageUploadForm->upload($model);

            if (!$model->save()) {
                throw new Exception('Failed to save Person: ' . $model->getErrorsAsString());
            }

            Yii::$app->session->setFlash('success', 'Картинка персоны успешно обновлена');
        }

        return $this->redirect('/person/edit?id=' . $model->id);
    }

    public function actionSetSex(int $id = 0)
    {
        if ($id <= 0) {
            // select random person

            $person_list = Person::find()
                ->where(['is', 'sex', null])
                ->orderBy(['created_at' => SORT_DESC])
                ->all();
            if (empty($person_list)) {
                return $this->redirect('/');
            }

            $random_index = array_rand($person_list);

            return $this->redirect('/person/set-sex?id=' . $person_list[$random_index]->id);
        }

        $model = Person::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Person by id: $id");
        }

        if (Yii::$app->request->isPost) {
            if (!empty(Yii::$app->request->post('male'))) {
                $model->sex = Person::SEX_MALE;
            } elseif (!empty(Yii::$app->request->post('female'))) {
                $model->sex = Person::SEX_FEMALE;
            } else {
                throw new Exception('Failed to get person sex');
            }

            $save_result = $model->save();
            if (!$save_result) {
                throw new Exception('Failed to save Person: ' . $model->getErrorsAsString());
            }

            return $this->redirect('/person/set-sex');
        }

        return $this->render('set-sex', [
            'model' => $model,
        ]);
    }

    public function actionAwardWithoutMovie(
        int $award_id = 0,
        string $type = null,
        string $sex = null, 
        int $viewed_movies = 0,
        string $sort = null
    )
    {
        if (Yii::$app->request->isPost) {
            $url = Url::to([
                '/person/award-without-movie',
                'type' => Yii::$app->request->post('type'),
                'award_id' => Yii::$app->request->post('award_id', 0),
                'sex' => Yii::$app->request->post('sex'),
                'viewed_movies' => Yii::$app->request->post('viewed_movies', 0),
                'sort' => Yii::$app->request->post('sort'),
            ]);
            return $this->redirect($url)->send();
        }

        $person_id_list = [];

        foreach (PersonAward::find()->each() as $person_award_item) {
            $movie_actor = MovieActor::find()
                ->where(['person_id' => $person_award_item->person_id])
                ->andWhere(['movie_id' => $person_award_item->movie_id])
                ->one();
            $movie_director = MovieDirector::find()
                ->where(['person_id' => $person_award_item->person_id])
                ->andWhere(['movie_id' => $person_award_item->movie_id])
                ->one();
            if (empty($movie_actor) && empty($movie_director)) {
                $person_id_list[] = $person_award_item->person_id;
            }
        }
        unset($person_award_item, $movie_actor, $movie_director);

        $person_query = Person::find()
            ->where(['person.id' => $person_id_list]);

        if (!empty($type)) {
            if ($type === 'actors') {
                $person_query
                    ->innerJoin('movie_actor', 'person.id = movie_actor.person_id')
                    ->andWhere(['is not', 'movie_actor.person_id', null]);
            } elseif ($type === 'directors') {
                $person_query
                    ->innerJoin('movie_director', 'person.id = movie_director.person_id')
                    ->andWhere(['is not', 'movie_director.person_id', null]);
            }
        }

        if ($award_id > 0) {
            $person_query
                ->innerJoin('person_award', 'person.id = person_award.person_id')
                ->andWhere(['person_award.award_id' => $award_id]);
        }

        if (!empty($sex)) {
            if ($sex === 'male') {
                $person_query
                    ->andWhere(['person.sex' => Person::SEX_MALE]);
            } elseif ($sex === 'female') {
                $person_query
                    ->andWhere(['person.sex' => Person::SEX_FEMALE]);
            }
        }

        if ($viewed_movies == 1) {
            $person_query->andWhere(['>', 'person.viewed_movies_count', 0]);
        }

        $person_query->groupBy(['person.id']);
        $person_query->prepareSorting($sort);

        $count_query = clone $person_query;
        $person_total_count = $count_query->count();

        return $this->render('index', [
            'person_query' => $person_query,
            'person_total_count' => $person_total_count,
            'type' => $type,
            'type_list' => Person::typeList(),
            'award_id' => $award_id,
            'award_list' => Award::getAwardsForPersonFilters(),
            'viewed_movies' => $viewed_movies,
            'sex' => $sex,
            'sex_list' => Person::sexList(),
            'sort' => $sort,
            'sort_list' => Person::sortList(),
        ]);
    }
}
