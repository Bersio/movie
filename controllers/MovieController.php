<?php

namespace app\controllers;

use app\models\imdb\ImdbUrlForm;
use Exception;
use Yii;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use app\models\Comment;
use app\models\Country;
use app\models\Genre;
use app\models\Movie;
use app\models\MovieCountry;
use app\models\MovieGenre;
use app\models\MovieActor;
use app\models\MovieAward;
use app\models\MovieDirector;
use app\models\Person;
use app\models\PersonAward;
use app\models\View;
use app\models\Queue;
use app\models\image\ImageUploadForm;
use app\widgets\Menu;

class MovieController extends \yii\web\Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => [
                    'create',
                    'edit',
                    'delete',
                    'edit-rating',
                    'add-genre',
                    'delete-movie-genre',
                    'add-actor',
                    'delete-movie-actor',
                    'add-director',
                    'delete-movie-director',
                    'update-image',
                    'without-country',
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        Menu::setActive(Menu::MENU_MOVIE);

        return parent::beforeAction($action);
    }

    public function actionIndex(int $is_favorite = 0, string $sort = null)
    {
        if (Yii::$app->request->isPost) {
            $url = Url::to([
                '/movie/index',
                'is_favorite' => Yii::$app->request->post('is_favorite'),
                'sort' => Yii::$app->request->post('sort'),
            ]);
            return $this->redirect($url)->send();
        }

        $movie_query = Movie::find()
            ->with(['views'])
            ->prepareSorting($sort, Movie::SORT_TYPE_MOVIE);

        if ($is_favorite) {
            $movie_query->andWhere(['movie.is_favorite' => true]);
        }

        $count_query = clone $movie_query;
        $movie_total_count = $count_query->count();

        return $this->render('index', [
            'movie_query' => $movie_query,
            'movie_total_count' => $movie_total_count,
            'is_favorite' => $is_favorite,
            'sort' => $sort,
            'sort_list' => Movie::sortList(),
        ]);
    }

    public function actionCreate()
    {
        $model = new Movie();
        $ImageUploadForm = new ImageUploadForm();

        if (Yii::$app->request->isPost) {
            if (!$model->load(Yii::$app->request->post())) {
                throw new Exception('Failed to load Movie');
            }

            // saving for id
            if (!$model->save()) {
                throw new Exception('Failed to save Movie: ' . $model->getErrorsAsString());
            }

            if (!$ImageUploadForm->load(Yii::$app->request->post())) {
                throw new Exception('Failed to load image form');
            }

            $model->image = $ImageUploadForm->upload($model);

            // another saving
            if (!$model->save()) {
                throw new Exception('Failed to save Movie: ' . $model->getErrorsAsString());
            }

            // автоматически добавляю фильм в очередь
            Queue::createQueueByMovie((int)$model->id);

            Yii::$app->session->setFlash('success', 'Фильм успешно сохранен');

            return $this->redirect('/movie/edit?id=' . $model->id);
        }

        return $this->render('create', [
            'model' => $model,
            'ImageUploadForm' => $ImageUploadForm,
        ]);
    }

    public function actionCreateByImdbUrl()
    {
        $imdbForm = new ImdbUrlForm();

        if (Yii::$app->request->isPost) {
            if (!$imdbForm->load(Yii::$app->request->post())) {
                throw new Exception('Failed to load imdb form');
            }

            $movie = $imdbForm->createMovie();

            // автоматически добавляю фильм в очередь
            Queue::createQueueByMovie((int)$movie->id);

            Yii::$app->session->setFlash('success', 'Фильм успешно сохранен');

            return $this->redirect('/movie/edit?id=' . $movie->id);
        }

        return $this->render('create-by-imdb-url', [
            'imdbForm' => $imdbForm,
        ]);
    }

    public function actionDetails(int $id)
    {
        $model = Movie::find()
            ->with(['views', 'views.audio', 'views.theater', 'views.specialShowing', 'views.drink'])
            ->where(['id' => $id])
            ->one();
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Movie by id: $id");
        }

        $movie_genre_list = Genre::find()
            ->innerJoin('movie_genre', 'genre.id = movie_genre.genre_id')
            ->where(['movie_genre.movie_id' => $model->id])
            ->groupBy(['genre.id'])
            ->sortByDefault()
            ->all();

        $movie_country_list = Country::find()
            ->innerJoin('movie_country', 'country.id = movie_country.country_id')
            ->where(['movie_country.movie_id' => $model->id])
            ->groupBy(['country.id'])
            ->sortByDefault()
            ->all();

        $movie_actor_list = Person::find()
            ->innerJoin('movie_actor', 'person.id = movie_actor.person_id')
            ->where(['movie_actor.movie_id' => $model->id])
            ->groupBy(['person.id'])
            ->sortByDefault()
            ->all();

        $movie_director_list = Person::find()
            ->innerJoin('movie_director', 'person.id = movie_director.person_id')
            ->where(['movie_director.movie_id' => $model->id])
            ->groupBy(['person.id'])
            ->sortByDefault()
            ->all();

        $comment_list = Comment::find()
            ->where(['movie_id' => $model->id])
            ->orderBy(['created_at' => SORT_DESC])
            ->all();

        $movie_award_list = MovieAward::find()
            ->with(['award', 'awardNomination'])
            ->where(['movie_id' => $model->id])
            ->orderBy([
                'type' => SORT_DESC,
                'id' => SORT_ASC
            ])
            ->all();

        $person_award_list = PersonAward::find()
            ->with(['award', 'awardNomination', 'person'])
            ->where(['movie_id' => $model->id])
            ->orderBy([
                'type' => SORT_DESC,
                'id' => SORT_ASC
            ])
            ->all();

        return $this->render('details', [
            'model' => $model,
            'movie_genre_list' => $movie_genre_list,
            'movie_country_list' => $movie_country_list,
            'movie_actor_list' => $movie_actor_list,
            'movie_director_list' => $movie_director_list,
            'comment_list' => $comment_list,
            'movie_award_list' => $movie_award_list,
            'person_award_list' => $person_award_list,
        ]);
    }

    public function actionEdit(int $id)
    {
        $model = Movie::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Movie by id: $id");
        }

        if (Yii::$app->request->isPost) {
            $load_result = $model->load(Yii::$app->request->post());
            if (!$load_result) {
                throw new Exception('Failed to load Movie');
            }

            $save_result = $model->save();
            if (!$save_result) {
                throw new Exception('Failed to save Movie: ' . $model->getErrorsAsString());
            }

            Yii::$app->session->setFlash('success', 'Фильм успешно сохранен');

            return $this->redirect('/movie/details?id=' . $model->id);
        }

        return $this->render('edit', [
            'model' => $model,
            'all_genre_list' => Genre::getAllGenresForFilters(),
            'all_person_list' => Person::getAllPersonsForFilters(),
            'all_country_list' => Country::getAllCountriesForFilters(),
            'imageUploadForm' => new ImageUploadForm(),
        ]);
    }

    public function actionDelete(int $id)
    {
        $model = Movie::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Movie by id: $id");
        }

        $model->deleteImageFolder();

        View::deleteAll(['movie_id' => $model->id]);
        Queue::deleteAll(['movie_id' => $model->id]);
        Comment::deleteAll(['movie_id' => $model->id]);
        MovieGenre::deleteAll(['movie_id' => $model->id]);
        MovieActor::deleteAll(['movie_id' => $model->id]);
        MovieDirector::deleteAll(['movie_id' => $model->id]);

        $model->delete();

        Yii::$app->session->setFlash('success', 'Фильм успешно удален');

        return $this->redirect('/movie/index');
    }

    public function actionAddGenre(int $id)
    {
        $movie = Movie::findOne($id);
        if (empty($movie)) {
            throw new NotFoundHttpException("Failed to get Movie by id: $id");
        }

        if (Yii::$app->request->isPost) {
            $genre_id = intval(Yii::$app->request->post('genre_id'));
            if ($genre_id <= 0) {
                throw new Exception('Failed to get genre id');
            }

            $genre = Genre::findOne($genre_id);
            if (empty($genre)) {
                throw new NotFoundHttpException("Failed to get Genre by id: $genre_id");
            }

            $old_movie_genre = MovieGenre::find()
                ->where(['movie_id' => $movie->id])
                ->andWhere(['genre_id' => $genre->id])
                ->one();
            if (!empty($old_movie_genre)) {
                // already added
                return $this->redirect('/movie/edit?id=' . $movie->id . '#genre');
            }

            // add genre to movie
            $movie_genre = new MovieGenre();
            $movie_genre->movie_id = $movie->id;
            $movie_genre->genre_id = $genre->id;

            $save_result = $movie_genre->save();
            if (!$save_result) {
                throw new Exception('Failed to save Movie Genre: ' . $movie_genre->getErrorsAsString());
            }

            return $this->redirect('/movie/edit?id=' . $movie->id . '#genre');
        }

        throw new Exception('Failed to add genre to movie');
    }

    public function actionDeleteMovieGenre(int $movie_id, int $genre_id)
    {
        $movie = Movie::findOne($movie_id);
        if (empty($movie)) {
            throw new NotFoundHttpException("Failed to get Movie by id: $movie_id");
        }

        $genre = Genre::findOne($genre_id);
        if (empty($genre)) {
            throw new NotFoundHttpException("Failed to get Genre by id: $genre_id");
        }

        MovieGenre::deleteAll(['movie_id' => $movie->id, 'genre_id' => $genre->id]);

        return $this->redirect('/movie/edit?id=' . $movie_id . '#genre');
    }

    public function actionAddCountry(int $id)
    {
        $movie = Movie::findOne($id);
        if (empty($movie)) {
            throw new NotFoundHttpException("Failed to get Movie by id: $id");
        }

        if (Yii::$app->request->isPost) {
            $country_id = intval(Yii::$app->request->post('country_id'));
            if ($country_id <= 0) {
                throw new Exception('Failed to get country id');
            }

            $country = Country::findOne($country_id);
            if (empty($country)) {
                throw new NotFoundHttpException("Failed to get Country by id: $country_id");
            }

            $old_movie_country = MovieCountry::find()
                ->where(['movie_id' => $movie->id])
                ->andWhere(['country_id' => $country->id])
                ->one();
            if (!empty($old_movie_country)) {
                // already added
                return $this->redirect('/movie/edit?id=' . $movie->id . '#country');
            }

            // add country to movie
            $movie_country = new MovieCountry();
            $movie_country->movie_id = $movie->id;
            $movie_country->country_id = $country->id;

            $save_result = $movie_country->save();
            if (!$save_result) {
                throw new Exception('Failed to save Movie Country: ' . $movie_country->getErrorsAsString());
            }

            return $this->redirect('/movie/edit?id=' . $movie->id . '#country');
        }

        throw new Exception('Failed to add country to movie');
    }

    public function actionDeleteMovieCountry(int $movie_id, int $country_id)
    {
        $movie = Movie::findOne($movie_id);
        if (empty($movie)) {
            throw new NotFoundHttpException("Failed to get Movie by id: $movie_id");
        }

        $country = Country::findOne($country_id);
        if (empty($country)) {
            throw new NotFoundHttpException("Failed to get Country by id: $country_id");
        }

        MovieCountry::deleteAll(['movie_id' => $movie->id, 'country_id' => $country->id]);

        return $this->redirect('/movie/edit?id=' . $movie_id . '#country');
    }

    public function actionAddActor(int $id)
    {
        $movie = Movie::findOne($id);
        if (empty($movie)) {
            throw new NotFoundHttpException("Failed to get Movie by id: $id");
        }

        if (Yii::$app->request->isPost) {
            $person_id = intval(Yii::$app->request->post('person_id'));
            if ($person_id <= 0) {
                throw new Exception('Failed to get person id');
            }

            $person = Person::findOne($person_id);
            if (empty($person)) {
                throw new NotFoundHttpException("Failed to get person by id: $person_id");
            }

            $old_movie_actor = MovieActor::find()
                ->where(['movie_id' => $movie->id])
                ->andWhere(['person_id' => $person->id])
                ->one();
            if (!empty($old_movie_actor)) {
                // already added
                return $this->redirect('/movie/edit?id=' . $movie->id . '#actor');
            }

            // add person to movie
            $movie_person = new MovieActor();
            $movie_person->movie_id = $movie->id;
            $movie_person->person_id = $person->id;

            $save_result = $movie_person->save();
            if (!$save_result) {
                throw new Exception('Failed to save Movie Person: ' . $movie_person->getErrorsAsString());
            }

            return $this->redirect('/movie/edit?id=' . $movie->id . '#actor');
        }

        throw new Exception('Failed to add person to movie');
    }

    public function actionDeleteMovieActor(int $movie_id, int $person_id)
    {
        $movie = Movie::findOne($movie_id);
        if (empty($movie)) {
            throw new NotFoundHttpException("Failed to get Movie by id: $movie_id");
        }

        $person = Person::findOne($person_id);
        if (empty($person)) {
            throw new NotFoundHttpException("Failed to get person by id: $person_id");
        }

        MovieActor::deleteAll(['movie_id' => $movie->id, 'person_id' => $person->id]);

        return $this->redirect('/movie/edit?id=' . $movie_id . '#actor');
    }

    public function actionAddDirector(int $id)
    {
        $movie = Movie::findOne($id);
        if (empty($movie)) {
            throw new NotFoundHttpException("Failed to get Movie by id: $id");
        }

        if (Yii::$app->request->isPost) {
            $person_id = intval(Yii::$app->request->post('person_id'));
            if ($person_id <= 0) {
                throw new Exception('Failed to get person id');
            }

            $person = Person::findOne($person_id);
            if (empty($person)) {
                throw new NotFoundHttpException("Failed to get person by id: $person_id");
            }

            $old_movie_director = MovieDirector::find()
                ->where(['movie_id' => $movie->id])
                ->andWhere(['person_id' => $person->id])
                ->one();
            if (!empty($old_movie_director)) {
                // already added
                return $this->redirect('/movie/edit?id=' . $movie->id . '#director');
            }

            // add person to movie
            $movie_person = new MovieDirector();
            $movie_person->movie_id = $movie->id;
            $movie_person->person_id = $person->id;

            $save_result = $movie_person->save();
            if (!$save_result) {
                throw new Exception('Failed to save Movie Person: ' . $movie_person->getErrorsAsString());
            }

            return $this->redirect('/movie/edit?id=' . $movie->id . '#director');
        }

        throw new Exception('Failed to add person to movie');
    }

    public function actionDeleteMovieDirector(int $movie_id, int $person_id)
    {
        $movie = Movie::findOne($movie_id);
        if (empty($movie)) {
            throw new NotFoundHttpException("Failed to get Movie by id: $movie_id");
        }

        $person = Person::findOne($person_id);
        if (empty($person)) {
            throw new NotFoundHttpException("Failed to get person by id: $person_id");
        }

        MovieDirector::deleteAll(['movie_id' => $movie->id, 'person_id' => $person->id]);

        return $this->redirect('/movie/edit?id=' . $movie_id . '#director');
    }

    public function actionUpdateImage(int $id)
    {
        $model = Movie::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Movie by id: $id");
        }

        $ImageUploadForm = new ImageUploadForm();

        if (Yii::$app->request->isPost) {
            if (!empty($model->image)) {
                $old_image_path = Yii::getAlias('@webroot') . '/images/movie/' . $model->image;

                if (file_exists($old_image_path)) {
                    unlink($old_image_path);
                }
            }

            if (!$ImageUploadForm->load(Yii::$app->request->post())) {
                throw new Exception('Failed to load image form');
            }

            $model->image = $ImageUploadForm->upload($model);

            if (!$model->save()) {
                throw new Exception('Failed to save Movie: ' . $model->getErrorsAsString());
            }

            Yii::$app->session->setFlash('success', 'Картинка фильма успешно обновлена');
        }

        return $this->redirect('/movie/edit?id=' . $model->id);
    }

    public function actionWithoutCountry(string $sort = null)
    {
        if (Yii::$app->request->isPost) {
            $url = Url::to([
                '/movie/without-country',
                'sort' => Yii::$app->request->post('sort'),
            ]);
            return $this->redirect($url)->send();
        }

        $movie_query = Movie::find()
            ->join('LEFT OUTER JOIN', 'movie_country', 'movie.id = movie_country.movie_id')
            ->where(['is', 'movie_country.id', null])
            ->prepareSorting($sort, Movie::SORT_TYPE_MOVIE);

        $count_query = clone $movie_query;
        $movie_total_count = $count_query->count();

        return $this->render('index', [
            'movie_query' => $movie_query,
            'movie_total_count' => $movie_total_count,
            'sort' => $sort,
            'sort_list' => Movie::sortList(),
        ]);
    }
}
