<?php

namespace app\controllers;

use Exception;
use Yii;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use app\models\Audio;
use app\models\Movie;
use app\models\View;
use app\widgets\Menu;

class AudioController extends \yii\web\Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['create', 'edit', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        Menu::setActive(Menu::MENU_AUDIO);

        return parent::beforeAction($action);
    }

    public function actionIndex(string $sort = null)
    {
        if (Yii::$app->request->isPost) {
            $url = Url::to([
                '/audio/index',
                'sort' => Yii::$app->request->post('sort'),
            ]);
            return $this->redirect($url)->send();
        }

        $audio_query = Audio::find()->prepareSorting($sort);

        $count_query = clone $audio_query;
        $audio_total_count = $count_query->count();

        return $this->render('index', [
            'audio_query' => $audio_query,
            'audio_total_count' => $audio_total_count,
            'sort' => $sort,
            'sort_list' => Audio::sortList(),
        ]);
    }

    public function actionDetails(int $id, int $is_favorite = 0, string $sort = null)
    {
        $model = Audio::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Audio by id: $id");
        }

        if (Yii::$app->request->isPost) {
            $url = Url::to([
                '/audio/details',
                'id' => $model->id,
                'is_favorite' => Yii::$app->request->post('is_favorite', 0),
                'sort' => Yii::$app->request->post('sort'),
            ]);
            return $this->redirect($url)->send();
        }

        $view_query = View::find()
            ->innerJoin('movie', 'view.movie_id = movie.id')
            ->with(['movie'])
            ->where(['view.audio_id' => $model->id])
            ->groupBy(['view.id'])
            ->prepareSorting($sort, Movie::SORT_TYPE_VIEW);

        if ($is_favorite) {
            $view_query->andWhere(['movie.is_favorite' => true]);
        }

        return $this->render('details', [
            'model' => $model,
            'view_query' => $view_query,
            'is_favorite' => $is_favorite,
            'sort' => $sort,
            'sort_list' => View::sortList(),
        ]);
    }

    public function actionCreate()
    {
        $model = new Audio();

        if (Yii::$app->request->isPost) {
            $load_result = $model->load(Yii::$app->request->post());
            if (!$load_result) {
                throw new Exception('Failed to load Audio');
            }

            $save_result = $model->save();
            if (!$save_result) {
                throw new Exception('Failed to save Audio: ' . $model->getErrorsAsString());
            }

            Yii::$app->session->setFlash('success', 'Озвучка успешно сохранена');

            return $this->redirect('/audio/index');
        }

        return $this->render('create', [
            'model' => $model
        ]);
    }

    public function actionEdit(int $id)
    {
        $model = Audio::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Audio by id: $id");
        }

        if (Yii::$app->request->isPost) {
            $load_result = $model->load(Yii::$app->request->post());
            if (!$load_result) {
                throw new Exception('Failed to load Audio');
            }

            $save_result = $model->save();
            if (!$save_result) {
                throw new Exception('Failed to save Audio: ' . $model->getErrorsAsString());
            }

            Yii::$app->session->setFlash('success', 'Озвучка успешно сохранена');

            return $this->redirect('/audio/details?id=' . $model->id);
        }

        return $this->render('edit', [
            'model' => $model
        ]);
    }

    public function actionDelete(int $id)
    {
        $model = Audio::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Audio by id: $id");
        }

        $model->delete();

        Yii::$app->session->setFlash('success', 'Озвучка успешно удалена');

        return $this->redirect('/audio/index');
    }
}
