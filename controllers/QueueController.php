<?php

namespace app\controllers;

use Exception;
use Yii;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use app\models\Award;
use app\models\Genre;
use app\models\Movie;
use app\models\Queue;
use app\widgets\Menu;

class QueueController extends \yii\web\Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['create', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        Menu::setActive(Menu::MENU_QUEUE);

        return parent::beforeAction($action);
    }

    public function actionIndex(
        int $genre_id = 0,
        int $award_id = 0,
        int $is_favorite = 0,
        int $year = 0,
        string $sort = null
    ) {
        if (Yii::$app->request->isPost) {
            $url = Url::to([
                '/queue/index',
                'genre_id' => Yii::$app->request->post('genre_id', 0),
                'award_id' => Yii::$app->request->post('award_id', 0),
                'is_favorite' => Yii::$app->request->post('is_favorite', 0),
                'year' => Yii::$app->request->post('year', 0),
                'sort' => Yii::$app->request->post('sort'),
            ]);
            return $this->redirect($url)->send();
        }

        $queue_query = Movie::find()
            ->innerJoin('queue', 'movie.id = queue.movie_id')
            ->where(['>', 'movie.id', 0]);

        if ($genre_id > 0) {
            $queue_query
                ->innerJoin('movie_genre', 'movie.id = movie_genre.movie_id')
                ->andWhere(['movie_genre.genre_id' => $genre_id]);
        }

        if ($award_id > 0) {
            $queue_query
                ->innerJoin('movie_award', 'movie.id = movie_award.movie_id')
                ->andWhere(['movie_award.award_id' => $award_id]);
        }

        if ($is_favorite) {
            $queue_query->andWhere(['movie.is_favorite' => true]);
        }

        if ($year > 0) {
            $queue_query->andWhere(['movie.year' => $year]);
        }

        $queue_query->groupBy(['movie.id']);
        $queue_query->prepareSorting($sort, Movie::SORT_TYPE_QUEUE);

        $count_query = clone $queue_query;
        $queue_total_count = $count_query->count();

        $queue_query->with([
            'movieAwards.award',
            'movieAwards.awardNomination',
            'personAwards.award',
            'personAwards.awardNomination',
            'personAwards.person',
        ]);

        return $this->render('index', [
            'queue_query' => $queue_query,
            'queue_total_count' => $queue_total_count,
            'genre_id' => $genre_id,
            'genre_list' => Genre::getGenresForQueueFilters(),
            'award_id' => $award_id,
            'award_list' => Award::getAwardsForQueueFilters(),
            'year' => $year,
            'year_list' => Movie::getYearsForQueueFilters(),
            'is_favorite' => $is_favorite,
            'sort' => $sort,
            'sort_list' => Queue::sortList(),
        ]);
    }

    public function actionCreate()
    {
        $model = new Queue();

        if (Yii::$app->request->isPost) {
            $load_result = $model->load(Yii::$app->request->post());
            if (!$load_result) {
                throw new Exception('Failed to load Queue');
            }

            $old_queue = Queue::find()
                ->where(['movie_id' => $model->movie_id])
                ->one();
            if (!empty($old_queue)) {
                // фильм уже в очереди
                return $this->redirect('/queue/index');
            }

            $save_result = $model->save();
            if (!$save_result) {
                throw new Exception('Failed to save Queue: ' . $model->getErrorsAsString());
            }

            Yii::$app->session->setFlash('success', 'Фильм успешно добавлен в очередь');

            return $this->redirect('/queue/index');
        }

        return $this->render('create', [
            'model' => $model,
            'movie_list' => Movie::getMoviesForQueue()
        ]);
    }

    public function actionDelete(int $movie_id)
    {
        $movie = Movie::findOne($movie_id);
        if (empty($movie)) {
            throw new NotFoundHttpException("Failed to get Movie by id: $movie_id");
        }

        Queue::deleteAll(['movie_id' => $movie->id]);

        Yii::$app->session->setFlash('success', 'Фильм успешно удален из очереди');

        return $this->redirect('/queue/index');
    }

    public function actionRandom(int $genre_id = 0, int $award_id = 0, int $year = 0)
    {
        $queue_query = Queue::find();

        if ($genre_id > 0) {
            $queue_query
                ->innerJoin('movie_genre', 'queue.movie_id = movie_genre.movie_id')
                ->andWhere(['movie_genre.genre_id' => $genre_id]);
        }

        if ($award_id > 0) {
            $queue_query
                ->innerJoin('movie_award', 'queue.movie_id = movie_award.movie_id')
                ->andWhere(['movie_award.award_id' => $award_id]);
        }

        if ($year > 0) {
            $queue_query
                ->innerJoin('movie', 'queue.movie_id = movie.id')
                ->andWhere(['movie.year' => $year]);
        }

        $queue_list = $queue_query
                ->groupBy(['queue.movie_id'])
                ->orderBy(['created_at' => SORT_DESC])
                ->all();

        $random_index = array_rand($queue_list);

        return $this->redirect('/movie/details?id=' . $queue_list[$random_index]->movie_id);
    }
}
