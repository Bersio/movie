<?php

namespace app\controllers;

use Exception;
use Yii;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use app\models\Comment;
use app\models\View;
use app\models\image\CommentImagesUploadForm;
use app\models\image\Thumb;

class CommentController extends \yii\web\Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['create', 'edit', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionCreate(int $movie_id = null, int $person_id = null, int $series_id = null)
    {
        $model = new Comment();
        $imagesUploadForm = new CommentImagesUploadForm();

        if ($movie_id > 0) {
            $model->movie_id = $movie_id;
        } else if ($person_id > 0) {
            $model->person_id = $person_id;
        } else if ($series_id > 0) {
            $model->series_id = $series_id;
        } else {
            throw new Exception('Failed to get comment target');
        }

        if (!empty($model->movie_id)) {
            $view = View::find()->where(['movie_id' => $model->movie_id])->one();
            if (empty($view)) {
                throw new Exception('need watch movie first');
            }
        }

        if (Yii::$app->request->isPost) {
            $load_result = $model->load(Yii::$app->request->post());
            if (!$load_result) {
                throw new Exception('Failed to load Comment');
            }

            // saving for id
            if (!$model->save()) {
                throw new Exception('Failed to save Comment: ' . $model->getErrorsAsString());
            }

            $model->images = $imagesUploadForm->upload($model);

            // another saving
            if ($model->images) {
                if (!$model->save()) {
                    throw new Exception('Failed to save Comment: ' . $model->getErrorsAsString());
                }
                
                foreach ($model->images as $image) {
                    foreach (Thumb::WIDTH_ALL as $width) {
                        Thumb::create($width, $model, $image);
                    }
                }
            }

            Yii::$app->session->setFlash('success', 'Комментарий успешно сохранен');

            if (!empty($model->movie_id)) {
                return $this->redirect('/movie/details?id=' . $model->movie_id);
            } else if (!empty($model->person_id)) {
                return $this->redirect('/person/details?id=' . $model->person_id);
            } else if (!empty($model->series_id)) {
                return $this->redirect('/series/details?id=' . $model->series_id);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'imagesUploadForm' => $imagesUploadForm,
        ]);
    }

    public function actionEdit(int $id)
    {
        $model = Comment::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Comment by id: $id");
        }

        if (Yii::$app->request->isPost) {
            $load_result = $model->load(Yii::$app->request->post());
            if (!$load_result) {
                throw new Exception('Failed to load Comment');
            }

            $save_result = $model->save();
            if (!$save_result) {
                throw new Exception('Failed to save Comment: ' . $model->getErrorsAsString());
            }

            Yii::$app->session->setFlash('success', 'Комментарий успешно сохранен');

            if (!empty($model->movie_id)) {
                return $this->redirect('/movie/details?id=' . $model->movie_id);
            } else if (!empty($model->person_id)) {
                return $this->redirect('/person/details?id=' . $model->person_id);
            }
        }

        return $this->render('edit', [
            'model' => $model
        ]);
    }

    public function actionDelete(int $id)
    {
        $model = Comment::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Comment by id: $id");
        }

        $model->deleteImageFolder();
        $model->deleteThumbs();
        $model->delete();

        Yii::$app->session->setFlash('success', 'Комментарий успешно удален');

        if (!empty($model->movie_id)) {
            return $this->redirect('/movie/details?id=' . $model->movie_id);
        } else if (!empty($model->person_id)) {
            return $this->redirect('/person/details?id=' . $model->person_id);
        } else if (!empty($model->series_id)) {
            return $this->redirect('/series/details?id=' . $model->series_id);
        } else {
            throw new Exception('Failed to get comment target');
        }
    }
}
