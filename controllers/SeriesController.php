<?php

namespace app\controllers;

use app\models\Comment;
use app\models\image\ImageUploadForm;
use app\models\imdb\ImdbUrlForm;
use app\models\Series;
use app\widgets\Menu;
use Exception;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class SeriesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => [
                    'create',
                    'edit',
                    'delete',
                    'update-image',
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        Menu::setActive(Menu::MENU_SERIES);

        return parent::beforeAction($action);
    }

    public function actionIndex(int $status = 0, int $is_favorite = 0, string $sort = null)
    {
        if (Yii::$app->request->isPost) {
            $url = Url::to([
                '/series/index',
                'status' => Yii::$app->request->post('status'),
                'is_favorite' => Yii::$app->request->post('is_favorite'),
                'sort' => Yii::$app->request->post('sort'),
            ]);
            return $this->redirect($url)->send();
        }

        $series_query = Series::find()->where(['>', 'series.id', 0]);

        if ($status > 0) {
            $series_query->andWhere(['series.status' => $status]);
        }

        if ($is_favorite) {
            $series_query->andWhere(['series.is_favorite' => true]);
        }

        $count_query = clone $series_query;
        $series_total_count = $count_query->count();

        return $this->render('index', [
            'series_query' => $series_query,
            'series_total_count' => $series_total_count,
            'status' => $status,
            'status_list' => Series::getAllStatusesForFilters(),
            'is_favorite' => $is_favorite,
            'sort' => $sort,
            'sort_list' => Series::sortList(),
        ]);
    }

    public function actionCreate()
    {
        $model = new Series();
        $imageUploadForm = new ImageUploadForm();

        if (Yii::$app->request->isPost) {
            if (!$model->load(Yii::$app->request->post())) {
                throw new Exception('Failed to load Series');
            }

            // saving for id
            if (!$model->save()) {
                throw new Exception("Failed to save Series: " . $model->getErrorsAsString());
            }

            if (!$imageUploadForm->load(Yii::$app->request->post())) {
                throw new Exception('Failed to load image form');
            }

            $model->image = $imageUploadForm->upload($model);

            // another saving
            if (!$model->save()) {
                throw new Exception("Failed to save Series: " . $model->getErrorsAsString());
            }

            Yii::$app->session->setFlash('success', 'Сериал успешно сохранен');

            return $this->redirect('/series/edit?id=' . $model->id);
        }

        return $this->render('create', [
            'model' => $model,
            'imageUploadForm' => $imageUploadForm,
        ]);
    }

    public function actionCreateByImdbUrl()
    {
        $imdbForm = new ImdbUrlForm();

        if (Yii::$app->request->isPost) {
            if (!$imdbForm->load(Yii::$app->request->post())) {
                throw new Exception('Failed to load imdb form');
            }

            $series = $imdbForm->createSeries();

            Yii::$app->session->setFlash('success', 'Сериал успешно сохранен');

            return $this->redirect('/series/edit?id=' . $series->id);
        }

        return $this->render('create-by-imdb-url', [
            'imdbForm' => $imdbForm,
        ]);
    }

    public function actionDetails(int $id)
    {
        $model = Series::find()
            ->where(['id' => $id])
            ->one();
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Series by id: $id");
        }

        $comment_list = Comment::find()
            ->where(['series_id' => $model->id])
            ->orderBy(['created_at' => SORT_DESC])
            ->all();

        return $this->render('details', [
            'model' => $model,
            'comment_list' => $comment_list,
        ]);
    }

    public function actionEdit(int $id)
    {
        $model = Series::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Series by id: $id");
        }

        if (Yii::$app->request->isPost) {
            $load_result = $model->load(Yii::$app->request->post());
            if (!$load_result) {
                throw new Exception('Failed to load Series');
            }

            $save_result = $model->save();
            if (!$save_result) {
                throw new Exception('Failed to save Series: ' . $model->getErrorsAsString());
            }

            Yii::$app->session->setFlash('success', 'Фильм успешно сохранен');

            return $this->redirect('/series/details?id=' . $model->id);
        }

        return $this->render('edit', [
            'model' => $model,
            'imageUploadForm' => new ImageUploadForm(),
        ]);
    }

    public function actionUpdateImage(int $id)
    {
        $model = Series::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Series by id: $id");
        }

        $imageUploadForm = new ImageUploadForm();

        if (Yii::$app->request->isPost) {
            if (!empty($model->image)) {
                $old_image_path = Yii::getAlias('@webroot') . '/images/series/' . $model->image;

                if (file_exists($old_image_path)) {
                    unlink($old_image_path);
                }
            }

            if (!$imageUploadForm->load(Yii::$app->request->post())) {
                throw new Exception('Failed to load image form');
            }

            $model->image = $imageUploadForm->upload($model);

            if (!$model->save()) {
                throw new Exception('Failed to save Series: ' . $model->getErrorsAsString());
            }

            Yii::$app->session->setFlash('success', 'Картинка фильма успешно обновлена');
        }

        return $this->redirect('/series/edit?id=' . $model->id);
    }

    public function actionDelete(int $id)
    {
        $model = Series::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Series by id: $id");
        }

        $model->deleteImageFolder();

        Comment::deleteAll(['series_id' => $model->id]);

        $model->delete();

        return $this->redirect('/series/index');
    }
}
