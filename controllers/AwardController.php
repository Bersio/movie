<?php

namespace app\controllers;

use Exception;
use Yii;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\web\Cookie;
use yii\web\NotFoundHttpException;
use app\models\Award;
use app\models\AwardNomination;
use app\models\Movie;
use app\models\MovieAward;
use app\models\Person;
use app\models\PersonAward;
use app\widgets\Menu;
use app\models\image\ImageUploadForm;
use yii\base\Model;

class AwardController extends \yii\web\Controller
{
    private const string COOKIE_NAME_LAST_ADDED_NOMINATION_ID = 'last-award-nomination-id';
    private const string COOKIE_NAME_LAST_ADDED_YEAR = 'last-award-year';

    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['create', 'edit', 'delete', 'update-image', 'assign-movie', 'assign-person'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action): bool
    {
        Menu::setActive(Menu::MENU_AWARD);

        return parent::beforeAction($action);
    }

    public function actionIndex(string $sort = null)
    {
        if (Yii::$app->request->isPost) {
            $url = Url::to([
                '/award/index',
                'sort' => Yii::$app->request->post('sort'),
            ]);
            return $this->redirect($url)->send();
        }

        $award_query = Award::find()->prepareSorting($sort);

        $count_query = clone $award_query;
        $award_total_count = $count_query->count();

        return $this->render('index', [
            'award_query' => $award_query,
            'award_total_count' => $award_total_count,
            'sort' => $sort,
            'sort_list' => Award::sortList(),
        ]);
    }

    public function actionCreate()
    {
        $model = new Award();
        $ImageWinnerUploadForm = new ImageUploadForm();
        $ImageNominationUploadForm = new ImageUploadForm();

        if (Yii::$app->request->isPost) {
            $load_result = $model->load(Yii::$app->request->post());
            if (!$load_result) {
                throw new Exception('Failed to load Award');
            }

            // saving for id
            if (!$model->save()) {
                throw new Exception('Failed to save Award: ' . $model->getErrorsAsString());
            }

            Model::loadMultiple([
                'winner' => $ImageWinnerUploadForm,
                'nomination' => $ImageNominationUploadForm
            ], Yii::$app->request->post());

            $model->image_winner = $ImageWinnerUploadForm->upload($model, 'winner');
            $model->image_nomination = $ImageNominationUploadForm->upload($model, 'nomination');

            // another saving
            $save_result = $model->save();
            if (!$save_result) {
                throw new Exception('Failed to save Award: ' . $model->getErrorsAsString());
            }

            Yii::$app->session->setFlash('success', 'Награда успешно сохранена');

            return $this->redirect('/award/edit?id=' . $model->id);
        }

        return $this->render('create', [
            'model' => $model,
            'ImageWinnerUploadForm' => $ImageWinnerUploadForm,
            'ImageNominationUploadForm' => $ImageNominationUploadForm,
        ]);
    }

    public function actionEdit(int $id)
    {
        $model = Award::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Award by id: $id");
        }

        if (Yii::$app->request->isPost) {
            $load_result = $model->load(Yii::$app->request->post());
            if (!$load_result) {
                throw new Exception('Failed to load Award');
            }

            $save_result = $model->save();
            if (!$save_result) {
                throw new Exception('Failed to save Award: ' . $model->getErrorsAsString());
            }

            Yii::$app->session->setFlash('success', 'Награда успешно сохранена');

            return $this->redirect('/award/details?id=' . $model->id);
        }

        return $this->render('edit', [
            'model' => $model,
            'ImageWinnerUploadForm' => new ImageUploadForm(),
            'ImageNominationUploadForm' => new ImageUploadForm(),
        ]);
    }

    public function actionDetails(int $id, string $sort = null)
    {
        $model = Award::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Award by id: $id");
        }

        if (Yii::$app->request->isPost) {
            $url = Url::to([
                '/award/details',
                'id' => $model->id,
                'sort' => Yii::$app->request->post('sort'),
            ]);
            return $this->redirect($url)->send();
        }

        $award_nomination_query = AwardNomination::find()
            ->where(['award_id' => $model->id])
            ->prepareSorting($sort);

        return $this->render('details', [
            'model' => $model,
            'award_nomination_query' => $award_nomination_query,
            'sort' => $sort,
            'sort_list' => AwardNomination::sortList(),
        ]);
    }

    public function actionDelete(int $id)
    {
        $award = Award::findOne($id);
        if (empty($award)) {
            throw new NotFoundHttpException("Failed to get Award by id: $id");
        }

        $award->delete();

        Yii::$app->session->setFlash('success', 'Награда успешно удалена');

        return $this->redirect('/award/index');
    }

    public function actionUpdateImage(int $id)
    {
        $model = Award::findOne($id);
        if (empty($model)) {
            throw new NotFoundHttpException("Failed to get Award by id: $id");
        }

        if (Yii::$app->request->isPost) {
            $award_image_type = Yii::$app->request->post('award-image-type');
            if (empty($award_image_type)) {
                throw new Exception('Failed to get award image type');
            }

            $ImageWinnerUploadForm = new ImageUploadForm();
            $ImageNominationUploadForm = new ImageUploadForm();

            Model::loadMultiple([
                'winner' => $ImageWinnerUploadForm,
                'nomination' => $ImageNominationUploadForm
            ], Yii::$app->request->post());

            if ($award_image_type === 'winner') {
                if (!empty($model->image_winner)) {
                    $old_image_path = Yii::getAlias('@webroot') . '/images/award/' . $model->image_winner;

                    if (file_exists($old_image_path)) {
                        unlink($old_image_path);
                    }
                }

                $model->image_winner = $ImageWinnerUploadForm->upload($model, 'winner');
            } elseif ($award_image_type === 'nominant') {
                if (!empty($model->image_nomination)) {
                    $old_image_path = Yii::getAlias('@webroot') . '/images/award/' . $model->image_nomination;

                    if (file_exists($old_image_path)) {
                        unlink($old_image_path);
                    }
                }

                $model->image_nomination = $ImageNominationUploadForm->upload($model, 'nomination');
            } else {
                throw new Exception('Image type is not correct');
            }

            $save_result = $model->save();
            if (!$save_result) {
                throw new Exception('Failed to save Award: ' . $model->getErrorsAsString());
            }
        }

        return $this->redirect('/award/edit?id=' . $model->id);
    }

    public function actionAssignMovie(int $id)
    {
        $award = Award::findOne($id);
        if (empty($award)) {
            throw new NotFoundHttpException("Failed to get Award by id: $id");
        }

        $model = new MovieAward();
        $model->award_id = $award->id;
        $model->award_nomination_id = Yii::$app->request->cookies->getValue(self::COOKIE_NAME_LAST_ADDED_NOMINATION_ID);
        $model->year = Yii::$app->request->cookies->getValue(self::COOKIE_NAME_LAST_ADDED_YEAR);

        if (Yii::$app->request->isPost) {
            $load_result = $model->load(Yii::$app->request->post());
            if (!$load_result) {
                throw new Exception('Failed to load Movie Award');
            }

            $save_result = $model->save();
            if (!$save_result) {
                throw new Exception('Failed to save Movie Award: ' . $model->getErrorsAsString());
            }

            Yii::$app->response->cookies->add(new Cookie([
                'name' => self::COOKIE_NAME_LAST_ADDED_NOMINATION_ID,
                'value' => $model->award_nomination_id,
            ]));

            Yii::$app->response->cookies->add(new Cookie([
                'name' => self::COOKIE_NAME_LAST_ADDED_YEAR,
                'value' => $model->year,
            ]));

            return $this->redirect('/award/details?id=' . $award->id);
        }

        return $this->render('assign-movie', [
            'award' => $award,
            'model' => $model,
            'nomination_list' => $award->getNominationsForFilters(AwardNomination::NOMINATION_TYPE_MOVIE),
            'movie_list' => Movie::getAllMoviesForFilters(),
            'type_list' => Award::typeList(),
        ]);
    }

    public function actionAssignPerson(int $id)
    {
        $award = Award::findOne($id);
        if (empty($award)) {
            throw new NotFoundHttpException("Failed to get Award by id: $id");
        }

        $model = new PersonAward();
        $model->award_id = $award->id;
        $model->award_nomination_id = Yii::$app->request->cookies->getValue(self::COOKIE_NAME_LAST_ADDED_NOMINATION_ID);
        $model->year = Yii::$app->request->cookies->getValue(self::COOKIE_NAME_LAST_ADDED_YEAR);

        if (Yii::$app->request->isPost) {
            $load_result = $model->load(Yii::$app->request->post());
            if (!$load_result) {
                throw new Exception('Failed to load Person Award');
            }

            $save_result = $model->save();
            if (!$save_result) {
                throw new Exception('Failed to save Person Award: ' . $model->getErrorsAsString());
            }

            Yii::$app->response->cookies->add(new Cookie([
                'name' => self::COOKIE_NAME_LAST_ADDED_NOMINATION_ID,
                'value' => $model->award_nomination_id,
            ]));

            Yii::$app->response->cookies->add(new Cookie([
                'name' => self::COOKIE_NAME_LAST_ADDED_YEAR,
                'value' => $model->year,
            ]));

            return $this->redirect('/award/details?id=' . $award->id);
        }

        return $this->render('assign-person', [
            'award' => $award,
            'model' => $model,
            'nomination_list' => $award->getNominationsForFilters(AwardNomination::NOMINATION_TYPE_PERSON),
            'movie_list' => Movie::getAllMoviesForFilters(),
            'person_list' => Person::getAllPersonsForFilters(),
            'type_list' => Award::typeList(),
        ]);
    }

    public function actionResult(int $id, int $year = 0, int $only_winner = 0, int $only_without_view = 0)
    {
        if (Yii::$app->request->isPost) {
            $url = Url::to([
                '/award/result',
                'id' => $id,
                'year' => Yii::$app->request->post('year', 0),
                'only_winner' => Yii::$app->request->post('only_winner', 0),
                'only_without_view' => Yii::$app->request->post('only_without_view', 0),
            ]);
            return $this->redirect($url)->send();
        }

        $award = Award::findOne($id);
        if (empty($award)) {
            throw new NotFoundHttpException("Failed to get Award by id: $id");
        }

        $nomination_query = AwardNomination::find()
            ->where(['award_id' => $award->id])
            ->sortByDefault();

        $years_list = $award->getYearsList();

        if ($year === 0) {
            // год не выбран, заполняем все доступные
            $selected_years_list = $years_list;
        } else {
            // год выбран
            $selected_years_list[$year] = $year;
        }

        $this->on(
            $this::EVENT_AFTER_ACTION,
            function ($event) {
                $event->result = str_replace(
                    ['{VIEWED_MOVIES}', '{MOVIE_WITHOUT_VIEW}'],
                    [
                        $event->data['model']->getViewedMoviesCount(),
                        $event->data['model']->getMoviesWithoutViewCount()
                    ],
                    $event->result
                );
            },
            ['model' => $award]
        );

        return $this->render('result', [
            'award' => $award,
            'nomination_query' => $nomination_query,
            'year' => $year,
            'years_list' => $years_list,
            'selected_years_list' => $selected_years_list,
            'only_winner' => $only_winner,
            'only_without_view' => $only_without_view,
        ]);
    }
}
