<?php

namespace app\assets\flags;

use yii\web\AssetBundle;

class FlagAsset extends AssetBundle
{
    public $sourcePath = '@vendor/components/flag-icon-css';
    public $css = [
        'css/flag-icon.min.css',
    ];
    public $publishOptions = [
        'only' => [
            'flags/4x3/*',
            'css/flag-icon.min.css',
        ]
    ];
}
