<?php

namespace app\commands;

use app\commands\services\InitService;
use Exception;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * Контроллер для налаштування локальної роботи.
 * Команда може бути виконана композером автоматично після оновлення пакетів.
 */
class InitController extends Controller
{
    /**
     * Основний метод налаштування роботи проєкту
     *
     * This command creates folders for images and generate cookie validation key if needed.
     * @return int Exit code
     * @throws Exception
     */
    public function actionIndex(): int
    {
        $directories = [
            '/web/images',
            '/web/images/award',
            '/web/images/comment',
            '/web/images/movie',
            '/web/images/person',
            '/web/images/series',
        ];

        foreach ($directories as $directory_item) {
            InitService::createDirectoryIfNotExist($directory_item);
        }

        InitService::createLocalFileIfNotExist('db');
        InitService::createLocalFileIfNotExist('params');
        InitService::createCookieLocalFileIfNotExist();

        echo ExitCode::getReason(ExitCode::OK) . PHP_EOL;

        return ExitCode::OK;
    }
}
