<?php

namespace app\commands\services;

use Exception;
use Yii;

/**
 * Сервіс містить методи налаштування локальної роботи
 */
class InitService
{
    /**
     * Створює директорію, якщо така не існує
     *
     * @param $directory_name
     * @return void
     * @throws Exception
     */
    public static function createDirectoryIfNotExist($directory_name): void
    {
        $directory_path = Yii::getAlias('@app') . $directory_name;
        if (!is_dir($directory_path)) {
            if (!mkdir($directory_path, 0777, true)) {
                throw new Exception("Failed to create folder $directory_path");
            }

            echo 'created ' . $directory_path . PHP_EOL;
        }
    }

    /**
     * Робить копію файлу конфігу з суфіксом -local для локальних налаштувань
     *
     * @param $file_type
     * @return void
     */
    public static function createLocalFileIfNotExist($file_type): void
    {
        $default_file = Yii::getAlias('@app') . "/config/$file_type.php";
        $local_file = Yii::getAlias('@app') . "/config/$file_type-local.php";
        if (file_exists($default_file) && !file_exists($local_file)) {
            copy($default_file, $local_file);

            echo 'created ' . $local_file . PHP_EOL;
        }
    }

    /**
     * Перевіряє наявність локального файлу з ключем валідацїї cookie, за відсутності створює
     *
     * @return void
     */
    public static function createCookieLocalFileIfNotExist(): void
    {
        $cookie_local_file = Yii::getAlias('@app') . '/config/cookie-local.php';
        if (!file_exists($cookie_local_file)) {
            self::generateCookieLocalFile($cookie_local_file);

            echo 'created ' . $cookie_local_file . PHP_EOL;
        }
    }

    /**
     * Створює локальний файл з ключем валідацїї cookie
     *
     * @param $cookie_local_file
     * @return void
     */
    public static function generateCookieLocalFile($cookie_local_file): void
    {
        $length = 32;
        $bytes = openssl_random_pseudo_bytes($length);
        $key = strtr(substr(base64_encode($bytes), 0, $length), '+/=', '_-.');

        file_put_contents($cookie_local_file, <<<EOF
<?php

return '$key';

EOF
        );
    }
}
