<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Audio;
use app\models\Country;
use app\models\Drink;
use app\models\Genre;
use app\models\Movie;
use app\models\Person;
use app\models\Theater;
use app\models\SpecialShowing;

class CronController extends Controller
{
    /**
     * This command updates sort counts.
     * @return int Exit code
     */
    public function actionSortCounts()
    {
        Genre::prepareSortCounts();
        Country::prepareSortCounts();
        Person::prepareSortCounts();
        Movie::prepareSortCounts();
        Audio::prepareSortCounts();
        Drink::prepareSortCounts();
        Theater::prepareSortCounts();
        SpecialShowing::prepareSortCounts();

        return ExitCode::OK;
    }

    /**
     * This command counts average movie ratings.
     * @return int Exit code
     */
    public function actionAverageRatings()
    {
        Movie::prepareAverageRatings();

        return ExitCode::OK;
    }
}
