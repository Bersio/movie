<?php

namespace app\models;

use Exception;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "comment".
 *
 * @property int $id
 * @property int|null $movie_id
 * @property int|null $person_id
 * @property int|null $series_id
 * @property string $content
 * @property string|null $images
 * @property string $created_at
 * @property string|null $updated_at
 *
 * @property Movie $movie
 * @property Person $person
 */
class Comment extends \app\models\base\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'comment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['movie_id', 'person_id', 'series_id'], 'integer'],
            [['content', 'created_at'], 'required'],
            [['content'], 'string'],
            [['images', 'created_at', 'updated_at'], 'safe'],
            [['movie_id'], 'exist', 'skipOnError' => true, 'targetClass' => Movie::class, 'targetAttribute' => ['movie_id' => 'id']],
            [['person_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::class, 'targetAttribute' => ['person_id' => 'id']],
            [['series_id'], 'exist', 'skipOnError' => true, 'targetClass' => Series::class, 'targetAttribute' => ['series_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'movie_id' => Yii::t('app', 'Movie'),
            'person_id' => Yii::t('app', 'Person'),
            'series_id' => Yii::t('app', 'Series(one)'),
            'content' => Yii::t('app', 'Comment'),
            'images' => Yii::t('app', 'Photo'),
            'created_at' => Yii::t('app', 'Created at'),
            'updated_at' => Yii::t('app', 'Updated at'),
        ];
    }

    /**
     * Gets query for [[Movie]].
     *
     * @return ActiveQuery
     */
    public function getMovie(): ActiveQuery
    {
        return $this->hasOne(Movie::class, ['id' => 'movie_id']);
    }

    /**
     * Gets query for [[Person]].
     *
     * @return ActiveQuery
     */
    public function getPerson(): ActiveQuery
    {
        return $this->hasOne(Person::class, ['id' => 'person_id']);
    }

    /**
     * Gets query for [[Series]].
     *
     * @return ActiveQuery
     */
    public function getSeries(): ActiveQuery
    {
        return $this->hasOne(Series::class, ['id' => 'series_id']);
    }

    public function getTargetName(): string
    {
        if (!empty($this->movie_id)) {
            $movie = $this->getMovie()->one();
            if (empty($movie)) {
                throw new Exception('Failed to get movie');
            }

            return $movie->title;
        }

        if (!empty($this->person_id)) {
            $person = $this->getPerson()->one();
            if (empty($person)) {
                throw new Exception('Failed to get person');
            }

            return $person->getFullName();
        }

        if (!empty($this->series_id)) {
            $series = $this->getSeries()->one();
            if (empty($series)) {
                throw new Exception('Failed to get series');
            }

            return $series->title;
        }

        throw new Exception('Failed to get comment target');
    }

    public function getTargetUrl(): string
    {
        if (!empty($this->movie_id)) {
            return '/movie/details?id=' . $this->movie_id;
        } else if (!empty($this->person_id)) {
            return '/person/details?id=' . $this->person_id;
        } else if (!empty($this->series_id)) {
            return '/series/details?id=' . $this->series_id;
        }

        throw new Exception('Failed to get comment target');
    }
}
