<?php

namespace app\models;

use Exception;
use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "queue".
 *
 * @property int $id
 * @property int $movie_id
 * @property string $created_at
 *
 * @property Movie $movie
 */
class Queue extends \app\models\base\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'queue';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['movie_id', 'created_at'], 'required'],
            [['movie_id'], 'integer'],
            [['created_at'], 'safe'],
            [['movie_id'], 'exist', 'skipOnError' => true, 'targetClass' => Movie::class, 'targetAttribute' => ['movie_id' => 'id']],
            [['movie_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'movie_id' => 'Фильм',
            'created_at' => 'Created At',
        ];
    }

    public static function sortList()
    {
        return [
            'year_asc' => 'По году ↑',
            'year_desc' => 'По году ↓',
            'title_asc' => 'По названию ↑',
            'title_desc' => 'По названию ↓',
            'queue_date_asc' => 'По дате добавления ↑',
            'queue_date_desc' => 'По дате добавления ↓',
        ];
    }

    /**
     * Gets query for [[Movie]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMovie()
    {
        return $this->hasOne(Movie::class, ['id' => 'movie_id']);
    }

    public static function createQueueByMovie(int $movieId): void
    {
        if (!$movieId) {
            throw new Exception("Movie id is not correct: {$movieId}");
        }

        $queue = new self();
        $queue->movie_id = $movieId;

        if (!$queue->save()) {
            throw new Exception('Failed to save Queue: ' . $queue->getErrorsAsString());
        }
    }
    
    public static function generateUrlForRandomMovie(): string
    {
        return Url::to([
            '/queue/random',
            'genre_id' => Yii::$app->request->get('genre_id', 0),
            'award_id' => Yii::$app->request->get('award_id', 0),
            'year' => Yii::$app->request->get('year', 0),
        ]);
    }
}
