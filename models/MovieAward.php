<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "movie_award".
 *
 * @property int $id
 * @property int $award_id
 * @property int $award_nomination_id
 * @property int $movie_id
 * @property int $type
 * @property int $year
 * @property string $created_at
 * @property string|null $updated_at
 *
 * @property Award $award
 * @property AwardNomination $awardNomination
 * @property Movie $movie
 */
class MovieAward extends \app\models\base\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'movie_award';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['award_id', 'award_nomination_id', 'movie_id', 'type', 'year'], 'required'],
            [['award_id', 'award_nomination_id', 'movie_id', 'type', 'year'], 'integer'],
            [['award_id'], 'exist', 'skipOnError' => true, 'targetClass' => Award::class, 'targetAttribute' => ['award_id' => 'id']],
            [['award_nomination_id'], 'exist', 'skipOnError' => true, 'targetClass' => AwardNomination::class, 'targetAttribute' => ['award_nomination_id' => 'id']],
            [['movie_id'], 'exist', 'skipOnError' => true, 'targetClass' => Movie::class, 'targetAttribute' => ['movie_id' => 'id']],
            [['award_id', 'award_nomination_id', 'movie_id', 'type', 'year'], 'unique', 'targetAttribute' => ['award_id', 'award_nomination_id', 'movie_id', 'type', 'year']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'award_id' => 'Награда',
            'award_nomination_id' => 'Номинация',
            'movie_id' => 'Фильм',
            'type' => 'Тип',
            'year' => 'Год',
        ];
    }

    /**
     * Gets query for [[Award]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAward(): ActiveQuery
    {
        return $this->hasOne(Award::class, ['id' => 'award_id']);
    }

    /**
     * Gets query for [[AwardNomination]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAwardNomination(): ActiveQuery
    {
        return $this->hasOne(AwardNomination::class, ['id' => 'award_nomination_id']);
    }

    /**
     * Gets query for [[Movie]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMovie(): ActiveQuery
    {
        return $this->hasOne(Movie::class, ['id' => 'movie_id']);
    }
    
    /**
     * Ссылка на картинку награды в зависимости от текущей номинации
     * @return string
     */
    public function getImageUrl(): string
    {
        return '/images/award/' . $this->award->getImageByType($this->type);
    }
    
    /**
     * Информация о текущей награде для фильма
     * @return string
     */
    public function getInfo(): string
    {
        return $this->award->name . ' - ' . $this->year . ' - ' . $this->awardNomination->name;
    }
}
