<?php

namespace app\models;

use Yii;
use app\models\query\MovieQuery;

/**
 * This is the model class for table "view".
 *
 * @property int $id
 * @property int $movie_id
 * @property int $rating
 * @property string $created_at
 * @property int|null $audio_id
 * @property int|null $theater_id
 * @property int|null $special_showing_id
 * @property int|null $drink_id
 *
 * @property Audio $audio
 * @property Drink $drink
 * @property Movie $movie
 * @property SpecialShowing $specialShowing
 * @property Theater $theater
 */
class View extends \app\models\base\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'view';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['movie_id', 'created_at'], 'required'],
            [['movie_id', 'rating', 'audio_id', 'theater_id', 'special_showing_id', 'drink_id'], 'integer'],
            [['created_at'], 'safe'],
            [['audio_id'], 'exist', 'skipOnError' => true, 'targetClass' => Audio::class, 'targetAttribute' => ['audio_id' => 'id']],
            [['drink_id'], 'exist', 'skipOnError' => true, 'targetClass' => Drink::class, 'targetAttribute' => ['drink_id' => 'id']],
            [['movie_id'], 'exist', 'skipOnError' => true, 'targetClass' => Movie::class, 'targetAttribute' => ['movie_id' => 'id']],
            [['special_showing_id'], 'exist', 'skipOnError' => true, 'targetClass' => SpecialShowing::class, 'targetAttribute' => ['special_showing_id' => 'id']],
            [['theater_id'], 'exist', 'skipOnError' => true, 'targetClass' => Theater::class, 'targetAttribute' => ['theater_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'movie_id' => 'Фильм',
            'rating' => 'Рейтинг',
            'created_at' => 'Дата просмотра',
            'audio_id' => 'Озвучка',
            'theater_id' => 'Кинотеатр',
            'special_showing_id' => 'Спец. показ',
            'drink_id' => 'Напиток',
        ];
    }

    public static function sortList(): array
    {
        return [
            'view_date_asc' => 'По дате просмотра ↑',
            'view_date_desc' => 'По дате просмотра ↓',
            'view_rating_asc' => 'По рейтингу ↑',
            'view_rating_desc' => 'По рейтингу ↓',
            'year_asc' => 'По году ↑',
            'year_desc' => 'По году ↓',
            'title_asc' => 'По названию ↑',
            'title_desc' => 'По названию ↓',
            'original_title_asc' => 'По оригинальному названию ↑',
            'original_title_desc' => 'По оригинальному названию ↓',
        ];
    }

    /**
     * Gets query for [[Audio]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAudio()
    {
        return $this->hasOne(Audio::class, ['id' => 'audio_id']);
    }

    /**
     * Gets query for [[Drink]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDrink()
    {
        return $this->hasOne(Drink::class, ['id' => 'drink_id']);
    }

    /**
     * Gets query for [[Movie]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMovie()
    {
        return $this->hasOne(Movie::class, ['id' => 'movie_id']);
    }

    /**
     * Gets query for [[SpecialShowing]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSpecialShowing()
    {
        return $this->hasOne(SpecialShowing::class, ['id' => 'special_showing_id']);
    }

    /**
     * Gets query for [[Theater]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTheater()
    {
        return $this->hasOne(Theater::class, ['id' => 'theater_id']);
    }

    /**
     * Возвращает количество просмотров в промежутке между $date_start и $date_end
     * @param string $date_start Дата начала периода
     * @param string $date_end Дата окончания периода
     * @param bool $onlyTheatersViews Только просмотры в кинотеатрах
     * @return int
     */
    public static function getViewsCountByPeriod(
        string $date_start,
        string $date_end,
        bool $onlyTheatersViews = false
    ): int
    {
        $query = View::find()
            ->where(['>=', 'created_at', $date_start])
            ->andWhere(['<', 'created_at', $date_end]);

        if ($onlyTheatersViews) {
            $query->andWhere(['is not', 'theater_id', null]);
        }    

        return $query->count();
    }

    /**
     * {@inheritdoc}
     * @return MovieQuery the active query used by this AR class.
     */
    public static function find(): MovieQuery
    {
        return new MovieQuery(get_called_class());
    }
}
