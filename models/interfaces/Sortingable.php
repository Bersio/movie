<?php

namespace app\models\interfaces;

/**
 * Interface contains methods for preparing sorts
 */
interface Sortingable
{
    public function prepareSortCount(): void;

    public static function prepareSortCounts(): void;
}
