<?php

namespace app\models;

use Exception;
use Yii;
use app\models\query\AwardQuery;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "award".
 *
 * @property int $id
 * @property string $name
 * @property string|null $image_winner
 * @property string|null $image_nomination
 * @property string|null $description
 * @property string|null $web_site
 * @property string $created_at
 * @property string|null $updated_at
 *
 * @property AwardNomination[] $awardNominations
 * @property MovieAward[] $movieAwards
 * @property PersonAward[] $personAwards
 */
class Award extends \app\models\base\BaseModel
{
    public const int TYPE_NOMINATION = 0;
    public const int TYPE_WINNER = 1;

    private array $viewed_movies_id_list = [];
    private array $movies_without_view_id_list = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'award';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['name', 'image_winner', 'image_nomination', 'web_site'], 'string', 'max' => 255],
            [['name', 'image_winner', 'image_nomination', 'web_site'], 'trim'],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name' => Yii::t('app', 'Name'),
            'image_winner' => Yii::t('app', 'Picture "winner"'),
            'image_nomination' => Yii::t('app', 'Picture "nominee"'),
            'description' => Yii::t('app', 'Description'),
            'web_site' => Yii::t('app', 'Web Site'),
        ];
    }

    public static function sortList(): array
    {
        return [
            'reverse' => Yii::t('app', 'Reverse'),
            'title_asc' => Yii::t('app', 'By title ↑'),
            'title_desc' => Yii::t('app', 'By title ↓'),
        ];
    }

    public static function typeList(): array
    {
        return [
            Award::TYPE_NOMINATION => Yii::t('app', 'Nomination'),
            Award::TYPE_WINNER => Yii::t('app', 'Winner'),
        ];
    }

    /**
     * Gets query for [[AwardNominations]].
     *
     * @return ActiveQuery
     */
    public function getAwardNominations(): ActiveQuery
    {
        return $this->hasMany(AwardNomination::class, ['award_id' => 'id']);
    }

    /**
     * Gets query for [[MovieAwards]].
     *
     * @return ActiveQuery
     */
    public function getMovieAwards(): ActiveQuery
    {
        return $this->hasMany(MovieAward::class, ['award_id' => 'id']);
    }

    /**
     * Gets query for [[PersonAwards]].
     *
     * @return ActiveQuery
     */
    public function getPersonAwards(): ActiveQuery
    {
        return $this->hasMany(PersonAward::class, ['award_id' => 'id']);
    }

    public function getImageByType(int $type): string
    {
        if ($type === Award::TYPE_WINNER) {
            return $this->image_winner;
        } elseif ($type === Award::TYPE_NOMINATION) {
            return $this->image_nomination;
        }

        throw new Exception('Incorrect Award type');
    }

    public function getOldestYear(): ?int
    {
        $oldest_movie_winner = MovieAward::find()
            ->andWhere(['award_id' => $this->id])
            ->orderBy(['year' => SORT_ASC])
            ->one();

        $oldest_person_winner = PersonAward::find()
            ->andWhere(['award_id' => $this->id])
            ->orderBy(['year' => SORT_ASC])
            ->one();
        if (empty($oldest_movie_winner) && empty($oldest_person_winner)) {
            return null;
        }

        if (empty($oldest_movie_winner)) {
            return intval($oldest_person_winner->year);
        }

        if (empty($oldest_person_winner)) {
            return intval($oldest_movie_winner->year);
        }

        if ($oldest_movie_winner->year <= $oldest_person_winner->year) {
            return intval($oldest_movie_winner->year);
        } else {
            return intval($oldest_person_winner->year);
        }
    }

    /**
     * Возвращает список годов от текущего и ранее до которого у этой награды есть фильмы или персоны.
     */
    public function getYearsList(): array
    {
        $year_start = $this->getOldestYear();
        $current_year = intval((new \DateTime())->format('Y'));

        $years_list = [];
        if (!empty($year_start)) {
            for ($year_item = $current_year; $year_item >= $year_start; $year_item--) {
                $years_list[$year_item] = $year_item;
            }
            unset($year_item, $year_start, $current_year);
        }

        return $years_list;
    }

    public function getUrlForGoogleSearch(): string
    {
        return 'https://www.google.com.ua/search?q=' . urlencode($this->name . ' вики');
    }

    /**
     * Считает количество просмотренных и не просмотренных фильмов в результатах награды.
     * С учетом того, что фильмы могут повторяться в разных номинациях.
     */
    public function checkView(Movie $movie): void
    {
        if (!empty($movie->views_count)) {
            if (!in_array($movie->id, $this->viewed_movies_id_list)) {
                $this->viewed_movies_id_list[] = $movie->id;
            }
        } else {
            if (!in_array($movie->id, $this->movies_without_view_id_list)) {
                $this->movies_without_view_id_list[] = $movie->id;
            }
        }
    }

    /**
     * Возвращает количество просмотренных фильмов в результатах награды.
     */
    public function getViewedMoviesCount(): int
    {
        return count($this->viewed_movies_id_list);
    }

    /**
     * Возвращает количество не просмотренных фильмов в результатах награды.
     */
    public function getMoviesWithoutViewCount(): int
    {
        return count($this->movies_without_view_id_list);
    }

    /**
     * Возвращает список номинаций для награды отфильтрованный по типу
     * @param int $type
     * @return array
     */
    public function getNominationsForFilters(int $type): array
    {
        return AwardNomination::find()
            ->select(['name', 'id'])
            ->where(['award_id' => $this->id])
            ->andWhere(['type' => $type])
            ->orderBy(['id' => SORT_ASC])
            ->indexBy('id')
            ->column();
    }

    public static function getAwardsForViewsFilters(): array
    {
        return self::find()
            ->select(['award.name', 'award.id'])
            ->innerJoin('movie_award', 'award.id = movie_award.award_id')
            ->innerJoin('view', 'movie_award.movie_id = view.movie_id')
            ->groupBy(['award.id'])
            ->sortByDefault()
            ->indexBy('award.id')
            ->column();
    }

    public static function getAwardsForQueueFilters(): array
    {
        return self::find()
            ->select(['award.name', 'award.id'])
            ->innerJoin('movie_award', 'award.id = movie_award.award_id')
            ->innerJoin('queue', 'movie_award.movie_id = queue.movie_id')
            ->groupBy(['award.id'])
            ->sortByDefault()
            ->indexBy('award.id')
            ->column();
    }

    public static function getAwardsForPersonFilters(): array
    {
        return self::find()
            ->select(['award.name', 'award.id'])
            ->innerJoin('person_award', 'award.id = person_award.award_id')
            ->innerJoin('person', 'person_award.person_id = person.id')
            ->groupBy(['award.id'])
            ->sortByDefault()
            ->indexBy('audio.id')
            ->column();
    }

    /**
     * {@inheritdoc}
     * @return AwardQuery the active query used by this AR class.
     */
    public static function find(): AwardQuery
    {
        return new AwardQuery(get_called_class());
    }
}
