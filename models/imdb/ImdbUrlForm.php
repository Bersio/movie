<?php

namespace app\models\imdb;

use app\models\ExternalInfo;
use app\models\image\ImageUploadForm;
use app\models\Movie;
use app\models\Series;
use DOMDocument;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Masterminds\HTML5;
use yii\base\Model;

class ImdbUrlForm extends Model
{
    private const string SCENARIO_MOVIE = 'scenario_movie';
    private const string SCENARIO_SERIES = 'scenario_series';

    public ?string $url = null;

    public $localizedTitle;
    public $originalTitle;
    public $year;
    public $imgPath;
    public $rating;

    private DOMDocument $dom;

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['url', 'localizedTitle', 'originalTitle', 'year', 'imgPath'], 'required', 'on' => self::SCENARIO_MOVIE],
            [['url', 'localizedTitle', 'originalTitle', 'imgPath'], 'required', 'on' => self::SCENARIO_SERIES],
            [['url', 'localizedTitle', 'originalTitle', 'imgPath'], 'string'],
            [['year'], 'integer'],
            [['rating'], 'number'],
            [['url', 'localizedTitle', 'originalTitle', 'imgPath'], 'trim'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'url' => 'Ссылка на страницу фильма на IMDB',
        ];
    }

    public function createMovie(): Movie
    {
        $this->scenario = self::SCENARIO_MOVIE;

        $this->prepareImdbPageDom();
        $this->prepareLocalizedTitle();
        $this->prepareOriginalTitle();
        $this->prepareImgPath();
        $this->prepareYear();
        $this->prepareRating();

        if (!$this->validate()) {
            throw new Exception("Validation failed: " . json_encode($this->getErrors(), JSON_UNESCAPED_UNICODE));
        }

        $movie = new Movie([
            'original_title' => $this->originalTitle,
            'title' => $this->localizedTitle,
            'year' => $this->year,
        ]);

        // saving for id
        if (!$movie->save()) {
            throw new Exception('Failed to save Movie: ' . $movie->getErrorsAsString());
        }

        $imageUploadForm = new ImageUploadForm([
            'imageType' => ImageUploadForm::IMAGE_TYPE_URL,
            'imageUrl' => $this->imgPath,
        ]);

        $movie->image = $imageUploadForm->saveImageByUrl($movie);

        // another saving
        if (!$movie->save()) {
            throw new Exception('Failed to save Movie: ' . $movie->getErrorsAsString());
        }

        $external_info = new ExternalInfo([
            'movie_id' => $movie->id,
            'imdb_id' => $this->getImdbIdByUrl(),
            'imdb_rating' => $this->rating,
        ]);

        if (!$external_info->save()) {
            throw new Exception('Failed to save External info: ' . $external_info->getErrorsAsString());
        }

        return $movie;
    }

    public function createSeries(): Series
    {
        $this->scenario = self::SCENARIO_SERIES;

        $this->prepareImdbPageDom();
        $this->prepareLocalizedTitle();
        $this->prepareOriginalTitle();
        $this->prepareImgPath();
        $this->prepareRating();

        if (!$this->validate()) {
            throw new Exception("Validation failed: " . json_encode($this->getErrors(), JSON_UNESCAPED_UNICODE));
        }

        $series = new Series([
            'original_title' => $this->originalTitle,
            'title' => $this->localizedTitle,
        ]);

        // saving for id
        if (!$series->save()) {
            throw new Exception('Failed to save Movie: ' . $series->getErrorsAsString());
        }

        $imageUploadForm = new ImageUploadForm([
            'imageType' => ImageUploadForm::IMAGE_TYPE_URL,
            'imageUrl' => $this->imgPath,
        ]);

        $series->image = $imageUploadForm->saveImageByUrl($series);

        // another saving
        if (!$series->save()) {
            throw new Exception('Failed to save Movie: ' . $series->getErrorsAsString());
        }

        $external_info = new ExternalInfo([
            'series_id' => $series->id,
            'imdb_id' => $this->getImdbIdByUrl(),
            'imdb_rating' => $this->rating,
        ]);

        if (!$external_info->save()) {
            throw new Exception('Failed to save External info: ' . $external_info->getErrorsAsString());
        }

        return $series;
    }

    private function getImdbIdByUrl(): string
    {
        if (empty($this->url)) {
            throw new Exception('URL is empty.');
        }

        $parts = explode('/', $this->url);

        foreach ($parts as $item) {
            if (str_contains($item, 'http')) {
                continue;
            }

            if (str_contains($item, 'tt')) {
                return $item;
            }
        }

        throw new Exception('Failed to get imdb id.');
    }

    private function prepareImdbPageDom(): void
    {
        $options = [
            RequestOptions::HEADERS => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.36',
            ]
        ];

        $response = (new Client())->request('GET', $this->url, $options);
        $content = $response->getBody()->getContents();

        if (empty($content)) {
            throw new Exception("Failed to get content from IMDB by URL: {$this->url}");
        }

        $this->dom = (new HTML5())->loadHTML($content);
    }

    private function prepareLocalizedTitle(): void
    {
        $this->localizedTitle = $this->dom->getElementsByTagName('h1')->item(0)->textContent;
    }

    private function prepareOriginalTitle(): void
    {
        foreach ($this->dom->getElementsByTagName('div') as $div) {
            if (!str_starts_with($div->textContent, 'Original title: ')) {
                continue;
            }

            $this->originalTitle = str_replace('Original title: ', '', $div->textContent);

            return;
        }

        if (empty($this->originalTitle) && !empty($this->localizedTitle)) {
            $this->originalTitle = $this->localizedTitle;
        }
    }

    private function prepareImgPath(): void
    {
        foreach ($this->dom->getElementsByTagName('div') as $div) {
            if (!str_contains($div->getAttribute('class'), 'ipc-poster')) {
                continue;
            }

            foreach ($div->getElementsByTagName('img') as $img) {
                if (!str_contains($img->getAttribute('class'), 'ipc-image')) {
                    continue;
                }

                $srcSet = $img->getAttribute('srcset');
                $parts = explode(', ', $srcSet);

                foreach ($parts as $part) {
                    if (!str_contains($part, '380w')) {
                        continue;
                    }

                    $this->imgPath = str_replace(' 380w', '', $part);

                    return;
                }
            }
        }
    }

    private function prepareYear(): void
    {
        foreach ($this->dom->getElementsByTagName('a') as $a) {
            if (
                str_contains($a->getAttribute('href'), 'releaseinfo')
                && is_numeric($a->textContent)
            ) {
                $this->year = (int)$a->textContent;

                return;
            }
        }
    }

    private function prepareRating(): void
    {
        foreach ($this->dom->getElementsByTagName('a') as $a) {
            if (!str_contains($a->getAttribute('href'), '/ratings/')) {
                continue;
            }

            foreach ($a->getElementsByTagName('span') as $span) {
                if (!is_numeric($span->textContent)) {
                    continue;
                }

                $this->rating = floatval($span->textContent);

                return;
            }
        }
    }
}
