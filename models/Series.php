<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "series".
 *
 * @property int $id
 * @property string $title
 * @property string|null $original_title
 * @property string|null $image
 * @property int $is_favorite
 * @property int|null $status
 * @property string $created_at
 * @property string|null $updated_at
 *
 * @property Comment[] $comments
 */
class Series extends \app\models\base\BaseModel
{
    public const int STATUS_IN_PROGRESS = 1;
    public const int STATUS_WAITING_NEW_SEASON = 2;
    public const int STATUS_COMPLETED = 3;

    public const string STATUS_NAME_IN_PROGRESS = 'В процессе просмотра';
    public const string STATUS_NAME_WAITING_NEW_SEASON = 'Ожидание нового сезона';
    public const string STATUS_NAME_COMPLETED = 'Завершен';

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'series';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['title', 'created_at'], 'required'],
            [['is_favorite', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'original_title', 'image'], 'string', 'max' => 255],
            [['original_title', 'title', 'image'], 'trim'],
            [['title', 'original_title'], 'unique', 'targetAttribute' => ['title', 'original_title']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'original_title' => 'Оригинальное название',
            'image' => 'Постер',
            'is_favorite' => 'Избранное',
            'status' => 'Статус',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public static function sortList(): array
    {
        return [
            'title_asc' => 'По названию ↑',
            'title_desc' => 'По названию ↓',
            'original_title_asc' => 'По оригинальному названию ↑',
            'original_title_desc' => 'По оригинальному названию ↓',
        ];
    }

    public static function getAllStatusesForFilters(): array
    {
        return[
            self::STATUS_IN_PROGRESS => self::STATUS_NAME_IN_PROGRESS,
            self::STATUS_WAITING_NEW_SEASON => self::STATUS_NAME_WAITING_NEW_SEASON,
            self::STATUS_COMPLETED => self::STATUS_NAME_COMPLETED,
        ];
    }

    /**
     * Gets query for [[Comments]].
     *
     * @return ActiveQuery
     */
    public function getComments(): ActiveQuery
    {
        return $this->hasMany(Comment::class, ['series_id' => 'id']);
    }

    public function getUrlForGoogleSearch(): string
    {
        return 'https://www.google.com.ua/search?q=' . urlencode('сериал ' . $this->title . ' вики');
    }

    public function getStatusName(): string
    {
        if (empty($this->status)) {
            return '';
        }

        return Series::getAllStatusesForFilters()[$this->status];
    }
}
