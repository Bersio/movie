<?php

namespace app\models;

use Exception;
use Yii;
use app\models\query\CountryQuery;
use app\models\interfaces\Sortingable;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "country".
 *
 * @property int $id
 * @property string $code
 * @property string $en_name
 * @property string $ru_name
 * @property string $created_at
 * @property string|null $updated_at
 * @property int|null $total_movies_count
 * @property int|null $viewed_movies_count
 *
 * @property MovieCountry[] $movieCountries
 */
class Country extends \app\models\base\BaseModel implements Sortingable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'country';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['code', 'en_name', 'ru_name'], 'required'],
            [['total_movies_count', 'viewed_movies_count'], 'integer'],
            [['code'], 'string', 'max' => 2],
            [['en_name', 'ru_name'], 'string', 'max' => 255],
            [['code', 'en_name', 'ru_name'], 'trim'],
            [['code'], 'unique'],
            [['en_name'], 'unique'],
            [['ru_name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'code' => Yii::t('app', 'Code'),
            'en_name' => Yii::t('app', 'En Name'),
            'ru_name' => Yii::t('app', 'Ru Name'),
            'total_movies_count' => Yii::t('app', 'Total Movies Count'),
            'viewed_movies_count' => Yii::t('app', 'Viewed Movies Count'),
        ];
    }

    public static function sortList(): array
    {
        return [
            'title_asc' => Yii::t('app', 'By title ↑'),
            'title_desc' => Yii::t('app', 'By title ↓'),
            'viewed_movies_count_asc' => Yii::t('app', 'By number of films watched ↑'),
            'viewed_movies_count_desc' => Yii::t('app', 'By number of films watched ↓'),
            'total_movies_count_asc' => Yii::t('app', 'By total films count ↑'),
            'total_movies_count_desc' => Yii::t('app', 'By total films count ↓'),
        ];
    }

    /**
     * Gets query for [[MovieCountries]].
     *
     * @return ActiveQuery
     */
    public function getMovieCountries(): ActiveQuery
    {
        return $this->hasMany(MovieCountry::class, ['country_id' => 'id']);
    }

    public function prepareSortCount(): void
    {
        $this->total_movies_count = count($this->movieCountries);

        $this->viewed_movies_count = MovieCountry::find()
            ->innerJoin('movie', 'movie_country.movie_id = movie.id')
            ->innerJoin('view', 'movie.id = view.movie_id')
            ->where(['movie_country.country_id' => $this->id])
            ->groupBy(['movie_country.id', 'movie_country.country_id'])
            ->count();

        if (
            !$this->isAttributeChanged('total_movies_count')
            && !$this->isAttributeChanged('viewed_movies_count')
        ) {
            return;
        }

        if (!$this->save()) {
            throw new Exception('Failed to save Country: ' . $this->getErrorsAsString());
        }
    }

    public static function prepareSortCounts(): void
    {
        /** @var Country $country_item */
        foreach (Country::find()->each() as $country_item) {
            $country_item->prepareSortCount();
        }
    }

    public static function getAllCountriesForFilters(): array
    {
        return self::find()
            ->select([
                'country.ru_name',
                'country.id',
            ])
            ->sortByRuNameASC()
            ->indexBy('country.id')
            ->column();
    }

    /**
     * {@inheritdoc}
     * @return CountryQuery the active query used by this AR class.
     */
    public static function find(): CountryQuery
    {
        return new CountryQuery(get_called_class());
    }
}
