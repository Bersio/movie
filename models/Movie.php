<?php

namespace app\models;

use Exception;
use Yii;
use yii\db\ActiveQuery;
use app\models\query\MovieQuery;
use app\models\interfaces\Sortingable;

/**
 * This is the model class for table "movie".
 *
 * @property int $id
 * @property string|null $original_title
 * @property string $title
 * @property int|null $year
 * @property float|null $average_rating
 * @property string|null $image
 * @property string $created_at
 * @property string|null $updated_at
 * @property int|null $awards_winner_count
 * @property int|null $awards_nomination_count
 * @property int|null $actors_count
 * @property int|null $directors_count
 * @property int|null $person_awards_winner_count
 * @property int|null $person_awards_nomination_count
 * @property int|null $views_count
 * @property int|null $comments_count
 * @property int $is_favorite
 *
 * @property Comment[] $comments
 * @property Genre[] $genres
 * @property MovieActor[] $movieActors
 * @property Person[] $actors
 * @property Person[] $directors
 * @property MovieAward[] $movieAwards
 * @property MovieCountry[] $movieCountries
 * @property MovieDirector[] $movieDirectors
 * @property MovieGenre[] $movieGenres
 * @property PersonAward[] $personAwards
 * @property Queue $queue
 * @property View[] $views
 */
class Movie extends \app\models\base\BaseModel implements Sortingable
{
    public const string SORT_TYPE_MOVIE = 'movie';
    public const string SORT_TYPE_VIEW = 'view';
    public const string SORT_TYPE_QUEUE = 'queue';

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'movie';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['title', 'created_at'], 'required'],
            [['year', 'awards_winner_count', 'awards_nomination_count', 'actors_count', 'directors_count', 'person_awards_winner_count', 'person_awards_nomination_count', 'views_count', 'comments_count'], 'integer'],
            [['is_favorite'], 'boolean'],
            [['average_rating'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['original_title', 'title', 'image'], 'string', 'max' => 255],
            [['original_title', 'title', 'image'], 'trim'],
            [['original_title', 'title', 'year'], 'unique', 'targetAttribute' => ['original_title', 'title', 'year']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'original_title' => 'Оригинальное название',
            'title' => 'Название',
            'year' => 'Год',
            'average_rating' => 'Average Rating',
            'image' => 'Постер',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'awards_winner_count' => 'Awards Winner Count',
            'awards_nomination_count' => 'Awards Nomination Count',
            'actors_count' => 'Actors Count',
            'directors_count' => 'Directors Count',
            'person_awards_winner_count' => 'Person Awards Winner Count',
            'person_awards_nomination_count' => 'Person Awards Nomination Count',
            'views_count' => 'Views Count',
            'comments_count' => 'Comments Count',
            'is_favorite' => 'Избранное',
        ];
    }

    public static function sortList(): array
    {
        return [
            'title_asc' => 'По названию ↑',
            'title_desc' => 'По названию ↓',
            'original_title_asc' => 'По оригинальному названию ↑',
            'original_title_desc' => 'По оригинальному названию ↓',
            'year_asc' => 'По году ↑',
            'year_desc' => 'По году ↓',
        ];
    }

    /**
     * Gets query for [[Comments]].
     *
     * @return ActiveQuery
     */
    public function getComments(): ActiveQuery
    {
        return $this->hasMany(Comment::class, ['movie_id' => 'id']);
    }

    /**
     * Gets query for [[MovieActors]].
     *
     * @return ActiveQuery
     */
    public function getMovieActors(): ActiveQuery
    {
        return $this->hasMany(MovieActor::class, ['movie_id' => 'id']);
    }

    /**
     * Gets query for [[Actors]].
     *
     * @return ActiveQuery
     */
    public function getActors(): ActiveQuery
    {
        return $this->hasMany(Person::class, ['id' => 'person_id'])->viaTable('movie_actor', ['movie_id' => 'id']);
    }

    /**
     * Gets query for [[Directors]].
     *
     * @return ActiveQuery
     */
    public function getDirectors(): ActiveQuery
    {
        return $this->hasMany(Person::class, ['id' => 'person_id'])->viaTable('movie_director', ['movie_id' => 'id']);
    }

    /**
     * Gets query for [[MovieAwards]].
     *
     * @return ActiveQuery
     */
    public function getMovieAwards(): ActiveQuery
    {
        return $this
            ->hasMany(MovieAward::class, ['movie_id' => 'id'])
            ->orderBy([
                'type' => SORT_DESC,
                'id' => SORT_ASC,
            ]);
    }

    /**
     * Gets query for [[MovieDirectors]].
     *
     * @return ActiveQuery
     */
    public function getMovieDirectors(): ActiveQuery
    {
        return $this->hasMany(MovieDirector::class, ['movie_id' => 'id']);
    }

    /**
     * Gets query for [[MovieGenres]].
     *
     * @return ActiveQuery
     */
    public function getMovieGenres(): ActiveQuery
    {
        return $this->hasMany(MovieGenre::class, ['movie_id' => 'id']);
    }

    /**
     * Gets query for [[MovieCountries]].
     *
     * @return ActiveQuery
     */
    public function getMovieCountries(): ActiveQuery
    {
        return $this->hasMany(MovieCountry::class, ['movie_id' => 'id']);
    }

    /**
     * Gets query for [[Genres]].
     *
     * @return ActiveQuery
     */
    public function getGenres(): ActiveQuery
    {
        return $this->hasMany(Genre::class, ['id' => 'genre_id'])->viaTable('movie_genre', ['movie_id' => 'id']);
    }

    /**
     * Gets query for [[PersonAwards]].
     *
     * @return ActiveQuery
     */
    public function getPersonAwards(): ActiveQuery
    {
        return $this
            ->hasMany(PersonAward::class, ['movie_id' => 'id'])
            ->orderBy([
                'type' => SORT_DESC,
                'id' => SORT_ASC,
            ]);
    }

    /**
     * Gets query for [[Queue]].
     *
     * @return ActiveQuery
     */
    public function getQueue(): ActiveQuery
    {
        return $this->hasOne(Queue::class, ['movie_id' => 'id']);
    }

    /**
     * Gets query for [[Views]].
     *
     * @return ActiveQuery
     */
    public function getViews(): ActiveQuery
    {
        return $this->hasMany(View::class, ['movie_id' => 'id']);
    }

    public function getUrlForGoogleSearch(): string
    {
        return 'https://www.google.com.ua/search?q=' .
            urlencode('фильм ' . $this->title . ' ' . $this->year . ' вики');
    }

    /**
     * Все фильмы, у которых есть просмотры
     * @return array
     */
    public static function getMoviesWithView(): array
    {
        return Movie::find()
            ->innerJoin('view', 'movie.id = view.movie_id')
            ->groupBy(['movie.id'])
            ->orderBy(['movie.title' => SORT_ASC])
            ->all();
    }

    /**
     * Все фильмы, без просмотров
     * @return array
     */
    public static function getMoviesWithoutView(): array
    {
        return Movie::find()
            ->join('LEFT OUTER JOIN', 'view', 'movie.id = view.movie_id')
            ->where(['is', 'view.movie_id', null])
            ->groupBy(['movie.id'])
            ->orderBy(['movie.title' => SORT_ASC])
            ->all();
    }

    /**
     * Фильмы без просмотра и которые не стоят в очереди
     * @return array
     */
    public static function getMoviesForQueue(): array
    {
        return Movie::find()
            ->select(['movie.title', 'movie.id'])
            ->join('LEFT OUTER JOIN', 'view', 'movie.id = view.movie_id')
            ->join('LEFT OUTER JOIN', 'queue', 'movie.id = queue.movie_id')
            ->where(['is', 'view.movie_id', null])
            ->andWhere(['is', 'queue.movie_id', null])
            ->groupBy(['movie.id'])
            ->indexBy('movie.id')
            ->orderBy(['movie.title' => SORT_ASC])
            ->column();
    }

    /**
     * Устанавливает параметры для сортировки фильмов
     * @return void
     * @throws Exception
     */
    public function prepareSortCount(): void
    {
        $this->views_count = View::find()
            ->where(['movie_id' => $this->id])
            ->count();

        $this->actors_count = MovieActor::find()
            ->where(['movie_id' => $this->id])
            ->count();

        $this->directors_count = MovieDirector::find()
            ->where(['movie_id' => $this->id])
            ->count();

        $this->awards_winner_count = MovieAward::find()
            ->where(['movie_id' => $this->id])
            ->andWhere(['type' => Award::TYPE_WINNER])
            ->count();

        $this->awards_nomination_count = MovieAward::find()
            ->where(['movie_id' => $this->id])
            ->andWhere(['type' => Award::TYPE_NOMINATION])
            ->count();

        $this->comments_count = Comment::find()
            ->where(['movie_id' => $this->id])
            ->count();

        $this->person_awards_winner_count = 0;
        $this->person_awards_nomination_count = 0;

        foreach (MovieActor::find()->where(['movie_id' => $this->id])->each() as $movie_actor_item) {
            $this->person_awards_winner_count += PersonAward::find()
                ->where(['person_id' => $movie_actor_item->person_id])
                ->andWhere(['type' => Award::TYPE_WINNER])
                ->count();

            $this->person_awards_nomination_count += PersonAward::find()
                ->where(['person_id' => $movie_actor_item->person_id])
                ->andWhere(['type' => Award::TYPE_NOMINATION])
                ->count();
        }
        unset($movie_actor_item);

        foreach (MovieDirector::find()->where(['movie_id' => $this->id])->each() as $movie_director_item) {
            $this->person_awards_winner_count += PersonAward::find()
                ->where(['person_id' => $movie_director_item->person_id])
                ->andWhere(['type' => Award::TYPE_WINNER])
                ->count();

            $this->person_awards_nomination_count += PersonAward::find()
                ->where(['person_id' => $movie_director_item->person_id])
                ->andWhere(['type' => Award::TYPE_NOMINATION])
                ->count();
        }
        unset($movie_director_item);

        if (
            !$this->isAttributeChanged('views_count')
            && !$this->isAttributeChanged('actors_count')
            && !$this->isAttributeChanged('directors_count')
            && !$this->isAttributeChanged('awards_winner_count')
            && !$this->isAttributeChanged('awards_nomination_count')
            && !$this->isAttributeChanged('comments_count')
            && !$this->isAttributeChanged('person_awards_winner_count')
            && !$this->isAttributeChanged('person_awards_nomination_count')
        ) {
            return;
        }

        if (!$this->save()) {
            throw new Exception('Failed to save Movie: ' . $this->getErrorsAsString());
        }
    }

    /**
     * Устанавливает средний рейтинг фильма на основе рейтингов просмотров
     * @return void
     * @throws Exception
     */
    public function prepareAverageRating(): void
    {
        $sum = 0;
        $counter = 0;

        foreach ($this->views as $view_item) {
            $counter++;
            $sum += $view_item->rating;
        }
        unset($view_item);

        if ($counter <= 0) {
            return;
        }

        $this->average_rating = (float)($sum / $counter);

        if (!$this->isAttributeChanged('average_rating')) {
            return;
        }

        if (!$this->save()) {
            throw new Exception('Failed to save Movie: ' . $this->getErrorsAsString());
        }
    }

    /**
     * Обновляет параметры для сортировки для всех фильмов
     * @return void
     */
    public static function prepareSortCounts(): void
    {
        /** @var Movie $movie_item */
        foreach (Movie::find()->each() as $movie_item) {
            $movie_item->prepareSortCount();
        }
    }

    /**
     * Обновляет средние рейтинги для всех фильмов
     * @param int|null $id ID фильма
     * @return void
     */
    public static function prepareAverageRatings(?int $id = null): void
    {
        $query = Movie::find()->with(['views']);

        if ($id) {
            $query->where(['movie.id' => $id]);
        }

        /** @var Movie $movie_item */
        foreach ($query->each() as $movie_item) {
            $movie_item->prepareAverageRating();
        }
    }

    /**
     * Возвращает список всех фильмов в формате для выпадающих списков
     * @return array
     */
    public static function getAllMoviesForFilters(): array
    {
        return self::find()
            ->select([
                'movie.title',
                'movie.id',
                'movie.awards_winner_count',
                'movie.awards_nomination_count',
                'movie.person_awards_winner_count',
                'movie.person_awards_nomination_count',
                'movie.actors_count',
                'movie.directors_count',
                'movie.comments_count',
                'movie.created_at',
            ])
            ->sortMoviesByTitleASC()
            ->indexBy('movie.id')
            ->column();
    }

    public static function getYearsForQueueFilters(): array
    {
        return self::find()
            ->select(['movie.year'])
            ->innerJoin('queue', 'movie.id = queue.movie_id')
            ->groupBy(['movie.year'])
            ->orderBy(['movie.year' => SORT_DESC])
            ->indexBy('movie.year')
            ->column();
    }

    /**
     * {@inheritdoc}
     * @return MovieQuery the active query used by this AR class.
     */
    public static function find(): MovieQuery
    {
        return new MovieQuery(get_called_class());
    }
}
