<?php

namespace app\models\image;

use app\models\Series;
use Exception;
use GuzzleHttp\Client;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use app\models\Award;
use app\models\Movie;
use app\models\Person;

class ImageUploadForm extends Model
{
    public const IMAGE_TYPE_URL = 'url';
    public const IMAGE_TYPE_FILE = 'file';

    public $imageType;
    public $imageFile;
    public $imageUrl;

    public $typesList = [
        ImageUploadForm::IMAGE_TYPE_URL => 'URL',
        ImageUploadForm::IMAGE_TYPE_FILE => 'File'
    ];

    public function rules()
    {
        return [
            [['imageType'], 'string', 'max' => 4],
            [['imageFile'], 'image', 'mimeTypes' => 'image/*'],
            [['imageUrl'], 'string', 'max' => 255],
            [['imageUrl'], 'trim'],
        ];
    }

    public function upload(Model $model, $index = null)
    {
        if ($this->imageType === ImageUploadForm::IMAGE_TYPE_URL) {
            return $this->saveImageByUrl($model);
        } elseif ($this->imageType === ImageUploadForm::IMAGE_TYPE_FILE) {
            return $this->saveImageByFile($model, $index);
        }

        throw new Exception('Image type is not correct');
    }

    public function saveImageByUrl(Model $model): string
    {
        if (!$this->validate()) {
            throw new Exception('Image form is not valid: ' . json_encode($this->getErrors()));
        }

        if (empty($this->imageUrl)) {
            throw new Exception('Image url is not correct');
        }

        $image_content = (new Client())
            ->request('GET', $this->imageUrl)
            ->getBody()
            ->getContents();
        if (empty($image_content)) {
            throw new Exception('Failed to get image content');
        }

        $images_folder = $this->prepareImagesFolder($model);

        $file_name = $this->prepareFileName($model);

        $file_path = $images_folder . '/' . $file_name;

        $put_result = file_put_contents($file_path, $image_content);
        if (empty($put_result)) {
            throw new Exception("Failed to save image file: $file_path");
        }

        return $file_name;
    }

    private function saveImageByFile(Model $model, $index = null): string
    {
        $attribute = empty($index) ? 'imageFile' : "[$index]imageFile";
        $this->imageFile = UploadedFile::getInstance($this, $attribute);

        if (!$this->validate()) {
            throw new Exception('Image form is not valid: ' . json_encode($this->getErrors()));
        }

        if (empty($this->imageFile)) {
            throw new Exception('Image file is empty');
        }

        $images_folder = $this->prepareImagesFolder($model);

        $file_name = $this->prepareFileName($model);

        $file_path = $images_folder . '/' . $file_name;

        $save_result = $this->imageFile->saveAs($file_path);
        if (!$save_result) {
            throw new Exception("Failed to save file: $file_path");
        }

        return $file_name;
    }

    private function prepareImagesFolder(Model $model): string
    {
        if ($model instanceof Award) {
            $model_type = 'award';
        } elseif ($model instanceof Movie) {
            $model_type = 'movie';
        } elseif ($model instanceof Person) {
            $model_type = 'person';
        } elseif ($model instanceof Series) {
            $model_type = 'series';
        } else {
            throw new Exception('Model is not correct');
        }

        $images_folder = Yii::getAlias('@webroot') . '/images/' . $model_type;
        if (!is_dir($images_folder)) {
            throw new Exception('Images folder doesn\'t exist');
        }

        $current_model_image_folder = $images_folder . '/' . $model->id;
        if (!is_dir($current_model_image_folder)) {
            if (!mkdir($current_model_image_folder)) {
                throw new Exception("Failed to create image folder $current_model_image_folder");
            }
        }

        if (!is_dir($current_model_image_folder)) {
            throw new Exception('Image folder for this model doesn\'t exist');
        }

        return $images_folder;
    }

    private function prepareFileName(Model $model): string
    {
        return $model->id . '/' . time() . '-' . uniqid() . '.' . $this->prepareFileExtension();
    }

    private function prepareFileExtension(): string
    {
        $extension = null;

        if ($this->imageType === ImageUploadForm::IMAGE_TYPE_URL) {
            $dot_position = strrpos($this->imageUrl, '.');
            if (empty($dot_position) || $dot_position > strlen($this->imageUrl)) {
                throw new Exception('Failed to get dot position');
            }

            $extension = substr($this->imageUrl, $dot_position + 1);
            if (empty($extension)) {
                throw new Exception('Failed to get image extension');
            }
        } elseif ($this->imageType === ImageUploadForm::IMAGE_TYPE_FILE) {
            $extension = $this->imageFile->extension;
        } else {
            throw new Exception('Image type is not correct');
        }

        return $extension;
    }
}
