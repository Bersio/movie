<?php

namespace app\models\image;

use Exception;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class CommentImagesUploadForm extends Model
{
    public $imageFiles;

    public function rules(): array
    {
        return [
            [['imageFiles'], 'image', 'mimeTypes' => 'image/*', 'maxFiles' => 10],
        ];
    }

    public function upload(Model $model): array
    {
        $this->imageFiles = UploadedFile::getInstances($this, 'imageFiles');

        if (!$this->validate()) {
            return [];
        }

        $result = [];

        foreach ($this->imageFiles as $file) {
            $result[] = $this->saveImageByFile($model, $file);
        }

        return $result;
    }

    private function saveImageByFile(Model $model, $file): string
    {
        $images_folder = $this->prepareImagesFolder($model);
        $file_name = $this->prepareFileName($file);
        $file_path = $images_folder . '/' . $model->id . '/' . $file_name;

        if (!$file->saveAs($file_path)) {
            throw new Exception("Failed to save file $file_path");
        }

        return $file_name;
    }

    private function prepareImagesFolder(Model $model): string
    {
        $model_type = 'comment';

        $images_folder = Yii::getAlias('@webroot') . '/images/' . $model_type;
        if (!is_dir($images_folder)) {
            if (!mkdir($images_folder)) {
                throw new Exception("Failed to create images folder $images_folder");
            }
        }

        $current_model_image_folder = $images_folder . '/' . $model->id;
        if (!is_dir($current_model_image_folder)) {
            if (!mkdir($current_model_image_folder)) {
                throw new Exception("Failed to create image folder $current_model_image_folder");
            }
        }

        if (!is_dir($current_model_image_folder)) {
            throw new Exception('Image folder for this model doesn\'t exist');
        }

        return $images_folder;
    }

    private function prepareFileName($file): string
    {
        return time() . '-' . uniqid() . '.' . $file->extension;
    }
}
