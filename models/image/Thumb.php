<?php

namespace app\models\image;

use app\models\base\BaseModel;
use Exception;
use GdImage;
use Yii;

class Thumb
{
    public const WIDTH_MIN = 256;
    public const WIDTH_BIG = 800;

    public const WIDTH_ALL = [self::WIDTH_MIN, self::WIDTH_BIG];

    public static function getMin(BaseModel $model, string $image_name): string
    {
        return self::get(self::WIDTH_MIN, $model, $image_name);
    }

    public static function getBig(BaseModel $model, string $image_name): string
    {
        return self::get(self::WIDTH_BIG, $model, $image_name);
    }

    public static function get(int $width, BaseModel $model, string $image_name): string
    {
        $file_uri = self::getUri($width, $model, $image_name);
        $file_path = self::getFilePath($file_uri);

        if (!file_exists($file_path)) {
            self::create($width, $model, $image_name);
        }

        return $file_uri;
    }

    public static function create(int $width, BaseModel $model, string $image_name): void
    {
        $origin_file_path = self::getOriginFilePath($model, $image_name);
        if (!file_exists($origin_file_path)) {
            throw new Exception('Image file does not exists');
        }

        $gd_image = self::getGdImage($origin_file_path);
        if (!$gd_image) {
            throw new Exception('Failed to get GD Image');
        }

        $gd_image = self::scale($gd_image, $width);
        $gd_image = self::fixOrientation($origin_file_path, $gd_image);

        self::prepareFolder($model);

        $thumb_file_uri = self::getUri($width, $model, $image_name);
        $thumb_file_path = self::getFilePath($thumb_file_uri);

        if (!imagewebp($gd_image, $thumb_file_path, 100)) {
            throw new Exception("Failed to save thumb GD Image: $thumb_file_path");
        }
    }

    public static function getAbsoluteFolder(BaseModel $model): string
    {
        return Yii::getAlias('@webroot') . self::getFolder($model);
    }

    private static function getFolder(BaseModel $model): string
    {
        return '/assets/thumbs/' . $model->getImageFolderName() . '/' . $model->id;
    }

    private static function getFilePath(string $uri): string
    {
        return Yii::getAlias('@webroot') . $uri;
    }

    private static function getUri(int $width, BaseModel $model, string $image_name): string
    {
        return self::getFolder($model) . '/' . $width . '_' . md5($image_name) . '.webp';
    }

    private static function getOriginFilePath(BaseModel $model, string $image_name): string
    {
        return Yii::getAlias('@webroot') . self::getOriginUri($model, $image_name);
    }

    public static function getOriginUri(BaseModel $model, string $image_name): string
    {
        return '/images/' . $model->getImageFolderName() . '/' . $model->id . '/' . $image_name;
    }

    private static function fixOrientation(string $origin_file_path, GdImage $image): GdImage
    {
        try {
            $exif = exif_read_data($origin_file_path);
        } catch (Exception $ex) {
            return $image;
        }

        if (empty($exif['Orientation'])) {
            return $image;
        }

        switch ($exif['Orientation']) {
            case 8:
                $image = imagerotate($image, 90, 0);
                break;
            case 3:
                $image = imagerotate($image, 180, 0);
                break;
            case 6:
                $image = imagerotate($image, -90, 0);
                break;
        }

        return $image;
    }

    private static function scale(GdImage $image, int $width): GdImage
    {
        $image = imagescale($image, $width);
        if (!$image) {
            throw new Exception('Failed to scale GD Image');
        }

        return $image;
    }

    private static function prepareFolder(BaseModel $model): void
    {
        $thumb_folder = self::getAbsoluteFolder($model);
        if (is_dir($thumb_folder)) {
            return;
        }

        if (!mkdir($thumb_folder, 0777, true)) {
            throw new Exception('Failed to create folder for thumb');
        }
    }

    private static function getGdImage(string $file_path): GdImage
    {
        $extension = pathinfo($file_path, PATHINFO_EXTENSION);
        if (!$extension) {
            throw new Exception('Failed to get file extension');
        }

        $extension = strtolower($extension);

        switch ($extension) {
            case 'jpg':
            case 'jpeg':
                return imagecreatefromjpeg($file_path);
            case 'png':
                return imagecreatefrompng($file_path);
            case 'gif':
                return imagecreatefromgif($file_path);
            case 'webp':
                return imagecreatefromwebp($file_path);
        }

        throw new Exception('Invalid image format');
    }
}
