<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "external_info".
 *
 * @property int $id
 * @property int|null $movie_id
 * @property int|null $person_id
 * @property int|null $series_id
 * @property string|null $imdb_id
 * @property float|null $imdb_rating
 * @property string $created_at
 * @property string|null $updated_at
 *
 * @property Movie $movie
 * @property Person $person
 * @property Series $series
 */
class ExternalInfo extends \app\models\base\BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'external_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['movie_id', 'person_id', 'series_id'], 'integer'],
            [['imdb_rating'], 'number'],
            [['created_at'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['imdb_id'], 'string', 'max' => 255],
            [['movie_id'], 'exist', 'skipOnError' => true, 'targetClass' => Movie::class, 'targetAttribute' => ['movie_id' => 'id']],
            [['person_id'], 'exist', 'skipOnError' => true, 'targetClass' => Person::class, 'targetAttribute' => ['person_id' => 'id']],
            [['series_id'], 'exist', 'skipOnError' => true, 'targetClass' => Series::class, 'targetAttribute' => ['series_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'movie_id' => 'Movie ID',
            'person_id' => 'Person ID',
            'series_id' => 'Series ID',
            'imdb_id' => 'Imdb ID',
            'imdb_rating' => 'Imdb Rating',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * Gets query for [[Movie]].
     *
     * @return ActiveQuery
     */
    public function getMovie(): ActiveQuery
    {
        return $this->hasOne(Movie::class, ['id' => 'movie_id']);
    }

    /**
     * Gets query for [[Person]].
     *
     * @return ActiveQuery
     */
    public function getPerson(): ActiveQuery
    {
        return $this->hasOne(Person::class, ['id' => 'person_id']);
    }

    /**
     * Gets query for [[Series]].
     *
     * @return ActiveQuery
     */
    public function getSeries(): ActiveQuery
    {
        return $this->hasOne(Series::class, ['id' => 'series_id']);
    }
}
