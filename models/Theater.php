<?php

namespace app\models;

use Exception;
use Yii;
use app\models\query\TheaterQuery;
use app\models\interfaces\Sortingable;

/**
 * This is the model class for table "theater".
 *
 * @property int $id
 * @property string $name
 * @property string|null $address
 * @property string|null $web_site
 * @property string $created_at
 * @property string|null $updated_at
 * @property int|null $viewed_movies_count
 * @property int $status
 *
 * @property View[] $views
 */
class Theater extends \app\models\base\BaseModel implements Sortingable
{
    public const int STATUS_OK = 1;
    public const int STATUS_TEMPORARY_CLOSED = 2;
    public const int STATUS_CLOSED_FOREVER = 3;

    private const array STATUS_NAMES = [
        self::STATUS_OK => 'Активный',
        self::STATUS_TEMPORARY_CLOSED => 'Временно не работает',
        self::STATUS_CLOSED_FOREVER => 'Закрыто навсегда',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'theater';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'created_at'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['viewed_movies_count', 'status'], 'integer'],
            [['name', 'address', 'web_site'], 'string', 'max' => 255],
            [['name', 'address', 'web_site'], 'trim'],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'address' => 'Адрес',
            'web_site' => 'Сайт',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'viewed_movies_count' => 'Viewed Movies Count',
            'status' => 'Status',
        ];
    }

    public static function sortList()
    {
        return [
            'title_asc' => 'По названию ↑',
            'title_desc' => 'По названию ↓',
            'viewed_movies_count_asc' => 'По количеству просмотренных фильмов ↑',
            'viewed_movies_count_desc' => 'По количеству просмотренных фильмов ↓',
        ];
    }

    /**
     * Gets query for [[Views]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getViews()
    {
        return $this->hasMany(View::class, ['theater_id' => 'id']);
    }

    public static function getStatusList(): array
    {
        return [
            Theater::STATUS_OK => self::STATUS_NAMES[self::STATUS_OK],
            Theater::STATUS_TEMPORARY_CLOSED => self::STATUS_NAMES[self::STATUS_TEMPORARY_CLOSED],
            Theater::STATUS_CLOSED_FOREVER => self::STATUS_NAMES[self::STATUS_CLOSED_FOREVER]
        ];
    }

    public function getLabel(): string
    {
        $cssClass = null;
        $message = null;

        switch ($this->status) {
            case Theater::STATUS_TEMPORARY_CLOSED:
                $cssClass = 'warning';
                $message = self::STATUS_NAMES[self::STATUS_TEMPORARY_CLOSED];

                break;
            case Theater::STATUS_CLOSED_FOREVER:
                $cssClass = 'danger';
                $message = self::STATUS_NAMES[self::STATUS_CLOSED_FOREVER];

                break;
        }

        if (!$cssClass) {
            return '';
        }

        return "<span class=\"theater-status-label $cssClass\">$message</span>";
    }

    public function prepareFlashStatus(): void
    {
        $type = null;
        $message = null;

        switch ($this->status) {
            case Theater::STATUS_TEMPORARY_CLOSED:
                $type = 'warning';
                $message = self::STATUS_NAMES[self::STATUS_TEMPORARY_CLOSED];

                break;
            case Theater::STATUS_CLOSED_FOREVER:
                $type = 'danger';
                $message = self::STATUS_NAMES[self::STATUS_CLOSED_FOREVER];

                break;
        }

        if (!$type) {
            return;
        }

        Yii::$app->session->setFlash($type, $message);
    }

    public function getUrlForGoogleSearch()
    {
        return 'https://www.google.com.ua/search?q=' .
                urlencode('кинотеатр ' . $this->name . ' вики');
    }

    public function prepareSortCount(): void
    {
        $this->viewed_movies_count = View::find()
                ->where(['theater_id' => $this->id])
                ->count();

        if (!$this->isAttributeChanged('viewed_movies_count')) {
            return;
        }

        if (!$this->save()) {
            throw new Exception('Failed to save Theater: ' . $this->getErrorsAsString());
        }
    }

    /**
     * Обновляет параметры для сортировки для всех кинотеатров
     * @return void
     */
    public static function prepareSortCounts(): void
    {
        /** @var Theater $theater_item */
        foreach (self::find()->each() as $theater_item) {
            $theater_item->prepareSortCount();
        }
    }

    public static function getTheatersForViewsFilters(): array
    {
        $result = self::find()
            ->select([
                'theater.name',
                'theater.id',
                'theater.viewed_movies_count',
                'theater.created_at',
            ])
            ->innerJoin('view', 'theater.id = view.theater_id')
            ->groupBy(['theater.id'])
            ->sortByDefault()
            ->indexBy('theater.id')
            ->column();

        $result[-1] = 'Любой';

         return $result;
    }

    public static function getAllTheatersForFilters(): array
    {
        return self::find()
            ->select([
                'theater.name',
                'theater.id',
                'theater.viewed_movies_count',
            ])
            ->sortByDefault()
            ->indexBy('theater.id')
            ->column();
    }

    /**
     * {@inheritdoc}
     * @return TheaterQuery the active query used by this AR class.
     */
    public static function find(): TheaterQuery
    {
        return new TheaterQuery(get_called_class());
    }
}
