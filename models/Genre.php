<?php

namespace app\models;

use Exception;
use Yii;
use app\models\query\GenreQuery;
use app\models\interfaces\Sortingable;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "genre".
 *
 * @property int $id
 * @property string $title
 * @property string $created_at
 * @property string|null $updated_at
 * @property int|null $total_movies_count
 * @property int|null $viewed_movies_count
 * @property int|null $movie_awards_winner_count
 * @property int|null $movie_awards_nomination_count
 *
 * @property MovieGenre[] $movieGenres
 */
class Genre extends \app\models\base\BaseModel implements Sortingable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'genre';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['title', 'created_at'], 'required'],
            [['created_at'], 'safe'],
            [['total_movies_count', 'viewed_movies_count', 'movie_awards_winner_count', 'movie_awards_nomination_count'], 'integer'],
            [['total_movies_count', 'viewed_movies_count', 'movie_awards_winner_count', 'movie_awards_nomination_count'], 'default', 'value' => 0],
            [['title'], 'string', 'max' => 255],
            [['title'], 'trim'],
            [['title'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'title' => Yii::t('app', 'Name'),
            'created_at' => Yii::t('app', 'Created at'),
            'total_movies_count' => Yii::t('app', 'Total Movies Count'),
            'viewed_movies_count' => Yii::t('app', 'Viewed Movies Count'),
            'movie_awards_winner_count' => Yii::t('app', 'Movie Awards Winner Count'),
            'movie_awards_nomination_count' => Yii::t('app', 'Movie Awards Nomination Count'),
        ];
    }

    public static function sortList(): array
    {
        return [
            'title_asc' => Yii::t('app', 'By title ↑'),
            'title_desc' => Yii::t('app', 'By title ↓'),
            'total_movies_count_asc' => Yii::t('app', 'By total films count ↑'),
            'total_movies_count_desc' => Yii::t('app', 'By total films count ↓'),
            'viewed_movies_count_asc' => Yii::t('app', 'By number of films watched ↑'),
            'viewed_movies_count_desc' => Yii::t('app', 'By number of films watched ↓'),
        ];
    }

    /**
     * Gets query for [[MovieGenres]].
     *
     * @return ActiveQuery
     */
    public function getMovieGenres(): ActiveQuery
    {
        return $this->hasMany(MovieGenre::class, ['genre_id' => 'id']);
    }

    public function getMovieCount(): int
    {
        return $this->getMovieGenres()->count();
    }

    public function getUrlForGoogleSearch(): string
    {
        return 'https://www.google.com.ua/search?q=' . urlencode('жанр ' . $this->title . ' вики');
    }

    public function prepareSortCount(): void
    {
        $this->total_movies_count = MovieGenre::find()
            ->where(['genre_id' => $this->id])
            ->count();

        $this->viewed_movies_count = MovieGenre::find()
            ->innerJoin('movie', 'movie_genre.movie_id = movie.id')
            ->innerJoin('view', 'movie.id = view.movie_id')
            ->where(['movie_genre.genre_id' => $this->id])
            ->groupBy(['movie_genre.id'])
            ->count();

        $this->movie_awards_winner_count = 0;
        $this->movie_awards_nomination_count = 0;

        foreach (MovieGenre::find()->where(['genre_id' => $this->id])->each() as $movie_genre_item) {
            $this->movie_awards_winner_count += $movie_genre_item->movie
                ->getMovieAwards()
                ->where(['type' => Award::TYPE_WINNER])
                ->count();

            $this->movie_awards_winner_count += $movie_genre_item->movie
                ->getPersonAwards()
                ->where(['type' => Award::TYPE_WINNER])
                ->count();

            $this->movie_awards_nomination_count += $movie_genre_item->movie
                ->getMovieAwards()
                ->where(['type' => Award::TYPE_NOMINATION])
                ->count();

            $this->movie_awards_nomination_count += $movie_genre_item->movie
                ->getPersonAwards()
                ->where(['type' => Award::TYPE_NOMINATION])
                ->count();
        }
        unset($movie_genre_item);

        if (
            !$this->isAttributeChanged('total_movies_count')
            && !$this->isAttributeChanged('viewed_movies_count')
            && !$this->isAttributeChanged('movie_awards_winner_count')
            && !$this->isAttributeChanged('movie_awards_nomination_count')
        ) {
            return;
        }

        if (!$this->save()) {
            throw new Exception('Failed to save Genre: ' . $this->getErrorsAsString());
        }
    }

    public static function prepareSortCounts(): void
    {
        /** @var Genre $genre_item */
        foreach (Genre::find()->each() as $genre_item) {
            $genre_item->prepareSortCount();
        }
    }

    public static function getAllGenresForFilters(): array
    {
        return self::find()
            ->select([
                'genre.title',
                'genre.id',
            ])
            ->sortByTitleASC()
            ->indexBy('genre.id')
            ->column();
    }

    public static function getGenresForViewsFilters(): array
    {
        return self::find()
            ->select([
                'genre.title',
                'genre.id',
                'genre.viewed_movies_count',
                'genre.movie_awards_winner_count',
                'genre.movie_awards_nomination_count',
                'genre.total_movies_count',
                'genre.created_at',
            ])
            ->innerJoin('movie_genre', 'genre.id = movie_genre.genre_id')
            ->innerJoin('view', 'movie_genre.movie_id = view.movie_id')
            ->groupBy(['genre.id'])
            ->sortByDefault()
            ->indexBy('genre.id')
            ->column();
    }

    public static function getGenresForQueueFilters(): array
    {
        return self::find()
            ->select([
                'genre.title',
                'genre.id',
                'genre.viewed_movies_count',
                'genre.movie_awards_winner_count',
                'genre.movie_awards_nomination_count',
                'genre.total_movies_count',
                'genre.created_at',
            ])
            ->innerJoin('movie_genre', 'genre.id = movie_genre.genre_id')
            ->innerJoin('queue', 'movie_genre.movie_id = queue.movie_id')
            ->groupBy(['genre.id'])
            ->sortByDefault()
            ->indexBy('genre.id')
            ->column();
    }

    /**
     * {@inheritdoc}
     * @return GenreQuery the active query used by this AR class.
     */
    public static function find(): GenreQuery
    {
        return new GenreQuery(get_called_class());
    }
}
