<?php

namespace app\models;

use Exception;
use Yii;
use app\models\query\PersonQuery;
use app\models\interfaces\Sortingable;

/**
 * This is the model class for table "person".
 *
 * @property int $id
 * @property string $first_name
 * @property string|null $last_name
 * @property string|null $birthday
 * @property int|null $sex
 * @property string|null $image
 * @property string $created_at
 * @property string|null $updated_at
 * @property int|null $total_movies_count
 * @property int|null $viewed_movies_count
 * @property int|null $awards_winner_count
 * @property int|null $awards_nomination_count
 * @property int|null $movie_awards_winner_count
 * @property int|null $movie_awards_nomination_count
 * @property int|null $comments_count
 *
 * @property Comment[] $comments
 * @property MovieActor[] $movieActors
 * @property MovieDirector[] $movieDirectors
 * @property PersonAward[] $personAwards
 */
class Person extends \app\models\base\BaseModel implements Sortingable
{
    const SEX_FEMALE = 0;
    const SEX_MALE = 1;
    const SEX_OTHER = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'person';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['first_name', 'created_at'], 'required'],
            [['birthday', 'created_at', 'updated_at'], 'safe'],
            [['sex', 'total_movies_count', 'viewed_movies_count', 'awards_winner_count', 'awards_nomination_count', 'movie_awards_winner_count', 'movie_awards_nomination_count', 'comments_count'], 'integer'],
            [['first_name', 'last_name', 'image'], 'string', 'max' => 255],
            [['first_name', 'last_name', 'image'], 'trim'],
            [['first_name', 'last_name'], 'unique', 'targetAttribute' => ['first_name', 'last_name']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'birthday' => 'Дата рождения',
            'sex' => 'Пол',
            'image' => 'Фото',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'total_movies_count' => 'Total Movies Count',
            'viewed_movies_count' => 'Viewed Movies Count',
            'awards_winner_count' => 'Awards Winner Count',
            'awards_nomination_count' => 'Awards Nomination Count',
            'movie_awards_winner_count' => 'Movie Awards Winner Count',
            'movie_awards_nomination_count' => 'Movie Awards Nomination Count',
            'comments_count' => 'Comments Count',
        ];
    }

    public static function sortList(): array
    {
        return [
            'first_name_asc' => 'По имени ↑',
            'first_name_desc' => 'По имени ↓',
            'last_name_asc' => 'По фамилии ↑',
            'last_name_desc' => 'По фамилии ↓',
            'viewed_movies_count_asc' => 'По количеству просмотренных фильмов ↑',
            'viewed_movies_count_desc' => 'По количеству просмотренных фильмов ↓',
            'total_movies_count_asc' => 'По общему количеству фильмов ↑',
            'total_movies_count_desc' => 'По общему количеству фильмов ↓',
            'awards_count_desc' => 'По количеству наград ↓',
            'nomination_count_desc' => 'По количеству номинаций ↓',
        ];
    }

    public static function typeList(): array
    {
        return [
            'actors' => 'Актеры',
            'directors' => 'Режиссеры',
        ];
    }

    public static function sexList(): array
    {
        return [
            'male' => 'Мужчины',
            'female' => 'Женщины',
        ];
    }

    public function getFullName(): string
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Gets query for [[Comments]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::class, ['person_id' => 'id']);
    }

    /**
     * Gets query for [[MovieActors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMovieActors()
    {
        return $this->hasMany(MovieActor::class, ['person_id' => 'id']);
    }

    /**
     * Gets query for [[MovieDirectors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMovieDirectors()
    {
        return $this->hasMany(MovieDirector::class, ['person_id' => 'id']);
    }

    /**
     * Gets query for [[PersonAwards]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonAwards()
    {
        return $this->hasMany(PersonAward::class, ['person_id' => 'id']);
    }

    public function getViewedMovieActorCount(): int
    {
        return Person::find()
            ->innerJoin('movie_actor', 'movie_actor.person_id = person.id')
            ->innerJoin('movie', 'movie.id = movie_actor.movie_id')
            ->innerJoin('view', 'view.movie_id = movie.id')
            ->where(['person.id' => $this->id])
            ->groupBy(['movie.id'])
            ->count();
    }

    public function getMovieActorCount(): int
    {
        return $this->getMovieActors()->count();
    }

    public function getViewedMovieDirectorCount(): int
    {
        return Person::find()
            ->innerJoin('movie_director', 'movie_director.person_id = person.id')
            ->innerJoin('movie', 'movie.id = movie_director.movie_id')
            ->innerJoin('view', 'view.movie_id = movie.id')
            ->where(['person.id' => $this->id])
            ->groupBy(['movie.id'])
            ->count();
    }

    public function getMovieDirectorCount(): int
    {
        return $this->getMovieDirectors()->count();
    }

    public function getUrlForGoogleSearch(): string
    {
        return 'https://www.google.com.ua/search?q=' . urlencode($this->getFullName() . ' вики');
    }

    public function prepareSortCount(): void
    {
        $this->total_movies_count = 0;
        $this->viewed_movies_count = 0;

        $this->total_movies_count += MovieActor::find()
            ->where(['person_id' => $this->id])
            ->count();

        $this->total_movies_count += MovieDirector::find()
            ->where(['person_id' => $this->id])
            ->count();

        $this->viewed_movies_count += MovieActor::find()
            ->innerJoin('movie', 'movie_actor.movie_id = movie.id')
            ->innerJoin('view', 'movie.id = view.movie_id')
            ->where(['person_id' => $this->id])
            ->groupBy(['movie_actor.id'])
            ->count();

        $this->viewed_movies_count += MovieDirector::find()
            ->innerJoin('movie', 'movie_director.movie_id = movie.id')
            ->innerJoin('view', 'movie.id = view.movie_id')
            ->where(['person_id' => $this->id])
            ->groupBy(['movie_director.id'])
            ->count();

        $this->awards_winner_count = PersonAward::find()
            ->where(['person_id' => $this->id])
            ->andWhere(['type' => Award::TYPE_WINNER])
            ->count();

        $this->awards_nomination_count = PersonAward::find()
            ->where(['person_id' => $this->id])
            ->andWhere(['type' => Award::TYPE_NOMINATION])
            ->count();

        $this->comments_count = Comment::find()
            ->where(['person_id' => $this->id])
            ->count();

        $this->movie_awards_winner_count = 0;
        $this->movie_awards_nomination_count = 0;

        foreach (MovieActor::find()->where(['person_id' => $this->id])->each() as $movie_actor_item) {
            $this->movie_awards_winner_count += MovieAward::find()
                ->where(['movie_id' => $movie_actor_item->movie_id])
                ->andWhere(['type' => Award::TYPE_WINNER])
                ->count();

            $this->movie_awards_nomination_count += MovieAward::find()
                ->where(['movie_id' => $movie_actor_item->movie_id])
                ->andWhere(['type' => Award::TYPE_NOMINATION])
                ->count();
        }
        unset($movie_actor_item);

        foreach (MovieDirector::find()->where(['person_id' => $this->id])->each() as $movie_director_item) {
            $this->movie_awards_winner_count += MovieAward::find()
                ->where(['movie_id' => $movie_director_item->movie_id])
                ->andWhere(['type' => Award::TYPE_WINNER])
                ->count();

            $this->movie_awards_nomination_count += MovieAward::find()
                ->where(['movie_id' => $movie_director_item->movie_id])
                ->andWhere(['type' => Award::TYPE_NOMINATION])
                ->count();
        }
        unset($movie_director_item);

        if (
            !$this->isAttributeChanged('total_movies_count')
            && !$this->isAttributeChanged('viewed_movies_count')
            && !$this->isAttributeChanged('awards_winner_count')
            && !$this->isAttributeChanged('awards_nomination_count')
            && !$this->isAttributeChanged('comments_count')
            && !$this->isAttributeChanged('movie_awards_winner_count')
            && !$this->isAttributeChanged('movie_awards_nomination_count')
        ) {
            return;
        }

        if (!$this->save()) {
            throw new Exception('Failed to save Person: ' . $this->getErrorsAsString());
        }
    }

    public static function prepareSortCounts(): void
    {
        /** @var Person $person_item */
        foreach (Person::find()->each() as $person_item) {
            $person_item->prepareSortCount();
        }
    }

    public static function getAllPersonsForFilters(): array
    {
        return self::find()
            ->select(['concat(first_name, " ", last_name) as name', 'id'])
            ->orderBy(['name' => SORT_ASC])
            ->indexBy('id')
            ->column();
    }

    /**
     * {@inheritdoc}
     * @return PersonQuery the active query used by this AR class.
     */
    public static function find(): PersonQuery
    {
        return new PersonQuery(get_called_class());
    }
}
