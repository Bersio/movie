<?php

namespace app\models\base;

use app\models\Series;
use DateTime;
use Exception;
use Yii;
use app\models\Award;
use app\models\Comment;
use app\models\Movie;
use app\models\Person;
use app\models\image\Thumb;

/**
 * This is the base model class for all models that contains 'created_at' and 'updated_at' fields.
 */
class BaseModel extends \yii\db\ActiveRecord
{
    public function init(): void
    {
        parent::init();

        if (empty($this->created_at)) {
            $this->created_at = (new DateTime())->format('Y-m-d H:i:s');
        }

        if ($this->hasAttribute('updated_at') && empty($this->updated_at)) {
            $this->updated_at = (new DateTime())->format('Y-m-d H:i:s');
        }
    }

    public function beforeSave($insert): bool
    {
        if ($this->hasAttribute('updated_at')) {
            $this->updated_at = (new DateTime())->format('Y-m-d H:i:s');
        }

        return parent::beforeSave($insert);
    }

    public function deleteImageFolder(): bool
    {
        $type = $this->getImageFolderName();
        $images_folder = Yii::getAlias('@webroot') . '/images/' . $type . '/' . $this->id;

        return $this->deleteFolder($images_folder);
    }

    public function deleteThumbs(): bool
    {
        $thumbs_folder = Thumb::getAbsoluteFolder($this);

        return $this->deleteFolder($thumbs_folder);
    }

    public function getImageFolderName(): string
    {
        if ($this instanceof Award) {
            return 'award';
        } elseif ($this instanceof Comment) {
            return 'comment';
        } elseif ($this instanceof Movie) {
            return 'movie';
        } elseif ($this instanceof Person) {
            return 'person';
        } elseif ($this instanceof Series) {
            return 'series';
        }

        throw new Exception('Model is not correct');
    }

    public function getErrorsAsString(): string
    {
        return json_encode($this->getErrors(), JSON_UNESCAPED_UNICODE);
    }

    private function deleteFolder(string $folder_path): bool
    {
        if (!is_dir($folder_path)) {
            return false;
        }

        $objs = glob($folder_path . '/*');

        if (!$objs) {
            return false;
        }

        foreach ($objs as $obj) {
            if (is_dir($obj)) {
                continue;
            }

            unlink($obj);
        }

        return rmdir($folder_path);
    }
}
