<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;
use app\models\query\AwardNominationQuery;

/**
 * This is the model class for table "award_nomination".
 *
 * @property int $id
 * @property int $award_id
 * @property string $name
 * @property int|null $type
 * @property string $created_at
 * @property string|null $updated_at
 *
 * @property Award $award
 * @property MovieAward[] $movieAwards
 * @property PersonAward[] $personAwards
 */
class AwardNomination extends \app\models\base\BaseModel
{
    public const int NOMINATION_TYPE_MOVIE = 1;
    public const int NOMINATION_TYPE_PERSON = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'award_nomination';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['award_id', 'name'], 'required'],
            [['award_id', 'type'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'trim'],
            [['award_id', 'name'], 'unique', 'targetAttribute' => ['award_id', 'name']],
            [['award_id'], 'exist', 'skipOnError' => true, 'targetClass' => Award::class, 'targetAttribute' => ['award_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'award_id' => Yii::t('app', 'Award'),
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
            'created_at' => Yii::t('app', 'Created at'),
            'updated_at' => Yii::t('app', 'Updated at'),
        ];
    }

    public static function sortList(): array
    {
        return [
            'reverse' => Yii::t('app', 'Reverse'),
            'title_asc' => Yii::t('app', 'By title ↑'),
            'title_desc' => Yii::t('app', 'By title ↓'),
        ];
    }

    public static function typesList(): array
    {
        return [
            self::NOMINATION_TYPE_MOVIE => Yii::t('app', 'Film Nominations'),
            self::NOMINATION_TYPE_PERSON => Yii::t('app', 'Nomination of persons'),
        ];
    }

    /**
     * Gets query for [[Award]].
     *
     * @return ActiveQuery
     */
    public function getAward(): ActiveQuery
    {
        return $this->hasOne(Award::class, ['id' => 'award_id']);
    }

    /**
     * Gets query for [[MovieAwards]].
     *
     * @return ActiveQuery
     */
    public function getMovieAwards(): ActiveQuery
    {
        return $this->hasMany(MovieAward::class, ['award_nomination_id' => 'id']);
    }

    /**
     * Gets query for [[PersonAwards]].
     *
     * @return ActiveQuery
     */
    public function getPersonAwards(): ActiveQuery
    {
        return $this->hasMany(PersonAward::class, ['award_nomination_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return AwardNominationQuery the active query used by this AR class.
     */
    public static function find(): AwardNominationQuery
    {
        return new AwardNominationQuery(get_called_class());
    }
}
