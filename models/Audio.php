<?php

namespace app\models;

use Exception;
use Yii;
use app\models\query\AudioQuery;
use app\models\interfaces\Sortingable;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "audio".
 *
 * @property int $id
 * @property string $name
 * @property string $created_at
 * @property string|null $updated_at
 * @property int|null $viewed_movies_count
 *
 * @property View[] $views
 */
class Audio extends \app\models\base\BaseModel implements Sortingable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'audio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['viewed_movies_count'], 'integer'],
            [['viewed_movies_count'], 'default', 'value' => 0],
            [['name'], 'string', 'max' => 255],
            [['name'], 'trim'],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name' => Yii::t('app', 'Name'),
            'viewed_movies_count' => Yii::t('app', 'Viewed Movies Count'),
        ];
    }

    public static function sortList(): array
    {
        return [
            'title_asc' => Yii::t('app', 'By title ↑'),
            'title_desc' => Yii::t('app', 'By title ↓'),
            'viewed_movies_count_asc' => Yii::t('app', 'By number of films watched ↑'),
            'viewed_movies_count_desc' => Yii::t('app', 'By number of films watched ↓'),
        ];
    }

    /**
     * Gets query for [[Views]].
     *
     * @return ActiveQuery
     */
    public function getViews(): ActiveQuery
    {
        return $this->hasMany(View::class, ['audio_id' => 'id']);
    }

    public function prepareSortCount(): void
    {
        $this->viewed_movies_count = View::find()
            ->where(['audio_id' => $this->id])
            ->count();

        if (!$this->isAttributeChanged('viewed_movies_count')) {
            return;
        }

        if (!$this->save()) {
            throw new Exception('Failed to save Audio: ' . $this->getErrorsAsString());
        }
    }

    /**
     * Обновляет параметры для сортировки для всех озвучек
     * @return void
     */
    public static function prepareSortCounts(): void
    {
        /** @var Audio $audio_item */
        foreach (self::find()->each() as $audio_item) {
            $audio_item->prepareSortCount();
        }
    }

    public static function getAllAudioForFilters(): array
    {
        return self::find()
            ->select([
                'audio.name',
                'audio.id',
                'audio.viewed_movies_count',
            ])
            ->sortByDefault()
            ->indexBy('audio.id')
            ->column();
    }

    /**
     * {@inheritdoc}
     * @return AudioQuery the active query used by this AR class.
     */
    public static function find(): AudioQuery
    {
        return new AudioQuery(get_called_class());
    }
}
