<?php

namespace app\models\query;

use app\models\Country;

/**
 * This is the ActiveQuery class for [[Country]].
 *
 * @see Country
 */
class CountryQuery extends \yii\db\ActiveQuery
{
    public function prepareSorting($sort): self
    {
        if (!empty($sort)) {
            // выбрана сортировка
            if ($sort === 'title_asc') {
                $this->sortByRuNameASC();
            } elseif ($sort === 'title_desc') {
                $this->sortByRuNameDESC();
            } elseif ($sort === 'en_title_asc') {
                $this->sortByEnNameASC();
            } elseif ($sort === 'en_title_desc') {
                $this->sortByEnNameDESC();
            } elseif ($sort === 'code_asc') {
                $this->sortByCodeASC();
            } elseif ($sort === 'code_desc') {
                $this->sortByCodeDESC();
            } elseif ($sort === 'viewed_movies_count_asc') {
                $this->sortByViewedMoviesCountASC();
            } elseif ($sort === 'viewed_movies_count_desc') {
                $this->sortByViewedMoviesCountDESC();
            } elseif ($sort === 'total_movies_count_asc') {
                $this->sortByTotalMoviesCountASC();
            } elseif ($sort === 'total_movies_count_desc') {
                $this->sortByTotalMoviesCountDESC();
            } else {
                $this->sortByDefault();
            }
        } else {
            // обычная сортировка
            $this->sortByDefault();
        }

        return $this;
    }

    public function sortByDefault(): self
    {
        return $this->orderBy([
            'country.viewed_movies_count' => SORT_DESC,
            'country.total_movies_count' => SORT_DESC,
            'country.ru_name' => SORT_ASC,
        ]);
    }

    public function sortByRuNameASC(): self
    {
        return $this->orderBy([
            'country.ru_name' => SORT_ASC,
        ]);
    }

    public function sortByRuNameDESC(): self
    {
        return $this->orderBy([
            'country.ru_name' => SORT_DESC,
        ]);
    }

    public function sortByEnNameASC(): self
    {
        return $this->orderBy([
            'country.en_name' => SORT_ASC,
        ]);
    }

    public function sortByEnNameDESC(): self
    {
        return $this->orderBy([
            'country.en_name' => SORT_DESC,
        ]);
    }

    public function sortByCodeASC(): self
    {
        return $this->orderBy([
            'country.code' => SORT_ASC,
        ]);
    }

    public function sortByCodeDESC(): self
    {
        return $this->orderBy([
            'country.code' => SORT_DESC,
        ]);
    }

    public function sortByViewedMoviesCountASC(): self
    {
        return $this->orderBy([
            'country.viewed_movies_count' => SORT_ASC,
            'country.id' => SORT_ASC,
        ]);
    }

    public function sortByViewedMoviesCountDESC(): self
    {
        return $this->orderBy([
            'country.viewed_movies_count' => SORT_DESC,
            'country.id' => SORT_ASC,
        ]);
    }

    public function sortByTotalMoviesCountASC(): self
    {
        return $this->orderBy([
            'country.total_movies_count' => SORT_ASC,
            'country.id' => SORT_ASC,
        ]);
    }

    public function sortByTotalMoviesCountDESC(): self
    {
        return $this->orderBy([
            'country.total_movies_count' => SORT_DESC,
            'country.id' => SORT_ASC,
        ]);
    }
}
