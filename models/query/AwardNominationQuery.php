<?php

namespace app\models\query;

use app\models\AwardNomination;

/**
 * This is the ActiveQuery class for [[AwardNomination]].
 *
 * @see AwardNomination
 */
class AwardNominationQuery extends \yii\db\ActiveQuery
{
    public function prepareSorting($sort): self
    {
        if (!empty($sort)) {
            // выбрана сортировка
            if ($sort === 'reverse') {
                $this->sortByReverse();
            } elseif ($sort === 'title_asc') {
                $this->sortByTitleASC();
            } elseif ($sort === 'title_desc') {
                $this->sortByTitleDESC();
            } else {
                $this->sortByDefault();
            }
        } else {
            // обычная сортировка
            $this->sortByDefault();
        }

        return $this;
    }

    public function sortByDefault(): self
    {
        return $this->orderBy([
            'award_nomination.id' => SORT_ASC,
        ]);
    }

    public function sortByReverse(): self
    {
        return $this->orderBy([
            'award_nomination.id' => SORT_DESC,
        ]);
    }

    public function sortByTitleASC(): self
    {
        return $this->orderBy([
            'award_nomination.name' => SORT_ASC,
        ]);
    }

    public function sortByTitleDESC(): self
    {
        return $this->orderBy([
            'award_nomination.name' => SORT_DESC,
        ]);
    }
}
