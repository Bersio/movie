<?php

namespace app\models\query;

use app\models\Person;

/**
 * This is the ActiveQuery class for [[Person]].
 *
 * @see Person
 */
class PersonQuery extends \yii\db\ActiveQuery
{
    public function prepareSorting($sort): self
    {
        if (!empty($sort)) {
            // выбрана сортировка
            if ($sort === 'first_name_asc') {
                $this->sortByFirstNameASC();
            } elseif ($sort === 'first_name_desc') {
                $this->sortByFirstNameDESC();
            } elseif ($sort === 'last_name_asc') {
                $this->sortByLastNameASC();
            } elseif ($sort === 'last_name_desc') {
                $this->sortByLastNameDESC();
            } elseif ($sort === 'viewed_movies_count_asc') {
                $this->sortByViewedMoviesCountASC();
            } elseif ($sort === 'viewed_movies_count_desc') {
                $this->sortByViewedMoviesCountDESC();
            } elseif ($sort === 'total_movies_count_asc') {
                $this->sortByTotalMoviesCountASC();
            } elseif ($sort === 'total_movies_count_desc') {
                $this->sortByTotalMoviesCountDESC();
            } elseif ($sort === 'awards_count_desc') {
                $this->sortByAwardsCountDESC();
            } elseif ($sort === 'nomination_count_desc') {
                $this->sortByNominationCountDESC();
            } else {
                $this->sortByDefault();
            }
        } else {
            // обычная сортировка
            $this->sortByDefault();
        }

        return $this;
    }

    public function sortByDefault(): self
    {
        return $this->orderBy([
            'person.viewed_movies_count' => SORT_DESC,
            'person.awards_winner_count' => SORT_DESC,
            'person.awards_nomination_count' => SORT_DESC,
            'person.movie_awards_winner_count' => SORT_DESC,
            'person.movie_awards_nomination_count' => SORT_DESC,
            'person.total_movies_count' => SORT_DESC,
            'person.comments_count' => SORT_DESC,
            'person.created_at' => SORT_DESC
        ]);
    }

    public function sortByFirstNameASC(): self
    {
        return $this->orderBy([
            'person.first_name' => SORT_ASC,
            'person.created_at' => SORT_DESC
        ]);
    }

    public function sortByFirstNameDESC(): self
    {
        return $this->orderBy([
            'person.first_name' => SORT_DESC,
            'person.created_at' => SORT_DESC
        ]);
    }

    public function sortByLastNameASC(): self
    {
        return $this->orderBy([
            'person.last_name' => SORT_ASC,
            'person.created_at' => SORT_DESC
        ]);
    }

    public function sortByLastNameDESC(): self
    {
        return $this->orderBy([
            'person.last_name' => SORT_DESC,
            'person.created_at' => SORT_DESC
        ]);
    }

    public function sortByViewedMoviesCountASC(): self
    {
        return $this->orderBy([
            'person.viewed_movies_count' => SORT_ASC,
            'person.created_at' => SORT_DESC
        ]);
    }

    public function sortByViewedMoviesCountDESC(): self
    {
        return $this->orderBy([
            'person.viewed_movies_count' => SORT_DESC,
            'person.created_at' => SORT_DESC
        ]);
    }

    public function sortByTotalMoviesCountASC(): self
    {
        return $this->orderBy([
            'person.total_movies_count' => SORT_ASC,
            'person.viewed_movies_count' => SORT_ASC,
            'person.created_at' => SORT_DESC
        ]);
    }

    public function sortByTotalMoviesCountDESC(): self
    {
        return $this->orderBy([
            'person.total_movies_count' => SORT_DESC,
            'person.viewed_movies_count' => SORT_DESC,
            'person.created_at' => SORT_DESC
        ]);
    }

    public function sortByAwardsCountDESC(): self
    {
        return $this->orderBy([
            'person.awards_winner_count' => SORT_DESC,
            'person.viewed_movies_count' => SORT_DESC,
            'person.created_at' => SORT_DESC
        ]);
    }

    public function sortByNominationCountDESC(): self
    {
        return $this->orderBy([
            'person.awards_nomination_count' => SORT_DESC,
            'person.awards_winner_count' => SORT_DESC,
            'person.viewed_movies_count' => SORT_DESC,
            'person.created_at' => SORT_DESC
        ]);
    }
}
