<?php

namespace app\models\query;

use app\models\Theater;

/**
 * This is the ActiveQuery class for [[Theater]].
 *
 * @see Theater
 */
class TheaterQuery extends \yii\db\ActiveQuery
{
    public function prepareSorting($sort): self
    {
        if (!empty($sort)) {
            // выбрана сортировка
            if ($sort === 'title_asc') {
                $this->sortByTitleASC();
            } elseif ($sort === 'title_desc') {
                $this->sortByTitleDESC();
            } elseif ($sort === 'viewed_movies_count_asc') {
                $this->sortByViewedMoviesCountASC();
            } elseif ($sort === 'viewed_movies_count_desc') {
                $this->sortByViewedMoviesCountDESC();
            } else {
                $this->sortByDefault();
            }
        } else {
            // обычная сортировка
            $this->sortByDefault();
        }

        return $this;
    }

    public function sortByDefault(): self
    {
        return $this->orderBy([
            'theater.viewed_movies_count' => SORT_DESC,
            'theater.id' => SORT_ASC,
        ]);
    }

    public function sortByTitleASC(): self
    {
        return $this->orderBy([
            'theater.name' => SORT_ASC,
        ]);
    }

    public function sortByTitleDESC(): self
    {
        return $this->orderBy([
            'theater.name' => SORT_DESC,
        ]);
    }

    public function sortByViewedMoviesCountASC(): self
    {
        return $this->orderBy([
            'theater.viewed_movies_count' => SORT_ASC,
            'theater.id' => SORT_ASC,
        ]);
    }

    public function sortByViewedMoviesCountDESC(): self
    {
        return $this->orderBy([
            'theater.viewed_movies_count' => SORT_DESC,
            'theater.id' => SORT_ASC,
        ]);
    }
}
