<?php

namespace app\models\query;

use app\models\Movie;

/**
 * This is the ActiveQuery class for [[Movie]].
 *
 * @see Movie
 */
class MovieQuery extends \yii\db\ActiveQuery
{
    public function prepareSorting($sort, $type): self
    {
        if (!empty($sort)) {
            // выбрана сортировка
            if ($sort === 'view_date_asc') {
                $this->sortViewsByDateASC();
            } elseif ($sort === 'view_date_desc') {
                $this->sortViewsByDateDESC();
            } elseif ($sort === 'view_rating_asc') {
                $this->sortViewsByRatingASC();
            } elseif ($sort === 'view_rating_desc') {
                $this->sortViewsByRatingDESC();
            } elseif ($sort === 'queue_date_asc') {
                $this->sortQueueByDateASC();
            } elseif ($sort === 'queue_date_desc') {
                $this->sortQueueByDateDESC();
            } elseif ($sort === 'rating_asc') {
                $this->sortByRatingASC();
            } elseif ($sort === 'rating_desc') {
                $this->sortByRatingDESC();
            } elseif ($sort === 'year_asc') {
                $this->sortMoviesByYearASC();
            } elseif ($sort === 'year_desc') {
                $this->sortMoviesByYearDESC();
            } elseif ($sort === 'title_asc') {
                $this->sortMoviesByTitleASC();
            } elseif ($sort === 'title_desc') {
                $this->sortMoviesByTitleDESC();
            } elseif ($sort === 'original_title_asc') {
                $this->sortMoviesByOriginalTitleASC();
            } elseif ($sort === 'original_title_desc') {
                $this->sortMoviesByOriginalTitleDESC();
            } else {
                $this->defaultSorting($type);
            }
        } else {
            // обычная сортировка
            $this->defaultSorting($type);
        }

        return $this;
    }

    private function defaultSorting($type): void
    {
        if ($type === Movie::SORT_TYPE_MOVIE) {
            $this->sortMoviesByDefault();
        } elseif ($type === Movie::SORT_TYPE_VIEW) {
            $this->sortViewsByDefault();
        } elseif ($type === Movie::SORT_TYPE_QUEUE) {
            $this->sortQueueByDefault();
        } else {
            $this->sortMoviesByDefault();
        }
    }

    public function sortMoviesByDefault(): self
    {
        return $this->orderBy([
            'movie.views_count' => SORT_DESC,
            'movie.awards_winner_count' => SORT_DESC,
            'movie.awards_nomination_count' => SORT_DESC,
            'movie.person_awards_winner_count' => SORT_DESC,
            'movie.person_awards_nomination_count' => SORT_DESC,
            'movie.actors_count' => SORT_DESC,
            'movie.directors_count' => SORT_DESC,
            'movie.comments_count' => SORT_DESC,
            'movie.created_at' => SORT_DESC
        ]);
    }

    public function sortViewsByDefault(): self
    {
        return $this->orderBy([
            'view.created_at' => SORT_DESC,
            'view.id' => SORT_DESC
        ]);
    }

    public function sortViewsByDateASC(): self
    {
        return $this->orderBy([
            'view.created_at' => SORT_ASC,
            'view.id' => SORT_ASC
        ]);
    }

    public function sortViewsByDateDESC(): self
    {
        return $this->orderBy([
            'view.created_at' => SORT_DESC,
            'view.id' => SORT_DESC
        ]);
    }

    public function sortViewsByRatingASC(): self
    {
        return $this->orderBy([
            'view.rating' => SORT_ASC,
            'movie.awards_winner_count' => SORT_DESC,
            'movie.awards_nomination_count' => SORT_DESC,
            'movie.person_awards_winner_count' => SORT_DESC,
            'movie.person_awards_nomination_count' => SORT_DESC,
            'movie.actors_count' => SORT_DESC,
            'movie.directors_count' => SORT_DESC,
            'movie.comments_count' => SORT_DESC,
            'movie.created_at' => SORT_DESC
        ]);
    }

    public function sortViewsByRatingDESC(): self
    {
        return $this->orderBy([
            'view.rating' => SORT_DESC,
            'movie.awards_winner_count' => SORT_DESC,
            'movie.awards_nomination_count' => SORT_DESC,
            'movie.person_awards_winner_count' => SORT_DESC,
            'movie.person_awards_nomination_count' => SORT_DESC,
            'movie.actors_count' => SORT_DESC,
            'movie.directors_count' => SORT_DESC,
            'movie.comments_count' => SORT_DESC,
            'movie.created_at' => SORT_DESC
        ]);
    }

    public function sortQueueByDateASC(): self
    {
        return $this->orderBy([
            'queue.created_at' => SORT_ASC,
            'queue.id' => SORT_ASC
        ]);
    }

    public function sortQueueByDateDESC(): self
    {
        return $this->orderBy([
            'queue.created_at' => SORT_DESC,
            'queue.id' => SORT_DESC
        ]);
    }

    public function sortByRatingASC(): self
    {
        return $this->orderBy([
            'movie.average_rating' => SORT_ASC,
            'movie.awards_winner_count' => SORT_DESC,
            'movie.awards_nomination_count' => SORT_DESC,
            'movie.person_awards_winner_count' => SORT_DESC,
            'movie.person_awards_nomination_count' => SORT_DESC,
            'movie.actors_count' => SORT_DESC,
            'movie.directors_count' => SORT_DESC,
            'movie.comments_count' => SORT_DESC,
            'movie.created_at' => SORT_DESC
        ]);
    }

    public function sortByRatingDESC(): self
    {
        return $this->orderBy([
            'movie.average_rating' => SORT_DESC,
            'movie.awards_winner_count' => SORT_DESC,
            'movie.awards_nomination_count' => SORT_DESC,
            'movie.person_awards_winner_count' => SORT_DESC,
            'movie.person_awards_nomination_count' => SORT_DESC,
            'movie.actors_count' => SORT_DESC,
            'movie.directors_count' => SORT_DESC,
            'movie.comments_count' => SORT_DESC,
            'movie.created_at' => SORT_DESC
        ]);
    }

    public function sortQueueByDefault(): self
    {
        return $this->orderBy([
            'movie.awards_winner_count' => SORT_DESC,
            'movie.awards_nomination_count' => SORT_DESC,
            'movie.person_awards_winner_count' => SORT_DESC,
            'movie.person_awards_nomination_count' => SORT_DESC,
            'movie.actors_count' => SORT_DESC,
            'movie.directors_count' => SORT_DESC,
            'movie.comments_count' => SORT_DESC,
            'movie.created_at' => SORT_DESC
        ]);
    }

    public function sortMoviesByYearASC(): self
    {
        return $this->orderBy([
            'movie.year' => SORT_ASC,
            'movie.awards_winner_count' => SORT_DESC,
            'movie.awards_nomination_count' => SORT_DESC,
            'movie.person_awards_winner_count' => SORT_DESC,
            'movie.person_awards_nomination_count' => SORT_DESC,
            'movie.actors_count' => SORT_DESC,
            'movie.directors_count' => SORT_DESC,
            'movie.comments_count' => SORT_DESC,
            'movie.created_at' => SORT_DESC
        ]);
    }

    public function sortMoviesByYearDESC(): self
    {
        return $this->orderBy([
            'movie.year' => SORT_DESC,
            'movie.awards_winner_count' => SORT_DESC,
            'movie.awards_nomination_count' => SORT_DESC,
            'movie.person_awards_winner_count' => SORT_DESC,
            'movie.person_awards_nomination_count' => SORT_DESC,
            'movie.actors_count' => SORT_DESC,
            'movie.directors_count' => SORT_DESC,
            'movie.comments_count' => SORT_DESC,
            'movie.created_at' => SORT_DESC
        ]);
    }

    public function sortMoviesByTitleASC(): self
    {
        return $this->orderBy([
            'movie.title' => SORT_ASC,
            'movie.awards_winner_count' => SORT_DESC,
            'movie.awards_nomination_count' => SORT_DESC,
            'movie.person_awards_winner_count' => SORT_DESC,
            'movie.person_awards_nomination_count' => SORT_DESC,
            'movie.actors_count' => SORT_DESC,
            'movie.directors_count' => SORT_DESC,
            'movie.comments_count' => SORT_DESC,
            'movie.created_at' => SORT_DESC
        ]);
    }

    public function sortMoviesByTitleDESC(): self
    {
        return $this->orderBy([
            'movie.title' => SORT_DESC,
            'movie.awards_winner_count' => SORT_DESC,
            'movie.awards_nomination_count' => SORT_DESC,
            'movie.person_awards_winner_count' => SORT_DESC,
            'movie.person_awards_nomination_count' => SORT_DESC,
            'movie.actors_count' => SORT_DESC,
            'movie.directors_count' => SORT_DESC,
            'movie.comments_count' => SORT_DESC,
            'movie.created_at' => SORT_DESC
        ]);
    }

    public function sortMoviesByOriginalTitleASC(): self
    {
        return $this->orderBy([
            'movie.original_title' => SORT_ASC,
            'movie.awards_winner_count' => SORT_DESC,
            'movie.awards_nomination_count' => SORT_DESC,
            'movie.person_awards_winner_count' => SORT_DESC,
            'movie.person_awards_nomination_count' => SORT_DESC,
            'movie.actors_count' => SORT_DESC,
            'movie.directors_count' => SORT_DESC,
            'movie.comments_count' => SORT_DESC,
            'movie.created_at' => SORT_DESC
        ]);
    }

    public function sortMoviesByOriginalTitleDESC(): self
    {
        return $this->orderBy([
            'movie.original_title' => SORT_DESC,
            'movie.awards_winner_count' => SORT_DESC,
            'movie.awards_nomination_count' => SORT_DESC,
            'movie.person_awards_winner_count' => SORT_DESC,
            'movie.person_awards_nomination_count' => SORT_DESC,
            'movie.actors_count' => SORT_DESC,
            'movie.directors_count' => SORT_DESC,
            'movie.comments_count' => SORT_DESC,
            'movie.created_at' => SORT_DESC
        ]);
    }
}
