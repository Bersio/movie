<?php

namespace app\models\query;

use app\models\Genre;

/**
 * This is the ActiveQuery class for [[Genre]].
 *
 * @see Genre
 */
class GenreQuery extends \yii\db\ActiveQuery
{
    public function prepareSorting($sort): self
    {
        if (!empty($sort)) {
            // выбрана сортировка
            if ($sort === 'title_asc') {
                $this->sortByTitleASC();
            } elseif ($sort === 'title_desc') {
                $this->sortByTitleDESC();
            } elseif ($sort === 'total_movies_count_asc') {
                $this->sortByTotalMoviesCountASC();
            } elseif ($sort === 'total_movies_count_desc') {
                $this->sortByTotalMoviesCountDESC();
            } elseif ($sort === 'viewed_movies_count_asc') {
                $this->sortByViewedMoviesCountASC();
            } elseif ($sort === 'viewed_movies_count_desc') {
                $this->sortByViewedMoviesCountDESC();
            } else {
                $this->sortByDefault();
            }
        } else {
            // обычная сортировка
            $this->sortByDefault();
        }

        return $this;
    }

    public function sortByDefault(): self
    {
        return $this->orderBy([
            'genre.viewed_movies_count' => SORT_DESC,
            'genre.movie_awards_winner_count' => SORT_DESC,
            'genre.movie_awards_nomination_count' => SORT_DESC,
            'genre.total_movies_count' => SORT_DESC,
            'genre.created_at' => SORT_ASC,
        ]);
    }

    public function sortByTitleASC(): self
    {
        return $this->orderBy([
            'genre.title' => SORT_ASC,
        ]);
    }

    public function sortByTitleDESC(): self
    {
        return $this->orderBy([
            'genre.title' => SORT_DESC,
        ]);
    }

    public function sortByTotalMoviesCountASC(): self
    {
        return $this->orderBy([
            'genre.total_movies_count' => SORT_ASC,
            'genre.viewed_movies_count' => SORT_DESC,
            'genre.created_at' => SORT_ASC,
        ]);
    }

    public function sortByTotalMoviesCountDESC(): self
    {
        return $this->orderBy([
            'genre.total_movies_count' => SORT_DESC,
            'genre.viewed_movies_count' => SORT_DESC,
            'genre.created_at' => SORT_ASC,
        ]);
    }

    public function sortByViewedMoviesCountASC(): self
    {
        return $this->orderBy([
            'genre.viewed_movies_count' => SORT_ASC,
            'genre.total_movies_count' => SORT_ASC,
            'genre.created_at' => SORT_ASC,
        ]);
    }

    public function sortByViewedMoviesCountDESC(): self
    {
        return $this->orderBy([
            'genre.viewed_movies_count' => SORT_DESC,
            'genre.total_movies_count' => SORT_DESC,
            'genre.created_at' => SORT_ASC,
        ]);
    }
}
