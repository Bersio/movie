<?php

namespace app\models;

use DateInterval;
use DateTime;
use Yii;

/**
 * Модель для расчета статистики за год
 */
class Calendar extends \yii\base\Model
{
    public int $year;

    public int $total_views_count;
    public int $theater_views_count = 0;
    public array $views_by_months = [];
    public array $views_by_genres = [];
    public array $views_by_countries = [];
    public array $ratings = [];
    public array $views_by_actors = [];
    public array $views_by_directors = [];
    public array $views_by_audio = [];
    public array $views_by_theaters = [];
    public array $views_by_drinks = [];

    private DateTime $date_now;
    private DateTime $start_date;
    private DateTime $end_date;

    /**
     * {@inheritdoc}
     */
    public function init(): void
    {
        parent::init();

        $this->date_now = new DateTime($this->year . '-01-01');
        $this->start_date = new DateTime($this->date_now->format('Y-01-01'));
        $this->end_date = new DateTime($this->date_now->format('Y-01-01'));

        $this->end_date->add(new DateInterval('P1Y'));

        $this->total_views_count = $this->getTotalViewsCountForYear();
        
        if ($this->total_views_count === 0) {
            return;
        }
        
        $this->theater_views_count = $this->getViewsCountInTheatersForYear();
        $this->views_by_months = $this->getViewsCountForYearByMonth();
        $this->views_by_genres = $this->getViewsCountForYearByGenres();
        $this->views_by_countries = $this->getViewsCountForYearByCountries();
        $this->ratings = $this->getRatingsForYear();
        $this->views_by_actors = $this->getViewsCountForYearByActors();
        $this->views_by_directors = $this->getViewsCountForYearByDirectors();
        $this->views_by_audio = $this->getViewsCountForYearByAudio();
        $this->views_by_theaters = $this->getViewsCountForYearByTheaters();
        $this->views_by_drinks = $this->getViewsCountForYearByDrinks();
    }

    private function getTotalViewsCountForYear(): int
    {
        return View::getViewsCountByPeriod(
            $this->start_date->format('Y-m-d'),
            $this->end_date->format('Y-m-d')
        );
    }

    private function getViewsCountInTheatersForYear(): int
    {
        return View::getViewsCountByPeriod(
            $this->start_date->format('Y-m-d'),
            $this->end_date->format('Y-m-d'),
            true
        );
    }

    private function getViewsCountForYearByMonth(): array
    {
        $views_by_months = [];
        $start_date_month = new DateTime($this->date_now->format('Y-01-01'));
        $end_date_month = new DateTime($this->date_now->format('Y-01-01'));
        $end_date_month->add(new DateInterval('P1M'));
        for ($i = 0; $i < 12; $i++) {
            $views_count_by_month = View::getViewsCountByPeriod(
                $start_date_month->format('Y-m-d'),
                $end_date_month->format('Y-m-d')
            );
            $views_by_months[] = [
                'month' => $start_date_month->format('F'),
                'month_number' => $i +1,
                'views_count' => $views_count_by_month,
            ];
            $start_date_month->add(new DateInterval('P1M'));
            $end_date_month->add(new DateInterval('P1M'));
        }
        unset($start_date_month, $end_date_month, $views_count_by_month);

        return $views_by_months;
    }

    private function getViewsCountForYearByGenres(): array
    {
        $views_by_genres = [];
        $view_query = View::find()
            ->with(['movie.movieGenres.genre'])
            ->where(['>=', 'created_at', $this->start_date->format('Y-m-d')])
            ->andWhere(['<', 'created_at', $this->end_date->format('Y-m-d')]);
        foreach ($view_query->each() as $view_item) {
            foreach ($view_item->movie->movieGenres as $movie_genre_item) {
                if (!empty($views_by_genres[$movie_genre_item->genre->id])) {
                    $views_by_genres[$movie_genre_item->genre->id]['views_count']++;
                } else {
                    $views_by_genres[$movie_genre_item->genre->id] = [
                        'genre' => $movie_genre_item->genre,
                        'views_count' => 1
                    ];
                }
            }
            unset($movie_genre_item);
        }
        unset($view_item);

        return $this->sort($views_by_genres);
    }

    private function getRatingsForYear(): array
    {
        $ratings = [
            [
                'rating' => 1,
                'views_count' => 0
            ],
            [
                'rating' => 2,
                'views_count' => 0
            ],
            [
                'rating' => 3,
                'views_count' => 0
            ],
            [
                'rating' => 4,
                'views_count' => 0
            ],
            [
                'rating' => 5,
                'views_count' => 0
            ],
            [
                'rating' => 6,
                'views_count' => 0
            ],
            [
                'rating' => 7,
                'views_count' => 0
            ],
            [
                'rating' => 8,
                'views_count' => 0
            ],
            [
                'rating' => 9,
                'views_count' => 0
            ],
            [
                'rating' => 10,
                'views_count' => 0
            ],
        ];
        $view_query = View::find()
            ->where(['>=', 'created_at', $this->start_date->format('Y-m-d')])
            ->andWhere(['<', 'created_at', $this->end_date->format('Y-m-d')]);
        foreach ($view_query->each() as $view_item) {
            foreach ($ratings as &$rating_item) {
                if ($rating_item['rating'] === $view_item->rating) {
                    $rating_item['views_count']++;
                }
            }
            unset($rating_item);
        }
        unset($view_item);

        return $ratings;
    }

    private function getViewsCountForYearByActors(): array
    {
        $views_by_actors = [];
        $view_query = View::find()
            ->with(['movie.movieActors.person'])
            ->where(['>=', 'created_at', $this->start_date->format('Y-m-d')])
            ->andWhere(['<', 'created_at', $this->end_date->format('Y-m-d')]);
        foreach ($view_query->each() as $view_item) {
            foreach ($view_item->movie->movieActors as $movie_actor_item) {
                if (!empty($views_by_actors[$movie_actor_item->person->id])) {
                    $views_by_actors[$movie_actor_item->person->id]['views_count']++;
                } else {
                    $views_by_actors[$movie_actor_item->person->id] = [
                        'person' => $movie_actor_item->person,
                        'views_count' => 1
                    ];
                }
            }
            unset($movie_actor_item);
        }
        unset($view_item);

        return $this->sort($views_by_actors);
    }

    private function getViewsCountForYearByDirectors(): array
    {
        $views_by_directors = [];
        $view_query = View::find()
            ->with(['movie.movieDirectors.person'])
            ->where(['>=', 'created_at', $this->start_date->format('Y-m-d')])
            ->andWhere(['<', 'created_at', $this->end_date->format('Y-m-d')]);
        foreach ($view_query->each() as $view_item) {
            foreach ($view_item->movie->movieDirectors as $movie_director_item) {
                if (!empty($views_by_directors[$movie_director_item->person->id])) {
                    $views_by_directors[$movie_director_item->person->id]['views_count']++;
                } else {
                    $views_by_directors[$movie_director_item->person->id] = [
                        'person' => $movie_director_item->person,
                        'views_count' => 1
                    ];
                }
            }
            unset($movie_director_item);
        }
        unset($view_item);

        return $this->sort($views_by_directors);
    }

    private function getViewsCountForYearByAudio(): array
    {
        $views_by_audio = [];
        $view_query = View::find()
            ->with(['audio'])
            ->where(['>=', 'created_at', $this->start_date->format('Y-m-d')])
            ->andWhere(['<', 'created_at', $this->end_date->format('Y-m-d')])
            ->andWhere(['is not', 'audio_id', null]);
        foreach ($view_query->each() as $view_item) {
            if (!empty($views_by_audio[$view_item->audio->id])) {
                $views_by_audio[$view_item->audio->id]['views_count']++;
            } else {
                $views_by_audio[$view_item->audio->id] = [
                    'audio' => $view_item->audio,
                    'views_count' => 1
                ];
            }
        }
        unset($view_item);

        return $this->sort($views_by_audio);
    }

    private function getViewsCountForYearByTheaters(): array
    {
        $views_by_theaters = [];
        $view_query = View::find()
            ->with(['theater'])
            ->where(['>=', 'created_at', $this->start_date->format('Y-m-d')])
            ->andWhere(['<', 'created_at', $this->end_date->format('Y-m-d')])
            ->andWhere(['is not', 'theater_id', null]);
        foreach ($view_query->each() as $view_item) {
            if (!empty($views_by_theaters[$view_item->theater->id])) {
                $views_by_theaters[$view_item->theater->id]['views_count']++;
            } else {
                $views_by_theaters[$view_item->theater->id] = [
                    'theater' => $view_item->theater,
                    'views_count' => 1
                ];
            }
        }
        unset($view_item);

        return $this->sort($views_by_theaters);
    }

    private function getViewsCountForYearByDrinks(): array
    {
        $views_by_drinks = [];
        $view_query = View::find()
            ->with(['drink'])
            ->where(['>=', 'created_at', $this->start_date->format('Y-m-d')])
            ->andWhere(['<', 'created_at', $this->end_date->format('Y-m-d')])
            ->andWhere(['is not', 'drink_id', null]);
        foreach ($view_query->each() as $view_item) {
            if (!empty($views_by_drinks[$view_item->drink->id])) {
                $views_by_drinks[$view_item->drink->id]['views_count']++;
            } else {
                $views_by_drinks[$view_item->drink->id] = [
                    'drink' => $view_item->drink,
                    'views_count' => 1
                ];
            }
        }
        unset($view_item);

        return $this->sort($views_by_drinks);
    }

     private function getViewsCountForYearByCountries(): array
    {
        $views_by_countries = [];
        $view_query = View::find()
            ->with(['movie.movieCountries.country'])
            ->where(['>=', 'created_at', $this->start_date->format('Y-m-d')])
            ->andWhere(['<', 'created_at', $this->end_date->format('Y-m-d')]);
        foreach ($view_query->each() as $view_item) {
            foreach ($view_item->movie->movieCountries as $movie_country_item) {
                if (!empty($views_by_countries[$movie_country_item->country->id])) {
                    $views_by_countries[$movie_country_item->country->id]['views_count']++;
                } else {
                    $views_by_countries[$movie_country_item->country->id] = [
                        'country' => $movie_country_item->country,
                        'views_count' => 1
                    ];
                }
            }
            unset($movie_country_item);
        }
        unset($view_item);

        return $this->sort($views_by_countries);
    }

    /**
     * Сортировка результатов статистики по убыванию
     * @param array $views_array массив результатов статистики
     * @return array
     */
    private function sort(array $views_array): array
    {
        $views_array = array_values($views_array);

        for ($i = 0; $i < count($views_array); $i++) {
            for ($j = $i; $j < count($views_array); $j++) {
                if ($views_array[$j]['views_count'] > $views_array[$i]['views_count']) {
                    $buff = $views_array[$i];
                    $views_array[$i] = $views_array[$j];
                    $views_array[$j] = $buff;
                }
            }
            unset($j);
        }
        unset($i);

        return $views_array;
    }
}
