<?php

namespace app\models;

use Exception;
use Yii;
use app\models\query\DrinkQuery;
use app\models\interfaces\Sortingable;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "drink".
 *
 * @property int $id
 * @property string $name
 * @property int|null $alcohol
 * @property string $created_at
 * @property string|null $updated_at
 * @property int|null $viewed_movies_count
 *
 * @property View[] $views
 */
class Drink extends \app\models\base\BaseModel implements Sortingable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'drink';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['alcohol'], 'boolean'],
            [['viewed_movies_count'], 'integer'],
            [['viewed_movies_count'], 'default', 'value' => 0],
            [['name'], 'string', 'max' => 255],
            [['name'], 'trim'],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name' => Yii::t('app', 'Name'),
            'alcohol' => Yii::t('app', 'Alcohol'),
            'viewed_movies_count' => Yii::t('app', 'Viewed Movies Count'),
        ];
    }

    public static function sortList(): array
    {
        return [
            'title_asc' => Yii::t('app', 'By title ↑'),
            'title_desc' => Yii::t('app', 'By title ↓'),
            'viewed_movies_count_asc' => Yii::t('app', 'By number of films watched ↑'),
            'viewed_movies_count_desc' => Yii::t('app', 'By number of films watched ↓'),
        ];
    }

    public static function alcoholList(): array
    {
        return [
            'yes' => Yii::t('app', 'Yes'),
            'no' => Yii::t('app', 'No'),
        ];
    }

    /**
     * Gets query for [[Views]].
     *
     * @return ActiveQuery
     */
    public function getViews(): ActiveQuery
    {
        return $this->hasMany(View::class, ['drink_id' => 'id']);
    }

    public function prepareSortCount(): void
    {
        $this->viewed_movies_count = View::find()
            ->where(['drink_id' => $this->id])
            ->count();

        if (!$this->isAttributeChanged('viewed_movies_count')) {
            return;
        }

        if (!$this->save()) {
            throw new Exception('Failed to save Drink: ' . $this->getErrorsAsString());
        }
    }

    /**
     * Обновляет параметры для сортировки для всех напитков
     * @return void
     */
    public static function prepareSortCounts(): void
    {
        /** @var Drink $drink_item */
        foreach (self::find()->each() as $drink_item) {
            $drink_item->prepareSortCount();
        }
    }

    public static function getAllDrinksForFilters(): array
    {
        return self::find()
            ->select([
                'drink.name',
                'drink.id',
                'drink.viewed_movies_count',
            ])
            ->sortByDefault()
            ->indexBy('drink.id')
            ->column();
    }

    /**
     * {@inheritdoc}
     * @return DrinkQuery the active query used by this AR class.
     */
    public static function find(): DrinkQuery
    {
        return new DrinkQuery(get_called_class());
    }
}
