<?php

namespace app\models;

use Exception;
use Yii;
use app\models\query\SpecialShowingQuery;
use app\models\interfaces\Sortingable;

/**
 * This is the model class for table "special_showing".
 *
 * @property int $id
 * @property string $name
 * @property string $created_at
 * @property string|null $updated_at
 * @property int|null $viewed_movies_count
 *
 * @property View[] $views
 */
class SpecialShowing extends \app\models\base\BaseModel implements Sortingable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'special_showing';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['name', 'created_at'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['viewed_movies_count'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'viewed_movies_count' => 'Viewed Movies Count',
        ];
    }

    public static function sortList(): array
    {
        return [
            'title_asc' => 'По названию ↑',
            'title_desc' => 'По названию ↓',
            'viewed_movies_count_asc' => 'По количеству просмотренных фильмов ↑',
            'viewed_movies_count_desc' => 'По количеству просмотренных фильмов ↓',
        ];
    }

    /**
     * Gets query for [[Views]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getViews()
    {
        return $this->hasMany(View::class, ['special_showing_id' => 'id']);
    }

    public function prepareSortCount(): void
    {
        $this->viewed_movies_count = View::find()
                ->where(['special_showing_id' => $this->id])
                ->count();

        if (!$this->isAttributeChanged('viewed_movies_count')) {
            return;
        }

        if (!$this->save()) {
            throw new Exception('Failed to save SpecialShowingQuery: ' . $this->getErrorsAsString());
        }
    }

    /**
     * Обновляет параметры для сортировки для всех кинотеатров
     * @return void
     */
    public static function prepareSortCounts(): void
    {
        /** @var SpecialShowing $special_showing_item */
        foreach (self::find()->each() as $special_showing_item) {
            $special_showing_item->prepareSortCount();
        }
    }

    public static function getSpecialShowingForViewsFilters(): array
    {
        $result = self::find()
            ->select([
                'special_showing.name',
                'special_showing.id',
                'special_showing.viewed_movies_count',
                'special_showing.created_at',
            ])
            ->innerJoin('view', 'special_showing.id = view.special_showing_id')
            ->groupBy(['special_showing.id'])
            ->sortByDefault()
            ->indexBy('special_showing.id')
            ->column();
        
        $result[-1] = 'Любой';
        
        return $result;
    }

    public static function getAllSpecialShowingForFilters(): array
    {
        return self::find()
            ->select([
                'special_showing.name',
                'special_showing.id',
                'special_showing.viewed_movies_count',
            ])
            ->sortByDefault()
            ->indexBy('special_showing.id')
            ->column();
    }

    /**
     * {@inheritdoc}
     * @return SpecialShowingQuery the active query used by this AR class.
     */
    public static function find(): SpecialShowingQuery
    {
        return new SpecialShowingQuery(get_called_class());
    }
}
