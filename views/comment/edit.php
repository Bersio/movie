<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\image\Thumb;

/* @var $this yii\web\View */
/* @var $model app\models\Comment */
/* @var $form ActiveForm */

$this->title = $model->getTargetName();

$this->registerCssFile('/css/image-viewer.css', ['depends' => 'app\assets\AppAsset']);
$this->registerJsFile('/js/image-viewer.min.js', ['depends' => 'app\assets\AppAsset']);
?>
<div class="comment-edit">
    <div class="row">
        <h1><?= $this->title ?></h1>
    </div>

    <div class="row">
    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'content')->textarea(['rows' => 10, 'cols' => 128, 'autofocus' => true]) ?>

        <?php if ($model->images) { ?>
        <p>
            <?php foreach ($model->images as $image) { ?>
            <a class="nsbbox" title="Фото из комментария" href="<?= Thumb::getBig($model, $image) ?>" data-origin="<?= Thumb::getOriginUri($model, $image) ?>">
                <img class="comment-image" src="<?= Thumb::getMin($model, $image) ?>" alt="comment image" />
            </a>
            <?php }
            unset($image);
            ?>
        </p>
        <?php } ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>

            <a href="/comment/delete?id=<?= $model->id ?>" class="btn btn-sm btn-danger" onclick="if (confirm('Точно удалить?')) { return true; } else { return false; }">Удалить комментарий</a>

            <a href="<?= $model->getTargetUrl() ?>" class="btn btn-sm btn-default">Отмена</a>
        </div>
    <?php ActiveForm::end(); ?>
    </div>

</div><!-- comment-edit -->
