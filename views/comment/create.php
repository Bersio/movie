<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Comment */
/* @var $form ActiveForm */
/* @var $imagesUploadForm app\models\image\CommentImagesUploadForm */

$this->title = $model->getTargetName();
?>
<div class="comment-create">
    <div class="row">
        <h1><?= $this->title ?></h1>
    </div>

    <div class="row">
    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'content')
            ->textarea(['rows' => 10, 'cols' => 128, 'autofocus' => true]) ?>

        <?= $form->field($imagesUploadForm, 'imageFiles[]')
            ->fileInput(['multiple' => true, 'accept' => 'image/*'])
            ->label('Фото') ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
    </div>
</div>
