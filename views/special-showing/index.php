<?php

use app\models\query\SpecialShowingQuery;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/** @var yii\web\View $this */
/* @var $special_showing_query SpecialShowingQuery */
/* @var $special_showing_total_count int */
/* @var $sort string */
/* @var $sort_list array */

$this->title = Yii::t('app', 'Special showing');
?>
<div class="special-showing-index">

    <div class="jumbotron">
        <h1><?= $this->title ?></h1>
        <h2>Всего спец. показов: <?= $special_showing_total_count ?></h2>
    </div>

    <div class="body-content">

        <div class="row">
            <?php ActiveForm::begin([
                'id' => 'special-showing-filters',
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-2 control-label'],
                ],
            ]); ?>

            <?php if (!Yii::$app->user->isGuest) { ?>
            <div class="form-group">
                <div class="col-lg-3">
                    <a href="/special-showing/create" class="btn btn-success">Создать спец. показ</a>
                </div>
            </div>
            <?php } ?>

            <div class="form-group">
                <div class="col-lg-4">
                <?= Html::dropDownList('sort', $sort, $sort_list, [
                    'onchange' => 'this.form.submit()',
                    'prompt' => Yii::t('app', 'Default sorting'),
                    'class' => 'form-control'
                ]); ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

        <hr />

        <div class="row">
            <table>
                <tbody>
                    <?php foreach ($special_showing_query->each() as $special_showing_item) { ?>
                    <tr>
                        <td>
                            <h2>
                                <a href="/special-showing/details?id=<?= $special_showing_item->id ?>">
                                    <?= $special_showing_item->name ?>
                                </a>
                            </h2>
                        </td>
                        <td><?= $special_showing_item->viewed_movies_count ?></td>
                        <td>

                        </td>
                    </tr>
                    <?php }
                    unset($special_showing_item);
                    ?>
                </tbody>
            </table>
        </div>

    </div>
</div>
