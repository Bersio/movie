<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SpecialShowing */
/* @var $form ActiveForm */

$this->title = $model->name . ' | Редактирование спец. показа';
?>
<div class="special-showing-edit">

    <div class="row">
        <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'name') ?>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

    <hr />

    <div class="row">
        <a href="/special-showing/delete?id=<?= $model->id ?>" class="btn btn-sm btn-danger" onclick="return confirm('Точно удалить?');">Удалить спец. показ</a>
    </div>

</div><!-- special-showing-edit -->
