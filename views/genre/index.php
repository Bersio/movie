<?php

use app\models\query\GenreQuery;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $genre_query GenreQuery */
/* @var $genre_total_count int */
/* @var $sort string */
/* @var $sort_list array */

$this->title = Yii::t('app', 'Genres');
?>
<div class="genre-index">

    <div class="jumbotron">
        <h1><?= $this->title ?></h1>
        <h2>Всего жанров: <?= $genre_total_count ?></h2>
    </div>

    <div class="body-content">

        <div class="row">
            <?php ActiveForm::begin([
                'id' => 'genre-filters',
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-2 control-label'],
                ],
            ]); ?>

            <?php if (!Yii::$app->user->isGuest) { ?>
            <div class="form-group">
                <div class="col-lg-3">
                    <a href="/genre/create" class="btn btn-success">Создать новый жанр</a>
                </div>
            </div>
            <?php } ?>

            <div class="form-group">
                <div class="col-lg-4">
                <?= Html::dropDownList('sort', $sort, $sort_list, [
                    'onchange' => 'this.form.submit()',
                    'prompt' => Yii::t('app', 'Default sorting'),
                    'class' => 'form-control'
                ]); ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

        <hr />

        <div class="row">
            <table>
                <tbody>
                    <?php foreach ($genre_query->each() as $genre_item) { ?>
                    <tr>
                        <td>
                            <h2>
                                <a href="/genre/details?id=<?= $genre_item->id ?>">
                                    <?= $genre_item->title ?>
                                </a>
                            </h2>
                        </td>
                        <td><?= $genre_item->viewed_movies_count ?> / <?= $genre_item->total_movies_count ?></td>
                        <td>

                        </td>
                    </tr>
                    <?php }
                    unset($genre_item); ?>
                </tbody>
            </table>
        </div>

    </div>
</div>
