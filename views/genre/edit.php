<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Genre */
/* @var $form ActiveForm */

$this->title = $model->title . ' | Редактирование жанра';
?>
<div class="genre-edit">

    <div class="row">
        <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'title') ?>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

    <hr />

    <div class="row">
        <a href="/genre/delete?id=<?= $model->id ?>" class="btn btn-sm btn-danger" onclick="if (confirm('Точно удалить?')) { return true; } else { return false; }">Удалить жанр</a>
    </div>

</div><!-- genre-edit -->
