<?php

use app\models\Theater;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model Theater */
/* @var $form ActiveForm */

$this->title = $model->name . ' | Редактирование кинотеатра';
?>
<div class="theater-edit">

    <div class="row">
        <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'name') ?>
            <?= $form->field($model, 'address') ?>
            <?= $form->field($model, 'web_site') ?>
            <?= $form->field($model, 'status')->dropDownList(Theater::getStatusList()) ?>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

    <hr />

    <div class="row">
        <a href="/theater/delete?id=<?= $model->id ?>" class="btn btn-sm btn-danger" onclick="return confirm('Точно удалить?');">Удалить кинотеатр</a>
    </div>

</div><!-- theater-edit -->
