<?php

use app\models\Theater;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $theater_query \yii\db\ActiveQuery */
/* @var $theater_total_count int */
/* @var $sort string */

$this->title = Yii::t('app', 'Theaters');
?>
<div class="theater-index">

    <div class="jumbotron">
        <h1><?= $this->title ?></h1>
        <h2>Всего кинотеатров: <?= $theater_total_count ?></h2>
    </div>

    <div class="body-content">

        <div class="row">
            <?php ActiveForm::begin([
                'id' => 'theater-filters',
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-2 control-label'],
                ],
            ]); ?>

            <?php if (!Yii::$app->user->isGuest) { ?>
            <div class="form-group">
                <div class="col-lg-3">
                    <a href="/theater/create" class="btn btn-success">Создать кинотеатр</a>
                </div>
            </div>
            <?php } ?>

            <div class="form-group">
                <div class="col-lg-4">
                <?= Html::dropDownList('sort', $sort, Theater::sortList(), [
                    'onchange' => 'this.form.submit()',
                    'prompt' => Yii::t('app', 'Default sorting'),
                    'class' => 'form-control'
                ]); ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

        <hr />

        <div class="row">
            <table>
                <tbody>
                    <?php foreach ($theater_query->each() as $theater_item) { ?>
                    <tr>
                        <td>
                            <h2>
                                <a href="/theater/details?id=<?= $theater_item->id ?>">
                                    <?= $theater_item->name ?>
                                </a>
                            </h2>
                        </td>
                        <td><?= $theater_item->getLabel() ?></td>
                        <td><?= $theater_item->viewed_movies_count ?></td>
                        <td>

                        </td>
                    </tr>
                    <?php }
                    unset($theater_item);
                    ?>
                </tbody>
            </table>
        </div>

    </div>
</div>
