<?php

use app\models\Theater;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model Theater */
/* @var $form ActiveForm */

$this->title = 'Создать кинотеатр';
?>
<div class="theater-create">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
    <?= $form->field($model, 'address')->textInput() ?>
    <?= $form->field($model, 'web_site')->textInput() ?>
    <?= $form->field($model, 'status')->dropDownList(Theater::getStatusList()) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div><!-- theater-create -->
