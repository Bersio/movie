<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\widgets\ImageUploader;

/* @var $this yii\web\View */
/* @var $model app\models\Award */
/* @var $form ActiveForm */

$this->title = $model->name . ' | Редактирование награды';
?>
<div class="award-edit">

    <div class="row">
        <div class="award-image-winner-details-container">
            <a href="/award/details?id=<?= $model->id ?>">
                <img class="award-image-details" src="/images/award/<?= $model->image_winner ?>" alt="award winner image" />
            </a>
        </div>
    </div>

    <div class="row">
        <?php $ImageWinnerActiveForm = ActiveForm::begin([
            'action' => '/award/update-image?id=' . $model->id,
            'options' => ['enctype' => 'multipart/form-data']
        ]); ?>

            <div class="form-group">
                <?= Html::hiddenInput('award-image-type', 'winner', ['id' => 'award-image-winner-type', 'class' => 'form-control']) ?>
                <div class="help-block"></div>
            </div>

            <?= ImageUploader::widget([
                'id' => 'winner',
                'label' => 'Картинка \'победитель\'',
                'ImageUploadForm' => $ImageWinnerUploadForm,
                'ActiveForm' => $ImageWinnerActiveForm
            ]); ?>

            <div class="form-group">
                <?= Html::submitButton('Обновить картинку', ['class' => 'btn btn-primary']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

    <hr />

    <div class="row">
        <div class="award-image-nominant-details-container">
            <a href="/award/details?id=<?= $model->id ?>">
                <img class="award-image-details" src="/images/award/<?= $model->image_nomination ?>" alt="award winner image" />
            </a>
        </div>
    </div>

    <div class="row">
        <?php $ImageNominationActiveForm = ActiveForm::begin([
            'action' => '/award/update-image?id=' . $model->id,
            'options' => ['enctype' => 'multipart/form-data']
        ]); ?>

            <div class="form-group">
                <?= Html::hiddenInput('award-image-type', 'nominant', ['id' => 'award-image-nominant-type', 'class' => 'form-control']) ?>
                <div class="help-block"></div>
            </div>

            <?= ImageUploader::widget([
                'id' => 'nomination',
                'label' => 'Картинка \'номинант\'',
                'ImageUploadForm' => $ImageNominationUploadForm,
                'ActiveForm' => $ImageNominationActiveForm
            ]); ?>

            <div class="form-group">
                <?= Html::submitButton('Обновить картинку', ['class' => 'btn btn-primary']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

    <div class="row">
        <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'name') ?>
            <?= $form->field($model, 'web_site') ?>
            <?= $form->field($model, 'description')->textarea([
                'cols' => 100,
                'rows' => 10
            ]) ?>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

    <div class="row">
        <a href="/award/delete?id=<?= $model->id ?>" class="btn btn-sm btn-danger" onclick="if (confirm('Точно удалить?')) { return true; } else { return false; }">Удалить награду</a>
    </div>

</div><!-- award-edit -->
