<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\widgets\ImageUploader;

/* @var $this yii\web\View */
/* @var $model app\models\Award */
/* @var $form ActiveForm */

$this->title = 'Создание награды';
?>
<div class="award-create">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <?= $form->field($model, 'name')->textInput() ?>

        <?= ImageUploader::widget([
            'id' => 'winner',
            'label' => 'Картинка \'победитель\'',
            'ImageUploadForm' => $ImageWinnerUploadForm,
            'ActiveForm' => $form
        ]); ?>

        <?= ImageUploader::widget([
            'id' => 'nomination',
            'label' => 'Картинка \'номинант\'',
            'ImageUploadForm' => $ImageNominationUploadForm,
            'ActiveForm' => $form
        ]); ?>

        <?= $form->field($model, 'web_site')->textInput() ?>
        <?= $form->field($model, 'description')->textarea([
            'cols' => 100,
            'rows' => 10
        ]) ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- award-create -->
