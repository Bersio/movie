<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Movie */
/* @var $form ActiveForm */

$this->title = $model->name;
?>
<div class="award-details">
    <div class="row">
        <div class="award-image-details-container">
            <img class="award-image-winner-details" src="/images/award/<?= $model->image_winner ?>" alt="award winner image" />
            <img class="award-image-nomination-details" src="/images/award/<?= $model->image_nomination ?>" alt="award nomination image" />
        </div>
    </div>

    <div class="row">
        <h1><?= $model->name ?></h1>
        <p>
            <a href="<?= $model->web_site ?>" target="_blank"><?= $model->web_site ?></a>
        </p>
        <div><?= nl2br($model->description) ?></div>
    </div>

    <hr />

    <div class="row">
        <?php if (!Yii::$app->user->isGuest) { ?>
        <a href="/award/edit?id=<?= $model->id ?>" class="btn btn-sm btn-primary">Редактировать</a>

        <a href="<?= $model->getUrlForGoogleSearch() ?>" class="btn btn-sm btn-primary" target="_blank">Искать в google</a>

        <a href="/award/assign-movie?id=<?= $model->id ?>" class="btn btn-sm btn-primary">Назначить награду фильму</a>

        <a href="/award/assign-person?id=<?= $model->id ?>" class="btn btn-sm btn-primary">Назначить награду персоне</a>
        <?php } ?>

        <a href="/award/result?id=<?= $model->id ?>" class="btn btn-sm btn-primary">Смотреть результаты</a>
    </div>

    <hr />

    <div class="row">
        <?php ActiveForm::begin([
            'id' => 'award-nomination-filters',
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-2 control-label'],
            ],
        ]); ?>

        <?php if (!Yii::$app->user->isGuest) { ?>
        <div class="form-group">
            <div class="col-lg-3">
                <a href="/award-nomination/create?id=<?= $model->id ?>" class="btn btn-success">Создать новую номинацию</a>
            </div>
        </div>
        <?php } ?>

        <div class="form-group">
            <div class="col-lg-4">
            <?= Html::dropDownList('sort', $sort, $sort_list, [
                'onchange' => 'this.form.submit()',
                'prompt' => Yii::t('app', 'Default sorting'),
                'class' => 'form-control'
            ]); ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

    <div class="row">
        <p><b>Номинации</b></p>

        <?php foreach ($award_nomination_query->each() as $award_nomination_item) { ?>
        <p class="col-xs-offset-1">
            <a href="/award-nomination/details?id=<?= $award_nomination_item->id ?>"><?= $award_nomination_item->name ?></a>
        </p>
        <?php }
        unset($award_nomination_item);
        ?>
    </div>

</div><!-- award-details -->
