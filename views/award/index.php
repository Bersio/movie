<?php

use app\models\query\AwardQuery;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $award_query AwardQuery */
/* @var $award_total_count int */
/* @var $sort string */
/* @var $sort_list array */

$this->title = Yii::t('app', 'Awards');
?>
<div class="award-index">

    <div class="jumbotron">
        <h1><?= $this->title ?></h1>
        <h2>Всего наград: <?= $award_total_count ?></h2>
    </div>

    <div class="body-content">

        <div class="row">
            <?php ActiveForm::begin([
                'id' => 'award-filters',
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-2 control-label'],
                ],
            ]); ?>

            <?php if (!Yii::$app->user->isGuest) { ?>
            <div class="form-group">
                <div class="col-lg-3">
                    <a href="/award/create" class="btn btn-success">Создать новую награду</a>
                </div>
            </div>
            <?php } ?>

            <div class="form-group">
                <div class="col-lg-4">
                <?= Html::dropDownList('sort', $sort, $sort_list, [
                    'onchange' => 'this.form.submit()',
                    'prompt' => Yii::t('app', 'Default sorting'),
                    'class' => 'form-control'
                ]); ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

        <hr />

        <div class="row">
            <table class="award-list">
                <tbody>
                    <?php foreach ($award_query->each() as $award_item) { ?>
                    <tr>
                        <td>
                            <a href="/award/details?id=<?= $award_item->id ?>">
                                <img class="award-image" src="/images/award/<?= $award_item->image_winner ?>" alt="award image" />
                            </a>
                        </td>
                        <td>
                            <h2>
                                <a href="/award/details?id=<?= $award_item->id ?>">
                                    <?= $award_item->name ?>
                                </a>
                            </h2>
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr class="separator">
                        <td colspan="3"></td>
                    </tr>
                    <?php }
                    unset($award_item);
                    ?>
                </tbody>
            </table>
        </div>

    </div>
</div>
