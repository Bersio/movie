<?php

use app\models\Award;
use app\models\MovieAward;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/** @var yii\web\View $this */
/* @var $award Award */
/* @var $model MovieAward */
/* @var $nomination_list array */
/* @var $movie_list array */
/* @var $type_list array */

$this->title = 'Назначение награды фильму';
?>
<div class="award-assign-movie">

    <div class="jumbotron">
        <h1><?= $this->title ?></h1>
        <h2><?= $award->name ?></h2>
    </div>

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'award_nomination_id')->dropDownList($nomination_list) ?>
        <?= $form->field($model, 'movie_id')->dropDownList($movie_list) ?>
        <?= $form->field($model, 'type')->dropDownList($type_list) ?>
        <?= $form->field($model, 'year') ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- award-assign-movie -->
