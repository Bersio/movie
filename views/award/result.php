<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Award;
use app\models\Movie;
use app\models\MovieAward;
use app\models\Person;
use app\models\PersonAward;

/* @var $this yii\web\View */

$this->title = $award->name . ' результаты';
?>
<div class="award-result">
    <div class="jumbotron">
        <h1><?= empty($year) ? $award->name . ' все результаты' : $award->name . ' результаты ' . $year ?></h1>
        <h2>Просмотрено фильмов: {VIEWED_MOVIES}, не просмотрено: {MOVIE_WITHOUT_VIEW}</h2>
    </div>

    <div class="row">
        <?php ActiveForm::begin([
            'id' => 'award-result-filters',
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-2 control-label'],
            ],
        ]); ?>

        <div class="form-group">
            <div class="col-lg-4">
            <?= Html::dropDownList('year', $year, $years_list, [
                'onchange' => 'this.form.submit()',
                'prompt' => [
                    'text' => 'Выбрать год',
                    'options' => [
                        'value' => '0'
                    ],
                ],
                'class' => 'form-control'
            ]); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="only_winner" ondblclick="return false;" onmousedown="return false;">Только победители</label>
            <div class="col-lg-1">
            <?= Html::checkbox('only_winner', $only_winner, [
                'id' => 'only_winner',
                'onchange' => 'this.form.submit()',
            ]); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-3 control-label" for="only_without_view" ondblclick="return false;" onmousedown="return false;">Только непросмотренные</label>
            <div class="col-lg-1">
            <?= Html::checkbox('only_without_view', $only_without_view, [
                'id' => 'only_without_view',
                'onchange' => 'this.form.submit()',
            ]); ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

    <div class="row">

    <?php foreach ($selected_years_list as $year_item) { ?>
        <h2><?= $year_item ?></h2>

        <?php foreach ($nomination_query->each() as $nomination_item) {
            $movie_winner_list = MovieAward::find()
                    ->where(['year' => $year_item])
                    ->andWhere(['award_nomination_id' => $nomination_item->id])
                    ->andWhere(['type' => Award::TYPE_WINNER])
                    ->orderBy(['id' => SORT_ASC])
                    ->all();

            if (!$only_winner) {
                $movie_nominant_list = MovieAward::find()
                        ->where(['year' => $year_item])
                        ->andWhere(['award_nomination_id' => $nomination_item->id])
                        ->andWhere(['type' => Award::TYPE_NOMINATION])
                        ->orderBy(['id' => SORT_ASC])
                        ->all();
            } else {
                $movie_nominant_list = [];
            }

            $person_winner_list = PersonAward::find()
                    ->where(['year' => $year_item])
                    ->andWhere(['award_nomination_id' => $nomination_item->id])
                    ->andWhere(['type' => Award::TYPE_WINNER])
                    ->orderBy(['id' => SORT_ASC])
                    ->all();

            if (!$only_winner) {
                $person_nominant_list = PersonAward::find()
                        ->where(['year' => $year_item])
                        ->andWhere(['award_nomination_id' => $nomination_item->id])
                        ->andWhere(['type' => Award::TYPE_NOMINATION])
                        ->orderBy(['id' => SORT_ASC])
                        ->all();
            } else {
                $person_nominant_list = [];
            }
        ?>
        <h3><?= $nomination_item->name ?></h3>

        <div class="row">
            <table class="movie-list">
                <tbody>
                    <?php foreach ($movie_winner_list as $winner_item) {
                        $movie = Movie::findOne($winner_item->movie_id);

                        if ($only_without_view && !empty($movie->views)) {
                            continue;
                        }

                        $award->checkView($movie);
                    ?>
                    <tr style="border: 5px solid gold; background-color: yellow;">
                        <td>
                            <a href="/movie/details?id=<?= $movie->id ?>">
                                <img class="movie-image" src="/images/movie/<?= $movie->image ?>" alt="movie image" />
                            </a>
                        </td>
                        <td>
                            <h2>
                                <a href="/movie/details?id=<?= $movie->id ?>">
                                    <?= $movie->title ?>
                                </a>
                            </h2>
                            <h4><?= $movie->original_title ?></h4>
                            <p><?= $movie->year ?></p>
                        </td>
                        <td>
                            <?php if (!empty($movie->average_rating)) { ?>
                            <span style="color: gold; font-size: 36px;"><?= round($movie->average_rating, 2) ?></span>
                            <?php } else { ?>
                            <span style="color: grey; font-size: 36px;"></span>
                            <?php } ?>
                        </td>
                        <td>
                            <?php if (!empty($movie->views)) { ?>
                            <span style="color: grey;"><?= (new \DateTime($movie->views[0]->created_at))->format('d.m.Y') ?></span>
                            <?php } else { ?>
                            <span style="color: grey;"></span>
                            <?php } ?>
                        </td>
                        <td>

                        </td>
                    </tr>
                    <?php } 
                    unset($winner_item, $movie_winner_list, $movie); ?>

                    <?php foreach ($movie_nominant_list as $nominant_item) {
                        $movie = Movie::findOne($nominant_item->movie_id);

                        if ($only_without_view && !empty($movie->views)) {
                            continue;
                        }

                        $award->checkView($movie);
                    ?>
                    <tr style="border: 5px solid aqua; background-color: aquamarine;">
                        <td>
                            <a href="/movie/details?id=<?= $movie->id ?>">
                                <img class="movie-image" src="/images/movie/<?= $movie->image ?>" alt="movie image" />
                            </a>
                        </td>
                        <td>
                            <h2>
                                <a href="/movie/details?id=<?= $movie->id ?>">
                                    <?= $movie->title ?>
                                </a>
                            </h2>
                            <h4><?= $movie->original_title ?></h4>
                            <p><?= $movie->year ?></p>
                        </td>
                        <td>
                            <?php if (!empty($movie->average_rating)) { ?>
                            <span style="color: gold; font-size: 36px;"><?= round($movie->average_rating, 2) ?></span>
                            <?php } else { ?>
                            <span style="color: grey; font-size: 36px;"></span>
                            <?php } ?>
                        </td>
                        <td>
                            <?php if (!empty($movie->views)) { ?>
                            <span style="color: grey;"><?= (new \DateTime($movie->views[0]->created_at))->format('d.m.Y') ?></span>
                            <?php } else { ?>
                            <span style="color: grey;"></span>
                            <?php } ?>
                        </td>
                        <td>

                        </td>
                    </tr>
                    <?php } 
                    unset($nominant_item, $movie_nominant_list, $movie); ?>

                    <?php foreach ($person_winner_list as $winner_item) {
                        $person = Person::findOne($winner_item->person_id);
                        $movie = Movie::findOne($winner_item->movie_id);

                        if ($only_without_view && !empty($movie->views)) {
                            continue;
                        }

                        $award->checkView($movie);
                    ?>
                    <tr style="border: 5px solid gold; background-color: yellow;">
                        <td>
                            <a href="/person/details?id=<?= $person->id ?>">
                                <img class="person-image" src="/images/person/<?= $person->image ?>" alt="person image" />
                            </a>
                        </td>
                        <td>
                            <h2>
                                <a href="/person/details?id=<?= $person->id ?>">
                                    <?= $person->getFullName() ?>
                                </a>
                            </h2>
                        </td>
                        <td>
                            <h2>
                                (<a href="/movie/details?id=<?= $movie->id ?>">
                                    <img class="movie-image-small" src="/images/movie/<?= $movie->image ?>" alt="movie image" />
                                    <?= $movie->title ?>
                                </a>
                                <?php if ($movie->average_rating) { ?>
                                 - <span style="color: gold; font-size: 36px;"><?= round($movie->average_rating, 2) ?></span>
                                <?php } ?>)
                            </h2>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <?php } 
                    unset($winner_item, $person_winner_list, $person, $movie); ?>

                    <?php foreach ($person_nominant_list as $nominant_item) {
                        $person = Person::findOne($nominant_item->person_id);
                        $movie = Movie::findOne($nominant_item->movie_id);

                        if ($only_without_view && !empty($movie->views)) {
                            continue;
                        }

                        $award->checkView($movie);
                    ?>
                    <tr style="border: 5px solid aqua; background-color: aquamarine;">
                        <td>
                            <a href="/person/details?id=<?= $person->id ?>">
                                <img class="person-image" src="/images/person/<?= $person->image ?>" alt="person image" />
                            </a>
                        </td>
                        <td>
                            <h2>
                                <a href="/person/details?id=<?= $person->id ?>">
                                    <?= $person->getFullName() ?>
                                </a>
                            </h2>
                        </td>
                        <td>
                            <h2>
                                (<a href="/movie/details?id=<?= $movie->id ?>">
                                    <img class="movie-image-small" src="/images/movie/<?= $movie->image ?>" alt="movie image" />
                                    <?= $movie->title ?>
                                </a>
                                <?php if ($movie->average_rating) { ?>
                                 - <span style="color: gold; font-size: 36px;"><?= round($movie->average_rating, 2) ?></span>
                                <?php } ?>)
                            </h2>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <?php } 
                    unset($nominant_item, $person_nominant_list, $person, $movie); ?>
                </tbody>
            </table>
        </div>

        <?php }
        unset($nomination_item);
        ?>

        <hr />
    <?php }
    unset($year_item);
    ?>

    </div>
</div><!-- award-result -->
