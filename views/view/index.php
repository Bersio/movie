<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $view_query app\models\query\MovieQuery */
/* @var $view_total_count int */
/* @var $genre_id int */
/* @var $genre_list array */
/* @var $award_id int */
/* @var $award_list array */
/* @var $theater_id int */
/* @var $theater_list array */
/* @var $special_showing_id int */
/* @var $special_showing_list array */
/* @var $is_favorite int */
/* @var $sort string */
/* @var $sort_list array */

$this->title = Yii::t('app', 'Views');
?>
<div class="view-index">

    <div class="jumbotron">
        <h1><?= $this->title ?></h1>
        <h2>Всего фильмов просмотрено: <?= $view_total_count ?></h2>
    </div>

    <div class="body-content">

        <?php ActiveForm::begin([
            'id' => 'view-filters',
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-2 control-label'],
            ],
        ]); ?>

        <div class="row filter-row">

            <?php if (!Yii::$app->user->isGuest) { ?>
            <div class="col-lg-4 filter-item">
                <a href="/view/create" class="btn btn-success">Отметить просмотр</a>
            </div>
            <?php } ?>

            <div class="col-lg-4 filter-item">
            <?= Html::dropDownList('sort', $sort, $sort_list, [
                'onchange' => 'this.form.submit()',
                'prompt' => Yii::t('app', 'Default sorting'),
                'class' => 'form-control'
            ]); ?>
            </div>

            <div class="col-lg-4 filter-item">
            <?= Html::dropDownList('genre_id', $genre_id, $genre_list, [
                'onchange' => 'this.form.submit()',
                'prompt' => [
                    'text' => 'Выбрать жанр',
                    'options' => [
                        'value' => '0'
                    ],
                ],
                'class' => 'form-control'
            ]); ?>
            </div>

        </div>

        <div class="row filter-row">
            <div class="col-lg-4 filter-item">
            <?= Html::dropDownList('award_id', $award_id, $award_list, [
                'onchange' => 'this.form.submit()',
                'prompt' => [
                    'text' => 'Выбрать награду',
                    'options' => [
                        'value' => '0'
                    ],
                ],
                'class' => 'form-control'
            ]); ?>
            </div>

            <div class="col-lg-4 filter-item">
            <?= Html::dropDownList('theater_id', $theater_id, $theater_list, [
                'onchange' => 'this.form.submit()',
                'prompt' => [
                    'text' => 'Кинотеатр',
                    'options' => [
                        'value' => '0'
                    ],
                ],
                'class' => 'form-control'
            ]); ?>
            </div>

            <div class="col-lg-4 filter-item">
            <?= Html::dropDownList('special_showing_id', $special_showing_id, $special_showing_list, [
                'onchange' => 'this.form.submit()',
                'prompt' => [
                    'text' => 'Спец. показ',
                    'options' => [
                        'value' => '0'
                    ],
                ],
                'class' => 'form-control'
            ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 filter-item">
                <?= Html::dropDownList('is_favorite', $is_favorite, ['1' => 'Избранное'], [
                    'onchange' => 'this.form.submit()',
                    'prompt' => [
                        'text' => 'Все фильмы',
                        'options' => [
                            'value' => '0'
                        ],
                    ],
                    'class' => 'form-control'
                ]); ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

        <hr />

        <div class="row">
            <table class="movie-list">
                <tbody>
                    <?php foreach ($view_query->each() as $view_item) { ?>
                    <tr>
                        <td>
                            <a href="/movie/details?id=<?= $view_item->movie->id ?>">
                                <img class="movie-image" src="/images/movie/<?= $view_item->movie->image ?>" alt="movie image" />
                            </a>
                        </td>
                        <td>
                            <h2>
                                <a href="/movie/details?id=<?= $view_item->movie->id ?>">
                                    <?= $view_item->movie->title ?>
                                </a>
                            </h2>
                            <h4><?= $view_item->movie->original_title ?></h4>
                            <p><?= $view_item->movie->year ?></p>
                        </td>
                        <td>
                            <?php if (!empty($view_item->rating)) { ?>
                            <span style="color: gold; font-size: 24px;"><?= $view_item->rating ?></span>
                            <?php } else { ?>
                            <span style="color: grey; font-size: 24px;">-</span>
                            <?php } ?>
                        </td>
                        <td>
                            <span style="color: grey;">
                            <?= (new \DateTime($view_item->created_at))->format('d.m.Y') ?>
                            </span>
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr class="separator">
                        <td colspan="5"></td>
                    </tr>
                    <?php }
                    unset($view_item);
                    ?>
                </tbody>
            </table>
        </div>

    </div>
</div>
