<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var @theater_list array */
/* @var @special_showing_list array */

?>
<?= $form->beginField($model, 'theater_id') ?>
<label class="control-label" for="view-theater_id"><?= Html::encode('Кинотеатр') ?></label>
<div class="col-lg-12 theater-wrapper">
    <div class="col-lg-12 view-theater-left">
        <?= Html::activeDropDownList($model, 'theater_id', $theater_list, [
            'prompt' => 'Кинотеатр',
            'class' => 'form-control',
            'onchange' => "
                var selected_value = $(event.target).val();
                if (selected_value) {
                    $('.view-theater-left').removeClass('col-lg-12');
                    $('.view-theater-left').addClass('col-lg-7');
                    $('.view-theater-center, .view-theater-right').show();
                } else {
                    $('.view-theater-left').removeClass('col-lg-7');
                    $('.view-theater-left').addClass('col-lg-12');
                    $('.view-theater-center, .view-theater-right').hide();
                    $('.view-theater-right input[type=checkbox]').prop('checked', false);
                }
            ",
        ]) ?>
    </div>
    <div class="col-lg-2 view-theater-center">
        <label class="control-label" for="view-special_showing"><?= Html::encode('Спец. показ') ?></label>
    </div>
    <div class="col-lg-3 view-theater-right">
        <?= Html::activeDropDownList($model, 'special_showing_id', $special_showing_list, [
            'prompt' => 'Обычный',
            'class' => 'form-control',
            'label' => false,
        ]) ?>
    </div>
</div>
<?= $form->endField() ?>
