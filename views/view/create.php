<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\View */
/* @var @movie_list array */
/* @var @audio_list array */
/* @var @drink_list array */
/* @var @theater_list array */
/* @var @special_showing_list array */
/* @var @rating_list array */

$this->title = 'Отметить просмотр';
?>
<div class="view-create">

    <div class="jumbotron">
        <h1><?= $this->title ?></h1>
    </div>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'movie_id')->dropDownList($movie_list, [
        'prompt' => 'Выбрать фильм'
    ]) ?>

    <?= $form->field($model, 'rating')->dropDownList($rating_list, [
        'prompt' => 'Рейтинг'
    ]) ?>

    <?= $form->field($model, 'created_at') ?>

    <?= $form->field($model, 'audio_id')->dropDownList($audio_list, [
        'prompt' => 'Озвучка'
    ]) ?>
    
    <?= $this->context->renderPartial('partials/create/theater', [
        'form' => $form,
        'model' => $model,
        'theater_list' => $theater_list,
        'special_showing_list' => $special_showing_list,
    ]) ?>

    <?= $form->field($model, 'drink_id')->dropDownList($drink_list, [
        'prompt' => 'Напиток'
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div><!-- view-create -->
