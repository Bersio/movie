<?php

use app\models\query\AudioQuery;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/** @var yii\web\View $this */
/* @var $audio_query AudioQuery */
/* @var $audio_total_count int */
/* @var $sort string */
/* @var $sort_list array */

$this->title = Yii::t('app', 'Audio');
?>
<div class="audio-index">

    <div class="jumbotron">
        <h1><?= $this->title ?></h1>
        <h2>Всего озвучек: <?= $audio_total_count ?></h2>
    </div>

    <div class="body-content">

        <div class="row">
            <?php ActiveForm::begin([
                'id' => 'audio-filters',
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-2 control-label'],
                ],
            ]); ?>

            <?php if (!Yii::$app->user->isGuest) { ?>
            <div class="form-group">
                <div class="col-lg-3">
                    <a href="/audio/create" class="btn btn-success">Создать озвучку</a>
                </div>
            </div>
            <?php } ?>

            <div class="form-group">
                <div class="col-lg-4">
                <?= Html::dropDownList('sort', $sort, $sort_list, [
                    'onchange' => 'this.form.submit()',
                    'prompt' => Yii::t('app', 'Default sorting'),
                    'class' => 'form-control'
                ]); ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

        <hr />

        <div class="row">
            <table>
                <tbody>
                    <?php foreach ($audio_query->each() as $audio_item) { ?>
                    <tr>
                        <td>
                            <h2>
                                <a href="/audio/details?id=<?= $audio_item->id ?>">
                                    <?= $audio_item->name ?>
                                </a>
                            </h2>
                        </td>
                        <td><?= $audio_item->viewed_movies_count ?></td>
                        <td>

                        </td>
                    </tr>
                    <?php }
                    unset($audio_item);
                    ?>
                </tbody>
            </table>
        </div>

    </div>
</div>
