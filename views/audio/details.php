<?php

use app\models\Audio;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model Audio */
/* @var $view_query \yii\db\ActiveQuery */
/* @var $is_favorite int */
/* @var $sort string */
/* @var $sort_list array */

$this->title = $model->name;
?>
<div class="audio-details">

    <div class="jumbotron">
        <h1><?= $model->name ?></h1>
        <h2>Всего фильмов просмотрено: <?= $model->viewed_movies_count ?></h2>
    </div>

    <div class="row">
        <?php ActiveForm::begin([
            'id' => 'audio-filters',
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-2 control-label'],
            ],
        ]); ?>

        <?php if (!Yii::$app->user->isGuest) { ?>
        <div class="form-group">
            <div class="col-lg-3">
                <a href="/audio/edit?id=<?= $model->id ?>" class="btn btn-sm btn-primary">Редактировать</a>
            </div>
        </div>
        <?php } ?>

        <div class="form-group">
            <div class="col-lg-4">
            <?= Html::dropDownList('sort', $sort, $sort_list, [
                'onchange' => 'this.form.submit()',
                'prompt' => Yii::t('app', 'Default sorting'),
                'class' => 'form-control'
            ]); ?>
            </div>
        </div>

        <div class="col-lg-4 filter-item">
            <?= Html::dropDownList('is_favorite', $is_favorite, ['1' => 'Избранное'], [
                'onchange' => 'this.form.submit()',
                'prompt' => [
                    'text' => 'Все фильмы',
                    'options' => [
                        'value' => '0'
                    ],
                ],
                'class' => 'form-control'
            ]); ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

    <hr />

    <div class="row">
        <table class="movie-list">
            <tbody>
                <?php foreach ($view_query->each() as $view_item) { ?>
                <tr>
                    <td>
                        <a href="/movie/details?id=<?= $view_item->movie->id ?>">
                            <img class="movie-image" src="/images/movie/<?= $view_item->movie->image ?>" alt="movie image" />
                        </a>
                    </td>
                    <td>
                        <h2>
                            <a href="/movie/details?id=<?= $view_item->movie->id ?>">
                                <?= $view_item->movie->title ?>
                            </a>
                        </h2>
                        <h4><?= $view_item->movie->original_title ?></h4>
                        <p><?= $view_item->movie->year ?></p>
                    </td>
                    <td>
                        <?php if (!empty($view_item->rating)) { ?>
                        <span style="color: gold; font-size: 24px;"><?= $view_item->rating ?></span>
                        <?php } else { ?>
                        <span style="color: grey; font-size: 24px;">-</span>
                        <?php } ?>
                    </td>
                    <td>
                        <span style="color: grey;">
                        <?= (new \DateTime($view_item->created_at))->format('d.m.Y') ?>
                        </span>
                    </td>
                    <td>

                    </td>
                </tr>
                <tr class="separator">
                    <td colspan="5"></td>
                </tr>
                <?php }
                unset($view_item);
                ?>
            </tbody>
        </table>
    </div>

</div><!-- audio-details -->
