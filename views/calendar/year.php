<?php

/* @var $this yii\web\View */
/* @var $calendar app\models\Calendar */
/* @var $date_now DateTime */
/* @var $previous_year_date DateTime */
/* @var $next_year_date DateTime */

$this->title = 'Итоги года';

?>
<div class="calendar-year">

    <div class="jumbotron">
        <h1><?= $this->title ?></h1>
        <h2><?= $date_now->format('Y') ?></h2>
        <h3>
            <a href="/calendar/year?year=<?= $previous_year_date->format('Y') ?>">Прошлый год</a>
             | 
            <a href="/calendar/year?year=<?= $next_year_date->format('Y') ?>">Следующий год</a>
        </h3>
    </div>

    <div class="body-content">

        <div class="row">
            <h3>Всего фильмов просмотрено: <?= $calendar->total_views_count ?>, 
                из них в кинотеатре: <?= $calendar->theater_views_count ?></h3>
        </div>

        <?php if (!empty($calendar->views_by_months)) { ?>
        <div class="row">
            <h3>Просмотры по месяцам</h3>
            <table>
                <?php foreach ($calendar->views_by_months as $views_by_month_item) { ?>
                <tr>
                    <td>
                        <a href="/calendar/index?year=<?= $date_now->format('Y') ?>&month=<?= $views_by_month_item['month_number'] ?>"><?= $views_by_month_item['month'] ?></a>: 
                    </td>
                    <td><?= $views_by_month_item['views_count'] ?></td>
                </tr>
                <?php } unset($views_by_month_item); ?>
            </table>
        </div>
        <?php } ?>

        <?php if (!empty($calendar->ratings)) { ?>
        <div class="row">
            <h3>Выставленные рейтинги</h3>
            <table>
                <?php foreach ($calendar->ratings as $content) { ?>
                <tr>
                    <td><?= $content['rating'] ?>: </td>
                    <td><?= $content['views_count'] ?></td>
                </tr>
                <?php } unset($content); ?>
            </table>
        </div>
        <?php } ?>

        <?php if (!empty($calendar->views_by_genres)) { ?>
        <div class="row">
            <h3>Просмотры по жанрам</h3>
            <table>
                <?php foreach ($calendar->views_by_genres as $content) { ?>
                <tr>
                    <td>
                        <a href="/genre/details?id=<?= $content['genre']->id ?>"><?= $content['genre']->title ?></a>: 
                    </td>
                    <td><?= $content['views_count'] ?></td>
                </tr>
                <?php } unset($content); ?>
            </table>
        </div>
        <?php } ?>

        <?php if (!empty($calendar->views_by_countries)) { ?>
        <div class="row">
            <h3>Просмотры по странам</h3>
            <table>
                <?php foreach ($calendar->views_by_countries as $content) { ?>
                <tr>
                    <td>
                        <a href="/country/details?id=<?= $content['country']->id ?>"><?= $content['country']->ru_name ?></a>: 
                    </td>
                    <td><?= $content['views_count'] ?></td>
                </tr>
                <?php } unset($content); ?>
            </table>
        </div>
        <?php } ?>

        <?php if (!empty($calendar->views_by_actors)) { ?>
        <div class="row">
            <h3>Популярные актёры</h3>
            <table>
                <?php foreach ($calendar->views_by_actors as $content) { ?>
                <tr>
                    <td>
                        <a href="/person/details?id=<?= $content['person']->id ?>"><?= $content['person']->first_name ?> <?= $content['person']->last_name ?></a>: 
                    </td>
                    <td><?= $content['views_count'] ?></td>
                </tr>
                <?php } unset($content); ?>
            </table>
        </div>
        <?php } ?>

        <?php if (!empty($calendar->views_by_directors)) { ?>
        <div class="row">
            <h3>Популярные режиссёры</h3>
            <table>
                <?php foreach ($calendar->views_by_directors as $content) { ?>
                <tr>
                    <td>
                        <a href="/person/details?id=<?= $content['person']->id ?>"><?= $content['person']->first_name ?> <?= $content['person']->last_name ?></a>: 
                    </td>
                    <td><?= $content['views_count'] ?></td>
                </tr>
                <?php } unset($content); ?>
            </table>
        </div>
        <?php } ?>

        <?php if (!empty($calendar->views_by_audio)) { ?>
        <div class="row">
            <h3>Популярные озвучки</h3>
            <table>
                <?php foreach ($calendar->views_by_audio as $content) { ?>
                <tr>
                    <td>
                        <a href="/audio/details?id=<?= $content['audio']->id ?>"><?= $content['audio']->name ?></a>: 
                    </td>
                    <td><?= $content['views_count'] ?></td>
                </tr>
                <?php } unset($content); ?>
            </table>
        </div>
        <?php } ?>

        <?php if (!empty($calendar->views_by_theaters)) { ?>
        <div class="row">
            <h3>Популярные кинотеатры</h3>
            <table>
                <?php foreach ($calendar->views_by_theaters as $content) { ?>
                <tr>
                    <td>
                        <a href="/theater/details?id=<?= $content['theater']->id ?>"><?= $content['theater']->name ?></a>: 
                    </td>
                    <td><?= $content['views_count'] ?></td>
                </tr>
                <?php } unset($content); ?>
            </table>
        </div>
        <?php } ?>

        <?php if (!empty($calendar->views_by_drinks)) { ?>
        <div class="row">
            <h3>Популярные напитки</h3>
            <table>
                <?php foreach ($calendar->views_by_drinks as $content) { ?>
                <tr>
                    <td>
                        <a href="/drink/details?id=<?= $content['drink']->id ?>"><?= $content['drink']->name ?></a>: 
                    </td>
                    <td><?= $content['views_count'] ?></td>
                </tr>
                <?php } unset($content); ?>
            </table>
        </div>
        <?php } ?>
    </div>
</div>
