<?php

use app\models\View;

/* @var $this yii\web\View */
/* @var $date_now DateTime */
/* @var $first_day_of_month_number int */
/* @var $days_in_month_count int */
/* @var $previous_month_date DateTime */
/* @var $next_month_date DateTime */
/* @var $viewed_movies_count int */

$this->registerCssFile("/css/calendar.css");

$this->title = Yii::t('app', 'Calendar');

$skip = true;
$day_value = null;
$is_month_over = false;
?>
<div class="calendar-index">

    <div class="jumbotron">
        <h1><?= $this->title ?></h1>
        <h2><?= $date_now->format('F Y') ?>, фильмов просмотрено: <?= $viewed_movies_count ?></h2>
        <h3>
            <a href="/calendar/year">Итоги года</a>
        </h3>
        <h3>
            <a href="/calendar/index?year=<?= $previous_month_date->format('Y') ?>&month=<?= $previous_month_date->format('m') ?>">Прошлый месяц</a>
             | 
            <a href="/calendar/index?year=<?= $next_month_date->format('Y') ?>&month=<?= $next_month_date->format('m') ?>">Следующий месяц</a>
        </h3>
    </div>

    <div class="body-content">

        <div class="row">

            <table id="view-calendar">
                <tbody>
                    <?php while (true) { ?>

                    <tr>
                        <?php for ($i_cols = 1; $i_cols <= 7; $i_cols++) {
                            $view = $movie_value = null;

                            if ($skip === true && $i_cols >= $first_day_of_month_number) {
                                $skip = false;
                                $day_value = 0;
                            }

                            if ($skip === false && $is_month_over === false) {
                                $day_value++;
                            }

                            if ($day_value > $days_in_month_count) {
                                $skip = true;
                                $day_value = null;
                            }

                            if ($is_month_over === true) {
                                $skip = true;
                                $day_value = null;
                            }

                            if (($day_value +1) > $days_in_month_count) {
                                $is_month_over = true;
                            }

                            if ($day_value > 0) {
                                $current_date = $date_now->format('Y-m-');
                                if ($day_value < 10) {
                                    $current_date .= '0';
                                }
                                $current_date .= $day_value;

                                $view = View::find()
                                    ->with(['movie'])
                                    ->where(['created_at' => $current_date])
                                    ->one();
                            }

                            if (!empty($view)) {
                                $movie_value = 
                                    '<a href="/movie/details?id=' . $view->movie->id . '">' . 
                                        $view->movie->title . 
                                    '</a>';
                            }

                            if ($skip === true) {
                                $cell_color = 'gray';
                            } else if (!empty($view)) {
                                $cell_color = 'green';
                            } else if (empty($view)) {
                                $cell_color = 'yellow';
                            }
                        ?>
                        <td class="<?= $cell_color ?>">
                            <p class="day"><?= $day_value ?></p>
                            <p class="movie"><?= $movie_value ?></p>
                        </td>
                        <?php } ?>

                    </tr>

                    <?php 
                        if ($is_month_over === true) {
                            break;
                        }
                    } ?>
                </tbody>
            </table>

        </div>

    </div>
</div>
