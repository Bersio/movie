<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Queue;

/* @var $this yii\web\View */
/* @var $queue_query app\models\query\MovieQuery */
/* @var $queue_total_count int */
/* @var $genre_id int */
/* @var $genre_list array */
/* @var $award_id int */
/* @var $award_list array */
/* @var $is_favorite int */
/* @var $year int */
/* @var $year_list array */
/* @var $sort string */
/* @var $sort_list array */

$this->title = Yii::t('app', 'Queue');
?>
<div class="queue-index">

    <div class="jumbotron">
        <h1><?= $this->title ?></h1>
        <h2>Всего фильмов в очереди: <?= $queue_total_count ?></h2>
    </div>

    <div class="body-content">

        <?php ActiveForm::begin([
            'id' => 'queue-filters',
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-2 control-label'],
            ],
        ]); ?>

        <div class="row filter-row">
            <?php if (!Yii::$app->user->isGuest) { ?>
            <div class="col-lg-4 filter-item">
                <a href="/queue/create" class="btn btn-success">Добавить фильм в очередь</a>
            </div>
            <?php } ?>

            <div class="col-lg-4 filter-item">
                <a href="<?= Queue::generateUrlForRandomMovie() ?>" class="btn btn-primary">Выбрать случайный фильм из очереди</a>
            </div>

            <div class="col-lg-4 filter-item">
            <?= Html::dropDownList('sort', $sort, $sort_list, [
                'onchange' => 'this.form.submit()',
                'prompt' => Yii::t('app', 'Default sorting'),
                'class' => 'form-control'
            ]); ?>
            </div>
        </div>

        <div class="row filter-row">
            <div class="col-lg-4 filter-item">
            <?= Html::dropDownList('genre_id', $genre_id, $genre_list, [
                'onchange' => 'this.form.submit()',
                'prompt' => [
                    'text' => 'Выбрать жанр',
                    'options' => [
                        'value' => '0'
                    ],
                ],
                'class' => 'form-control'
            ]); ?>
            </div>

            <div class="col-lg-4 filter-item">
            <?= Html::dropDownList('award_id', $award_id, $award_list, [
                'onchange' => 'this.form.submit()',
                'prompt' => [
                    'text' => 'Выбрать награду',
                    'options' => [
                        'value' => '0'
                    ],
                ],
                'class' => 'form-control'
            ]); ?>
            </div>

            <div class="col-lg-4 filter-item">
            <?= Html::dropDownList('year', $year, $year_list, [
                'onchange' => 'this.form.submit()',
                'prompt' => [
                    'text' => 'Выбрать год',
                    'options' => [
                        'value' => '0'
                    ],
                ],
                'class' => 'form-control'
            ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 filter-item">
                <?= Html::dropDownList('is_favorite', $is_favorite, ['1' => 'Избранное'], [
                    'onchange' => 'this.form.submit()',
                    'prompt' => [
                        'text' => 'Все фильмы',
                        'options' => [
                            'value' => '0'
                        ],
                    ],
                    'class' => 'form-control'
                ]); ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

        <hr />

        <div class="row">
            <table class="movie-list">
                <tbody>
                    <?php foreach ($queue_query->each() as $movie_item) { ?>
                    <tr>
                        <td>
                            <a href="/movie/details?id=<?= $movie_item->id ?>">
                                <img class="movie-image" src="/images/movie/<?= $movie_item->image ?>" alt="movie image" />
                            </a>
                        </td>
                        <td>
                            <h2>
                                <a href="/movie/details?id=<?= $movie_item->id ?>">
                                    <?= $movie_item->title ?>
                                </a>
                            </h2>
                            <h4><?= $movie_item->original_title ?></h4>
                            <div>
                                <span class="movie-year"><?= $movie_item->year ?></span>
                                <?php foreach ($movie_item->movieAwards as $movie_award_item) { ?>
                                    <img
                                        class="queue-award-image"
                                        src="<?= $movie_award_item->getImageUrl() ?>"
                                        alt="award image"
                                        title="<?= $movie_award_item->getInfo() ?>" />
                                <?php }
                                unset($movie_award_item);
                                ?>
                                <?= (!empty($movie_item->movieAwards) && !empty($movie_item->personAwards)) ? ' - ' : '' ?>
                                <?php foreach ($movie_item->personAwards as $person_award_item) { ?>
                                    <img
                                        class="queue-award-image"
                                        src="<?= $person_award_item->getImageUrl() ?>"
                                        alt="award image"
                                        title="<?= $person_award_item->getInfo() ?>" />
                                <?php }
                                unset($person_award_item);
                                ?>
                            </div>
                        </td>
                        <td>
                            <span style="color: grey;">
                            <?= (new \DateTime($movie_item->created_at))->format('d.m.Y') ?>
                            </span>
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr class="separator">
                        <td colspan="4"></td>
                    </tr>
                    <?php }
                    unset($movie_item);
                    ?>
                </tbody>
            </table>
        </div>

    </div>
</div>
