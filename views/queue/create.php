<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Queue */

$this->title = 'Добавить фильм в очередь';
?>
<div class="queue-create">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'movie_id')->dropDownList($movie_list, [
            'prompt' => 'Выбрать фильм'
        ]) ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- queue-create -->
