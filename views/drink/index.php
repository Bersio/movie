<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/** @var yii\web\View $this */
/* @var $drink_query \yii\db\ActiveQuery */
/* @var $drink_total_count int */
/* @var $alcohol string */
/* @var $alcohol_list array */
/* @var $sort string */
/* @var $sort_list array */

$this->title = Yii::t('app', 'Drinks');
?>
<div class="drink-index">

    <div class="jumbotron">
        <h1><?= $this->title ?></h1>
        <h2>Всего напитков: <?= $drink_total_count ?></h2>
    </div>

    <div class="body-content">

        <div class="row">
            <?php ActiveForm::begin([
                'id' => 'drink-filters',
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-2 control-label'],
                ],
            ]); ?>

            <?php if (!Yii::$app->user->isGuest) { ?>
            <div class="form-group">
                <div class="col-lg-3">
                    <a href="/drink/create" class="btn btn-success">Создать напиток</a>
                </div>
            </div>
            <?php } ?>

            <div class="form-group">
                <div class="col-lg-4">
                <?= Html::dropDownList('alcohol', $alcohol, $alcohol_list, [
                    'onchange' => 'this.form.submit()',
                    'prompt' => 'Алкоголь',
                    'class' => 'form-control'
                ]); ?>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-4">
                <?= Html::dropDownList('sort', $sort, $sort_list, [
                    'onchange' => 'this.form.submit()',
                    'prompt' => Yii::t('app', 'Default sorting'),
                    'class' => 'form-control'
                ]); ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

        <hr />

        <div class="row">
            <table>
                <tbody>
                    <?php foreach ($drink_query->each() as $drink_item) { ?>
                    <tr>
                        <td>
                            <h2>
                                <a href="/drink/details?id=<?= $drink_item->id ?>">
                                    <?= $drink_item->name ?>
                                </a>
                            </h2>
                        </td>
                        <td><?= $drink_item->viewed_movies_count ?></td>
                        <td>

                        </td>
                    </tr>
                    <?php }
                    unset($drink_item);
                    ?>
                </tbody>
            </table>
        </div>

    </div>
</div>
