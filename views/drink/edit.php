<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Drink */
/* @var $form ActiveForm */

$this->title = $model->name . ' | Редактирование напитка';
?>
<div class="drink-edit">

    <div class="row">
        <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'name') ?>
            <?= $form->field($model, 'alcohol')->checkbox() ?>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

    <hr />

    <div class="row">
        <a href="/drink/delete?id=<?= $model->id ?>" class="btn btn-sm btn-danger" onclick="return confirm('Точно удалить?');">Удалить напиток</a>
    </div>

</div><!-- drink-edit -->
