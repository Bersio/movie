<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\widgets\ImageUploader;

/* @var $this yii\web\View */
/* @var $model app\models\Movie */
/* @var $ImageUploadForm app\models\image\ImageUploadForm */

$this->title = 'Создание фильма';
?>
<div class="movie-create">
    <p>
        <a href="/movie/create-by-imdb-url">Создание фильма по ссылке IMDB</a>
    </p>

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <?= $form->field($model, 'title')->textInput(['autofocus' => true]) ?>
        <?= $form->field($model, 'year')->textInput() ?>
        <?= $form->field($model, 'original_title')->textInput() ?>

        <?= ImageUploader::widget([
            'label' => 'Постер',
            'ImageUploadForm' => $ImageUploadForm,
            'ActiveForm' => $form
        ]); ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- movie-create -->
