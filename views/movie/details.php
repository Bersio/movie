<?php

use yii\helpers\Html;
use app\models\image\Thumb;

/** @var yii\web\View $this */
/* @var $model app\models\Movie */
/* @var $movie_genre_list array */
/* @var $movie_country_list array */
/* @var $movie_actor_list array */
/* @var $movie_director_list array */
/* @var $comment_list array */
/* @var $movie_award_list array */
/* @var $person_award_list array */

$this->title = $model->title;

$this->registerCssFile('/css/image-viewer.css', ['depends' => 'app\assets\AppAsset']);
$this->registerJsFile('/js/image-viewer.min.js', ['depends' => 'app\assets\AppAsset']);
?>
<div class="movie-details">

    <div class="row">
        <div class="movie-image-details-container">
            <img class="movie-image-details" src="/images/movie/<?= $model->image ?>" alt="movie image" />
        </div>
    </div>

    <div class="row">
        <h1><?= $model->title ?></h1>
        <h3><i style="color: grey;">(<?= $model->original_title ?>)</i></h3>
        <p>Рейтинг: 
            <?php if (!empty($model->average_rating)) { ?>
            <span style="color: gold; font-size: 24px;"><?= round($model->average_rating, 2) ?></span>
            <?php } else { ?>
            <span style="color: grey; font-size: 24px;">-</span>
            <?php } ?>
        </p>
        <p>Год: <?= $model->year ?></p>
    </div>

    <?php if (!Yii::$app->user->isGuest) { ?>
    <div class="row">
        <a href="/movie/edit?id=<?= $model->id ?>" class="btn btn-sm btn-primary">Редактировать</a>
        <a href="<?= $model->getUrlForGoogleSearch() ?>" class="btn btn-sm btn-primary" target="_blank">Искать в google</a>
    </div>
    <?php } ?>

    <hr />

    <?php if (!empty($movie_award_list) || !empty($person_award_list)) { ?>
    <div class="row">
        <p><b>Награды</b></p>

        <?php foreach ($movie_award_list as $movie_award_item) { ?>
        <p class="col-xs-offset-1">
            <a href="/award/details?id=<?= $movie_award_item->award->id ?>">
                <img
                    class="award-image"
                    src="<?= $movie_award_item->getImageUrl() ?>"
                    alt="award image" />
                <?= $movie_award_item->getInfo() ?>
            </a>
        </p>
        <?php }
        unset($movie_award_item);
        ?>

        <?php foreach ($person_award_list as $person_award_item) { ?>
        <p class="col-xs-offset-1">
            <a href="/award/details?id=<?= $person_award_item->award->id ?>">
                <img
                    class="award-image"
                    src="<?= $person_award_item->getImageUrl() ?>"
                    alt="award image" />
                <?= $person_award_item->getInfo() ?>
            </a>
        </p>
        <?php }
        unset($person_award_item);
        ?>
    </div>

    <hr />
    <?php } ?>

    <div class="row">
        <p><b>Жанры</b></p>

        <?php foreach ($movie_genre_list as $genre_item) { ?>
        <p class="col-xs-offset-1">
            <a href="/genre/details?id=<?= $genre_item->id ?>"><?= $genre_item->title ?></a>
        </p>
        <?php }
        unset($genre_item);
        ?>
    </div>

    <hr />

    <div class="row">
        <p><b>Страны</b></p>

        <?php foreach ($movie_country_list as $country_item) { ?>
        <p class="col-xs-offset-1">
            <span class="flag-icon flag-icon-<?= $country_item->code ?>" title="<?= $country_item->ru_name ?>"></span>
            <a href="/country/details?id=<?= $country_item->id ?>">
                <?= $country_item->ru_name ?>
            </a>
        </p>
        <?php }
        unset($country_item);
        ?>
    </div>

    <hr />

    <div class="row">
        <p><b>Режиссер</b></p>

        <?php foreach ($movie_director_list as $person_item) { ?>
        <p class="col-xs-offset-1">
            <a href="/person/details?id=<?= $person_item->id ?>">
                <img class="person-image" src="/images/person/<?= $person_item->image ?>" alt="person image" />
                <?= $person_item->getFullName() ?>
            </a>
            <span class="person-actor-movie-count" title="<?= $person_item->sex === 0 ? 'актриса' : 'актер' ?>">
                <?= $person_item->getViewedMovieActorCount(); ?> / <?= $person_item->getMovieActorCount(); ?>
            </span>
            <span class="person-director-movie-count" title="режиссер">
            <?= $person_item->getViewedMovieDirectorCount(); ?> / <?= $person_item->getMovieDirectorCount(); ?>
            </span>
        </p>
        <?php }
        unset($person_item);
        ?>
    </div>

    <hr />

    <div class="row">
        <p><b>Актеры</b></p>

        <?php foreach ($movie_actor_list as $person_item) { ?>
        <p class="col-xs-offset-1">
            <a href="/person/details?id=<?= $person_item->id ?>">
                <img class="person-image" src="/images/person/<?= $person_item->image ?>" alt="person image" />
                <?= $person_item->getFullName() ?>
            </a>
            <span class="person-actor-movie-count" title="<?= $person_item->sex === 0 ? 'актриса' : 'актер' ?>">
                <?= $person_item->getViewedMovieActorCount(); ?> / <?= $person_item->getMovieActorCount(); ?>
            </span>
            <span class="person-director-movie-count" title="режиссер">
            <?= $person_item->getViewedMovieDirectorCount(); ?> / <?= $person_item->getMovieDirectorCount(); ?>
            </span>
        </p>
        <?php }
        unset($person_item);
        ?>
    </div>

    <hr />

    <div class="row">
        <p><b>Просмотры</b></p>

        <table class="movie-list col-xs-offset-1">
            <tbody>
                <?php foreach ($model->views as $view_item) { ?>
                <tr>
                    <td>
                        <?= (new \DateTime($view_item->created_at))->format('d.m.Y') ?>
                    </td>
                    <td>
                        <span style="color: gold; font-size: 24px;"><?= $view_item->rating ?></span>
                    </td>
                    <td>
                        <?php if (!empty($view_item->audio)) { ?>
                        <span><?= $view_item->audio->name ?></span>
                        <?php } ?>
                    </td>
                    <td>
                        <?php if (!empty($view_item->theater)) { ?>
                        <span><?= $view_item->theater->name ?></span>
                        <?php } ?>
                    </td>
                    <td>
                        <?php if (!empty($view_item->drink)) { ?>
                        <span><?= $view_item->drink->name ?></span>
                        <?php } ?>
                    </td>
                    <td>
                        <?php if (!empty($view_item->specialShowing)) { ?>
                        <span><?= $view_item->specialShowing->name ?></span>
                        <?php } ?>
                    </td>
                </tr>
                <tr class="separator">
                    <td colspan="6"></td>
                </tr>
                <?php }
                unset($view_item);
                ?>
            </tbody>
        </table>
    </div>

    <hr />

    <div class="row">
        <p><b>Комментарии</b></p>

        <?php foreach ($comment_list as $comment_item) { ?>
        <p class="col-xs-offset-1">
            <i style="color: grey;">
                <?= $comment_item->created_at ?>
            </i>
        </p>
        <p class="col-xs-offset-1">
            <?= nl2br($comment_item->content) ?>
        </p>
        <?php if ($comment_item->images) { ?>
        <p class="col-xs-offset-1">
            <?php foreach ($comment_item->images as $image) { ?>
            <a class="nsbbox" title="Фото из комментариев" href="<?= Thumb::getBig($comment_item, $image) ?>" data-origin="<?= Thumb::getOriginUri($comment_item, $image) ?>">
                <img class="comment-image" src="<?= Thumb::getMin($comment_item, $image) ?>" alt="comment image" />
            </a>
            <?php }
            unset($image);
            ?>
        </p>
        <?php } ?>
        <p class="col-xs-offset-1">
            <?php if (!Yii::$app->user->isGuest) { ?>
            <a href="/comment/edit?id=<?= $comment_item->id ?>" class="btn btn-sm btn-primary">Редактировать</a>
            <?php } ?>
        </p>
        <hr class="col-xs-offset-1" />
        <?php }
        unset($comment_item);
        ?>
    </div>

    <?php if (!empty($model->views) && !Yii::$app->user->isGuest) { ?>
    <div class="row">
        <div>
            <a href="/comment/create?movie_id=<?= $model->id ?>" class="btn btn-sm btn-primary">Оставить комментарий</a>
        </div>
    </div>
    <?php } ?>

</div><!-- movie-details -->
