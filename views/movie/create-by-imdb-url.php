<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $imdbForm app\models\imdb\ImdbUrlForm */

$this->title = 'Создание фильма по ссылке IMDB';
?>
<div class="movie-create">
    <p>
        <a href="/movie/create">Обычное создание фильма</a>
    </p>

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($imdbForm, 'url')->textInput(['autofocus' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div><!-- movie-create -->
