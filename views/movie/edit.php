<?php

use app\models\image\ImageUploadForm;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\widgets\ImageUploader;

/** @var yii\web\View $this */
/* @var $model app\models\Movie */
/* @var $imageUploadForm ImageUploadForm */
/* @var $all_genre_list array */
/* @var $all_person_list array */
/* @var $all_country_list array */

$this->title = $model->title . ' | Редактирование фильма';
?>
<div class="movie-edit">

    <div class="row">
        <div class="movie-image-details-container">
            <a href="/movie/details?id=<?= $model->id ?>">
                <img class="movie-image-details" src="/images/movie/<?= $model->image ?>" alt="movie image" />
            </a>
        </div>
    </div>

    <div class="row">
        <?php $ImageActiveForm = ActiveForm::begin([
            'action' => '/movie/update-image?id=' . $model->id,
            'options' => ['enctype' => 'multipart/form-data']
        ]); ?>

            <?= ImageUploader::widget([
                'label' => 'Постер',
                'ImageUploadForm' => $imageUploadForm,
                'ActiveForm' => $ImageActiveForm
            ]); ?>

            <div class="form-group">
                <?= Html::submitButton('Обновить картинку', ['class' => 'btn btn-primary']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

    <hr />

    <div class="row">
        <?php $movie_form = ActiveForm::begin(); ?>

            <?= $movie_form->field($model, 'title') ?>
            <?= $movie_form->field($model, 'year') ?>
            <?= $movie_form->field($model, 'original_title') ?>
            <?= $movie_form->field($model, 'is_favorite')->checkbox() ?>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

    <hr />

    <div class="row">
        <div>
            <a id="genre" class="anchor"></a>
        </div>
        <div>
            <p><b>Жанры</b></p>

            <?php $genre_form = ActiveForm::begin([
                'id' => 'add-genre-to-movie',
                'action' => '/movie/add-genre?id=' . $model->id,
                'layout' => 'horizontal',
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-2 control-label'],
                ],
            ]); ?>

            <div class="form-group">
                <?= Html::dropDownList('genre_id', null, $all_genre_list, [
                    'onchange' => 'this.form.submit();',
                    'prompt' => 'Выбрать жанр'
                ]) ?>
            </div>

            <?php foreach ($model->movieGenres as $movie_genre_item) { ?>
            <p>
                <?= $movie_genre_item->genre->title ?>
                <a href="/movie/delete-movie-genre?movie_id=<?= $model->id ?>&genre_id=<?= $movie_genre_item->genre->id ?>" class="btn btn-sm btn-danger" onclick="if (confirm('Точно удалить?')) { return true; } else { return false; }">удалить</a>
            </p>
            <?php }
            unset($movie_genre_item);
            ?>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

    <hr />

    <div class="row">
        <div>
            <a id="country" class="anchor"></a>
        </div>
        <div>
            <p><b>Страны</b></p>

            <?php $country_form = ActiveForm::begin([
                'id' => 'add-country-to-movie',
                'action' => '/movie/add-country?id=' . $model->id,
                'layout' => 'horizontal',
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-2 control-label'],
                ],
            ]); ?>

            <div class="form-group">
                <?= Html::dropDownList('country_id', null, $all_country_list, [
                    'onchange' => 'this.form.submit();',
                    'prompt' => 'Выбрать страну'
                ]) ?>
            </div>

            <?php foreach ($model->movieCountries as $movie_country_item) { ?>
            <p>
                <?= $movie_country_item->country->ru_name ?>
                <a href="/movie/delete-movie-country?movie_id=<?= $model->id ?>&country_id=<?= $movie_country_item->country->id ?>" class="btn btn-sm btn-danger" onclick="if (confirm('Точно удалить?')) { return true; } else { return false; }">удалить</a>
            </p>
            <?php }
            unset($movie_country_item);
            ?>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

    <hr />

    <div class="row">
        <div>
            <a id="director" class="anchor"></a>
        </div>
        <div>
            <p><b>Режиссёры</b></p>

            <?php $director_form = ActiveForm::begin([
                'id' => 'add-director-to-movie',
                'action' => '/movie/add-director?id=' . $model->id,
                'layout' => 'horizontal',
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-2 control-label'],
                ],
            ]); ?>

            <div class="form-group">
                <?= Html::dropDownList('person_id', null, $all_person_list, [
                    'onchange' => 'this.form.submit();',
                    'prompt' => 'Выбрать персону'
                ]) ?>
            </div>

            <?php foreach ($model->movieDirectors as $movie_director_item) { ?>
            <p>
                <?= $movie_director_item->person->getFullName() ?>
                <a href="/movie/delete-movie-director?movie_id=<?= $model->id ?>&person_id=<?= $movie_director_item->person->id ?>" class="btn btn-sm btn-danger" onclick="if (confirm('Точно удалить?')) { return true; } else { return false; }">удалить</a>
            </p>
            <?php }
            unset($movie_director_item);
            ?>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

    <hr />

    <div class="row">
        <div>
            <a id="actor" class="anchor"></a>
        </div>
        <div>
            <p><b>Актёры</b></p>

            <?php $actor_form = ActiveForm::begin([
                'id' => 'add-actor-to-movie',
                'action' => '/movie/add-actor?id=' . $model->id,
                'layout' => 'horizontal',
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-2 control-label'],
                ],
            ]); ?>

            <div class="form-group">
                <?= Html::dropDownList('person_id', null, $all_person_list, [
                    'onchange' => 'this.form.submit();',
                    'prompt' => 'Выбрать персону'
                ]) ?>
            </div>

            <?php foreach ($model->movieActors as $movie_actor_item) { ?>
            <p>
                <?= $movie_actor_item->person->getFullName() ?>
                <a href="/movie/delete-movie-actor?movie_id=<?= $model->id ?>&person_id=<?= $movie_actor_item->person->id ?>" class="btn btn-sm btn-danger" onclick="if (confirm('Точно удалить?')) { return true; } else { return false; }">удалить</a>
            </p>
            <?php }
            unset($movie_actor_item);
            ?>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

    <hr />

    <div class="row">
        <div>
            <a id="view" class="anchor"></a>
        </div>
        <div>
            <p><b>Просмотры</b></p>
            <table class="movie-list">
                <tbody>
                    <?php foreach ($model->views as $view_item) { ?>
                    <tr>
                        <td>
                            <?= (new \DateTime($view_item->created_at))->format('d.m.Y') ?>
                        </td>
                        <td>
                            <span style="color: gold; font-size: 24px;"><?= $view_item->rating ?></span>
                        </td>
                        <td>
                            <?php if (!empty($view_item->audio)) { ?>
                            <span><?= $view_item->audio->name ?></span>
                            <?php } ?>
                        </td>
                        <td>
                            <?php if (!empty($view_item->theater)) { ?>
                            <span><?= $view_item->theater->name ?></span>
                            <?php } ?>
                        </td>
                        <td>
                            <?php if (!empty($view_item->drink)) { ?>
                            <span><?= $view_item->drink->name ?></span>
                            <?php } ?>
                        </td>
                        <td>
                            <a href="/view/delete?id=<?= $view_item->id ?>" class="btn btn-sm btn-danger" onclick="if (confirm('Точно удалить?')) { return true; } else { return false; }">Удалить просмотр</a>
                        </td>
                    </tr>
                    <tr class="separator">
                        <td colspan="6"></td>
                    </tr>
                    <?php }
                    unset($view_item);
                    ?>
                </tbody>
            </table>
        </div>
    </div>

    <hr />

    <div class="row">
        <?php if (!empty($model->queue)) { ?>
        <a href="/queue/delete?movie_id=<?= $model->id ?>" class="btn btn-sm btn-danger" onclick="if (confirm('Точно удалить?')) { return true; } else { return false; }">Удалить из очереди</a>
        <?php } ?>

        <a href="/movie/delete?id=<?= $model->id ?>" class="btn btn-sm btn-danger" onclick="if (confirm('Точно удалить?')) { return true; } else { return false; }">Удалить фильм</a>
    </div>

</div><!-- movie-edit -->
