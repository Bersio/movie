<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\widgets\ImageUploader;

/* @var $this yii\web\View */
/* @var $model app\models\Person */
/* @var $form ActiveForm */

$this->title = 'Создание персоны';
?>
<div class="person-create">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

        <?= $form->field($model, 'first_name')->textInput(['autofocus' => true]) ?>
        <?= $form->field($model, 'last_name') ?>
        <?= $form->field($model, 'sex')->dropDownList($sex_list, [
            'prompt' => 'Выбрать пол'
        ]) ?>

        <?= ImageUploader::widget([
            'label' => 'Фото',
            'ImageUploadForm' => $ImageUploadForm,
            'ActiveForm' => $form
        ]); ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- person-create -->
