<?php

use app\models\Person;
use app\models\query\PersonQuery;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $person_query PersonQuery */
/* @var $person_total_count int */
/* @var $type string */
/* @var $type_list array */
/* @var $award_id int */
/* @var $award_list array */
/* @var $viewed_movies int */
/* @var $sex string */
/* @var $sex_list array */
/* @var $sort string */
/* @var $sort_list array */

$this->title = !empty($title) ? $title : Yii::t('app', 'Persons');
?>
<div class="person-index">

    <div class="jumbotron">
        <h1><?= $this->title ?></h1>
        <h2>Всего персон: <?= $person_total_count ?></h2>
    </div>

    <div class="body-content">

        <?php ActiveForm::begin([
            'id' => 'queue-filters',
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-2 control-label'],
            ],
        ]); ?>

        <div class="row filter-row">

            <?php if (!Yii::$app->user->isGuest) { ?>
            <div class="col-lg-4 filter-item">
                <a href="/person/create" class="btn btn-success">Создать новую персону</a>
            </div>
            <?php } ?>

            <div class="col-lg-4 filter-item">
            <?= Html::dropDownList('sort', $sort, $sort_list, [
                'onchange' => 'this.form.submit()',
                'prompt' => Yii::t('app', 'Default sorting'),
                'class' => 'form-control'
            ]); ?>
            </div>

            <div class="col-lg-4 filter-item">
                <label class="col-lg-11 control-label" for="viewed_movies" ondblclick="return false;" onmousedown="return false;">Только те, фильмы с которыми я смотрел</label>
                <div class="col-lg-1">
                <?= Html::checkbox('viewed_movies', $viewed_movies, [
                    'id' => 'viewed_movies',
                    'onchange' => 'this.form.submit()',
                ]); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 filter-item">
            <?= Html::dropDownList('type', $type, $type_list, [
                'onchange' => 'this.form.submit()',
                'prompt' => 'Выбрать тип',
                'class' => 'form-control'
            ]); ?>
            </div>

            <div class="col-lg-4 filter-item">
            <?= Html::dropDownList('award_id', $award_id, $award_list, [
                'onchange' => 'this.form.submit()',
                'prompt' => [
                    'text' => 'Выбрать награду',
                    'options' => [
                        'value' => '0'
                    ],
                ],
                'class' => 'form-control'
            ]); ?>
            </div>

            <div class="col-lg-4 filter-item">
            <?= Html::dropDownList('sex', $sex, $sex_list, [
                'onchange' => 'this.form.submit()',
                'prompt' => 'Выбрать пол',
                'class' => 'form-control'
            ]); ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

        <hr />

        <div class="row">
            <table class="person-list">
                <tbody>
                    <?php /** @var Person $person_item */
                    foreach ($person_query->each() as $person_item) { ?>
                    <tr>
                        <td>
                            <a href="/person/details?id=<?= $person_item->id ?>">
                                <img class="person-image" src="/images/person/<?= $person_item->image ?>" alt="person image" />
                            </a>
                        </td>
                        <td>
                            <h2>
                                <a href="/person/details?id=<?= $person_item->id ?>">
                                    <?= $person_item->getFullName() ?>
                                </a>
                            </h2>
                        </td>
                        <td title="<?= $person_item->sex === 0 ? 'актриса' : 'актер' ?>">
                            <?= $person_item->getViewedMovieActorCount(); ?> / <?= $person_item->getMovieActorCount(); ?>
                        </td>
                        <td></td>
                        <td title="режиссер">
                            <?= $person_item->getViewedMovieDirectorCount(); ?> / <?= $person_item->getMovieDirectorCount(); ?>
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr class="separator">
                        <td colspan="6"></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>

    </div>
</div>
