<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\widgets\ImageUploader;

/* @var $this yii\web\View */
/* @var $model app\models\Movie */
/* @var $form ActiveForm */

$this->title = $model->getFullName() . ' | Редактирование персоны';
?>
<div class="person-edit">

    <div class="row">
        <div class="person-image-details-container">
            <a href="/person/details?id=<?= $model->id ?>">
                <img class="person-image-details" src="/images/person/<?= $model->image ?>" alt="person image" />
            </a>
        </div>
    </div>

    <div class="row">
        <?php $ImageActiveForm = ActiveForm::begin([
            'action' => '/person/update-image?id=' . $model->id,
            'options' => ['enctype' => 'multipart/form-data']
        ]); ?>

            <?= ImageUploader::widget([
                'label' => 'Фото',
                'ImageUploadForm' => $ImageUploadForm,
                'ActiveForm' => $ImageActiveForm
            ]); ?>

            <div class="form-group">
                <?= Html::submitButton('Обновить картинку', ['class' => 'btn btn-primary']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

    <hr />

    <div class="row">
        <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'first_name') ?>
            <?= $form->field($model, 'last_name') ?>
            <?= $form->field($model, 'sex')->dropDownList($sex_list, [
                'prompt' => 'Выбрать пол'
            ]) ?>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

    <hr />

    <div class="row">
        <a href="/person/delete?id=<?= $model->id ?>" class="btn btn-sm btn-danger" onclick="if (confirm('Точно удалить?')) { return true; } else { return false; }">Удалить персону</a>
    </div>

</div><!-- person-edit -->
