<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Movie */
/* @var $form ActiveForm */

$this->title = 'Выбрать пол персоны';
?>
<div class="person-set-sex">
    <div class="row">
        <div class="person-image-details-container">
            <a href="/person/details?id=<?= $model->id ?>">
                <img class="person-image-details" src="/images/person/<?= $model->image ?>" alt="person image" />
            </a>
            <h2><?= $model->first_name ?> <?= $model->last_name ?></h2>
        </div>
    </div>

    <hr />

    <div class="row">
        <div class="person-image-details-container">
        <?php $form = ActiveForm::begin(); ?>

            <?= Html::submitInput('Мужчина', ['class' => 'btn btn-success', 'name' => 'male']) ?>
            <?= Html::submitInput('Женщина', ['class' => 'btn btn-success', 'name' => 'female']) ?>

        <?php ActiveForm::end(); ?>
        </div>
    </div>

</div><!-- person-set-sex -->
