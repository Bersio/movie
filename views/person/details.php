<?php

use yii\helpers\Html;
use app\models\image\Thumb;

/* @var $this yii\web\View */
/* @var $model app\models\Person */

$this->title = $model->getFullName();

$this->registerCssFile('/css/image-viewer.css', ['depends' => 'app\assets\AppAsset']);
$this->registerJsFile('/js/image-viewer.min.js', ['depends' => 'app\assets\AppAsset']);
?>
<div class="person-details">

    <div class="row">
        <div class="person-image-details-container">
            <img class="person-image-details" src="/images/person/<?= $model->image ?>" alt="person image" />
        </div>
    </div>

    <div class="row">
        <h1><?= $this->title ?></h1>
    </div>

    <?php if (!Yii::$app->user->isGuest) { ?>
    <div class="row">
        <a href="/person/edit?id=<?= $model->id ?>" class="btn btn-sm btn-primary">Редактировать</a>
        <a href="<?= $model->getUrlForGoogleSearch() ?>" class="btn btn-sm btn-primary" target="_blank">Искать в google</a>
    </div>
    <?php } ?>

    <hr />

    <?php if (!empty($person_award_query->count())) { ?>
    <div class="row">
        <p><b>Награды</b></p>

        <?php foreach ($person_award_query->each() as $person_award_item) { ?>
        <p class="col-xs-offset-1">
            <a href="/award/result?id=<?= $person_award_item->award->id ?>">
                <img class="award-image" src="<?= $person_award_item->getImageUrl() ?>" alt="award image" />
                <?= $person_award_item->getInfo() ?>
            </a>
        </p>
        <?php }
        unset($person_award_item);
        ?>
    </div>

    <hr />
    <?php } ?>

    <?php if (!empty($movie_actor_query->count())) { ?>
    <div class="row">
        <p><b><?= $model->sex === 0 ? 'Актриса' : 'Актер' ?></b></p>
        <table class="movie-list">
            <tbody>
                <?php foreach ($movie_actor_query->each() as $movie_item) { ?>
                <tr>
                    <td>
                        <a href="/movie/details?id=<?= $movie_item->id ?>">
                            <img class="movie-image" src="/images/movie/<?= $movie_item->image ?>" alt="movie image" />
                        </a>
                    </td>
                    <td>
                        <h2>
                            <a href="/movie/details?id=<?= $movie_item->id ?>">
                                <?= $movie_item->title ?>
                            </a>
                        </h2>
                        <h4><?= $movie_item->original_title ?></h4>
                        <p><?= $movie_item->year ?></p>
                    </td>
                    <td>
                        <?php if (!empty($movie_item->average_rating)) { ?>
                        <span style="color: gold; font-size: 24px;"><?= round($movie_item->average_rating, 2) ?></span>
                        <?php } else { ?>
                        <span style="color: grey; font-size: 24px;"></span>
                        <?php } ?>
                    </td>
                    <td>
                        <?php if (!empty($movie_item->views)) { ?>
                        <span style="color: grey;"><?= (new \DateTime($movie_item->views[0]->created_at))->format('d.m.Y') ?></span>
                        <?php } else { ?>
                        <span style="color: grey;"></span>
                        <?php } ?>
                    </td>
                    <td>

                    </td>
                </tr>
                <tr class="separator">
                    <td colspan="5"></td>
                </tr>
                <?php }
                unset($movie_item);
                ?>
            </tbody>
        </table>
    </div>

    <hr />
    <?php } ?>

    <?php if (!empty($movie_director_query->count())) { ?>
    <div class="row">
        <p><b>Режиссер</b></p>
        <table class="movie-list">
            <tbody>
                <?php foreach ($movie_director_query->each() as $movie_item) { ?>
                <tr>
                    <td>
                        <a href="/movie/details?id=<?= $movie_item->id ?>">
                            <img class="movie-image" src="/images/movie/<?= $movie_item->image ?>" alt="movie image" />
                        </a>
                    </td>
                    <td>
                        <h2>
                            <a href="/movie/details?id=<?= $movie_item->id ?>">
                                <?= $movie_item->title ?>
                            </a>
                        </h2>
                        <h4><?= $movie_item->original_title ?></h4>
                        <p><?= $movie_item->year ?></p>
                    </td>
                    <td>
                        <?php if (!empty($movie_item->average_rating)) { ?>
                        <span style="color: gold; font-size: 24px;"><?= round($movie_item->average_rating, 2) ?></span>
                        <?php } else { ?>
                        <span style="color: grey; font-size: 24px;"></span>
                        <?php } ?>
                    </td>
                    <td>
                        <?php if (!empty($movie_item->views)) { ?>
                        <span style="color: grey;"><?= (new \DateTime($movie_item->views[0]->created_at))->format('d.m.Y') ?></span>
                        <?php } else { ?>
                        <span style="color: grey;"></span>
                        <?php } ?>
                    </td>
                    <td>

                    </td>
                </tr>
                <tr class="separator">
                    <td colspan="5"></td>
                </tr>
                <?php } 
                unset($movie_item);
                ?>
            </tbody>
        </table>
    </div>

    <hr />
    <?php } ?>

    <div class="row">
        <p><b>Комментарии</b></p>

        <?php foreach ($comment_query->each() as $comment_item) { ?>
        <p class="col-xs-offset-1">
            <i style="color: grey;">
                <?= $comment_item->created_at ?>
            </i>
        </p>
        <p class="col-xs-offset-1">
            <?= nl2br($comment_item->content) ?>
        </p>
        <?php if ($comment_item->images) { ?>
        <p class="col-xs-offset-1">
            <?php foreach ($comment_item->images as $image) { ?>
            <a class="nsbbox" title="Фото из комментариев" href="<?= Thumb::getBig($comment_item, $image) ?>" data-origin="<?= Thumb::getOriginUri($comment_item, $image) ?>">
                <img class="comment-image" src="<?= Thumb::getMin($comment_item, $image) ?>" alt="comment image" />
            </a>
            <?php }
            unset($image);
            ?>
        </p>
        <?php } ?>
        <p class="col-xs-offset-1">
            <?php if (!Yii::$app->user->isGuest) { ?>
            <a href="/comment/edit?id=<?= $comment_item->id ?>" class="btn btn-sm btn-primary">Редактировать</a>
            <?php } ?>
        </p>
        <hr class="col-xs-offset-1" />
        <?php }
        unset($comment_item);
        ?>
    </div>

    <?php if (!Yii::$app->user->isGuest) { ?>
    <div class="row">
        <div>
            <a href="/comment/create?person_id=<?= $model->id ?>" class="btn btn-sm btn-primary">Оставить комментарий</a>
        </div>
    </div>
    <?php } ?>

</div><!-- person-details -->
