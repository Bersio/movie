<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model Audio */
/* @var $movie_query \yii\db\ActiveQuery */
/* @var $is_favorite int */
/* @var $sort string */
/* @var $sort_list array */

$this->title = $model->ru_name;
?>
<div class="country-details">

    <div class="jumbotron">
        <h1><?= $model->ru_name ?></h1>
        <h2>Всего фильмов: <?= $model->total_movies_count ?>, из них просмотрено: <?= $model->viewed_movies_count ?></h2>
    </div>

    <div class="row">
        <?php ActiveForm::begin([
            'id' => 'country-filters',
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-2 control-label'],
            ],
        ]); ?>

        <div class="form-group">
            <div class="col-lg-4">
            <?= Html::dropDownList('sort', $sort, $sort_list, [
                'onchange' => 'this.form.submit()',
                'prompt' => Yii::t('app', 'Default sorting'),
                'class' => 'form-control'
            ]); ?>
            </div>
        </div>

        <div class="col-lg-4 filter-item">
            <?= Html::dropDownList('is_favorite', $is_favorite, ['1' => 'Избранное'], [
                'onchange' => 'this.form.submit()',
                'prompt' => [
                    'text' => 'Все фильмы',
                    'options' => [
                        'value' => '0'
                    ],
                ],
                'class' => 'form-control'
            ]); ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

    <hr />

    <div class="row">
        <table class="movie-list">
            <tbody>
                <?php foreach ($movie_query->each() as $movie_item) { ?>
                <tr>
                    <td>
                        <a href="/movie/details?id=<?= $movie_item->id ?>">
                            <img class="movie-image" src="/images/movie/<?= $movie_item->image ?>" alt="movie image" />
                        </a>
                    </td>
                    <td>
                        <h2>
                            <a href="/movie/details?id=<?= $movie_item->id ?>">
                                <?= $movie_item->title ?>
                            </a>
                        </h2>
                        <h4><?= $movie_item->original_title ?></h4>
                        <p><?= $movie_item->year ?></p>
                    </td>
                    <td>
                        <?php if (!empty($movie_item->average_rating)) { ?>
                        <span style="color: gold; font-size: 24px;"><?= round($movie_item->average_rating, 2) ?></span>
                        <?php } else { ?>
                        <span style="color: grey; font-size: 24px;"></span>
                        <?php } ?>
                    </td>
                    <td>
                        <?php if (!empty($movie_item->views)) { ?>
                        <span style="color: grey;"><?= (new \DateTime($movie_item->views[0]->created_at))->format('d.m.Y') ?></span>
                        <?php } else { ?>
                        <span style="color: grey;"></span>
                        <?php } ?>
                    </td>
                    <td>

                    </td>
                </tr>
                <tr class="separator">
                    <td colspan="5"></td>
                </tr>
                <?php }
                unset($movie_item);
                ?>
            </tbody>
        </table>
    </div>

</div><!-- country-details -->
