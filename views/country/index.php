<?php

use app\models\query\CountryQuery;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/** @var yii\web\View $this */
/* @var $country_query CountryQuery */
/* @var $country_total_count int */
/* @var $sort string */
/* @var $sort_list array */

$this->title = Yii::t('app', 'Countries');
?>
<div class="country-index">

    <div class="jumbotron">
        <h1><?= $this->title ?></h1>
        <h2>Всего стран: <?= $country_total_count ?></h2>
    </div>

    <div class="body-content">

        <div class="row">
            <?php ActiveForm::begin([
                'id' => 'country-filters',
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-2 control-label'],
                ],
            ]); ?>

            <div class="form-group">
                <div class="col-lg-4">
                <?= Html::dropDownList('sort', $sort, $sort_list, [
                    'onchange' => 'this.form.submit()',
                    'prompt' => Yii::t('app', 'Default sorting'),
                    'class' => 'form-control'
                ]); ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>

        <hr />

        <div class="row">
            <table>
                <tbody>
                    <?php foreach ($country_query->each() as $country_item) { ?>
                    <tr>
                        <td>
                            <h2>
                                <span class="flag-icon flag-icon-<?= $country_item->code ?>" title="<?= $country_item->ru_name ?>"></span>
                                <a href="/country/details?id=<?= $country_item->id ?>">
                                    <?= $country_item->ru_name ?>
                                </a>
                            </h2>
                        </td>
                        <td><?= $country_item->viewed_movies_count ?> / <?= $country_item->total_movies_count ?></td>
                        <td>

                        </td>
                    </tr>
                    <?php }
                    unset($country_item);
                    ?>
                </tbody>
            </table>
        </div>

    </div>

</div>
