<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AwardNomination */

$this->title = 'Создание номинации';
?>
<div class="award-nomination-create">

    <div class="jumbotron">
        <h1><?= $this->title ?></h1>
        <h2><?= $model->award->name ?></h2>
    </div>

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name') ?>
    
        <?= $form->field($model, 'type')->dropDownList($type_list, [
            'prompt' => 'Выбрать тип'
        ]) ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- award-nomination-create -->
