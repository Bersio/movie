<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Award;
use app\models\Movie;
use app\models\MovieAward;
use app\models\Person;
use app\models\PersonAward;

/* @var $this yii\web\View */
/* @var $model app\models\AwardNomination */

$this->title = $model->name;
?>
<div class="award-nomination-details">
    <div class="jumbotron">
        <h1><?= $model->name ?></h1>
    </div>

    <div class="row">
        <a href="/award-nomination/edit?id=<?= $model->id ?>" class="btn btn-sm btn-primary">Редактировать</a>
    </div>

    <div class="row">
        <?php ActiveForm::begin([
            'id' => 'award-result-filters',
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-2 control-label'],
            ],
        ]); ?>

        <div class="form-group">
            <div class="col-lg-4">
            <?= Html::dropDownList('year', $year, $years_list, [
                'onchange' => 'this.form.submit()',
                'prompt' => [
                    'text' => 'Выбрать год',
                    'options' => [
                        'value' => '0'
                    ],
                ],
                'class' => 'form-control'
            ]); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-2 control-label" for="only_winner" ondblclick="return false;" onmousedown="return false;">Только победители</label>
            <div class="col-lg-1">
            <?= Html::checkbox('only_winner', $only_winner, [
                'id' => 'only_winner',
                'onchange' => 'this.form.submit()',
            ]); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-lg-3 control-label" for="only_without_view" ondblclick="return false;" onmousedown="return false;">Только непросмотренные</label>
            <div class="col-lg-1">
            <?= Html::checkbox('only_without_view', $only_without_view, [
                'id' => 'only_without_view',
                'onchange' => 'this.form.submit()',
            ]); ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>

    <div class="row">

    <?php foreach ($selected_years_list as $year_item) { ?>
        <h2><?= $year_item ?></h2>

        <?php
            $movie_winner_list = MovieAward::find()
                    ->with(['movie.views'])
                    ->where(['year' => $year_item])
                    ->andWhere(['award_nomination_id' => $model->id])
                    ->andWhere(['type' => Award::TYPE_WINNER])
                    ->orderBy(['id' => SORT_ASC])
                    ->all();

            if (!$only_winner) {
                $movie_nominant_list = MovieAward::find()
                    ->with(['movie.views'])
                    ->where(['year' => $year_item])
                    ->andWhere(['award_nomination_id' => $model->id])
                    ->andWhere(['type' => Award::TYPE_NOMINATION])
                    ->orderBy(['id' => SORT_ASC])
                    ->all();
            } else {
                $movie_nominant_list = [];
            }

            $person_winner_list = PersonAward::find()
                    ->with(['movie.views', 'person'])
                    ->where(['year' => $year_item])
                    ->andWhere(['award_nomination_id' => $model->id])
                    ->andWhere(['type' => Award::TYPE_WINNER])
                    ->orderBy(['id' => SORT_ASC])
                    ->all();

            if (!$only_winner) {
                $person_nominant_list = PersonAward::find()
                    ->with(['movie.views', 'person'])
                    ->where(['year' => $year_item])
                    ->andWhere(['award_nomination_id' => $model->id])
                    ->andWhere(['type' => Award::TYPE_NOMINATION])
                    ->orderBy(['id' => SORT_ASC])
                    ->all();
            } else {
                $person_nominant_list = [];
            }
        ?>

        <div class="row">
            <table class="movie-list">
                <tbody>
                    <?php foreach ($movie_winner_list as $winner_item) {
                        if ($only_without_view && !empty($winner_item->movie->views)) {
                            continue;
                        }
                    ?>
                    <tr style="border: 5px solid gold; background-color: yellow;">
                        <td>
                            <a href="/movie/details?id=<?= $winner_item->movie->id ?>">
                                <img class="movie-image" src="/images/movie/<?= $winner_item->movie->image ?>" alt="movie image" />
                            </a>
                        </td>
                        <td>
                            <h2>
                                <a href="/movie/details?id=<?= $winner_item->movie->id ?>">
                                    <?= $winner_item->movie->title ?>
                                </a>
                            </h2>
                            <h4><?= $winner_item->movie->original_title ?></h4>
                            <p><?= $winner_item->movie->year ?></p>
                        </td>
                        <td>
                            <?php if (!empty($winner_item->movie->average_rating)) { ?>
                            <span style="color: gold; font-size: 36px;"><?= round($winner_item->movie->average_rating, 2) ?></span>
                            <?php } else { ?>
                            <span style="color: grey; font-size: 36px;"></span>
                            <?php } ?>
                        </td>
                        <td>
                            <?php if (!empty($winner_item->movie->views)) { ?>
                            <span style="color: grey;"><?= (new \DateTime($winner_item->movie->views[0]->created_at))->format('d.m.Y') ?></span>
                            <?php } else { ?>
                            <span style="color: grey;"></span>
                            <?php } ?>
                        </td>
                        <td>

                        </td>
                    </tr>
                    <?php } 
                    unset($winner_item, $movie_winner_list); ?>

                    <?php foreach ($movie_nominant_list as $nominant_item) {
                        if ($only_without_view && !empty($nominant_item->movie->views)) {
                            continue;
                        }
                    ?>
                    <tr style="border: 5px solid aqua; background-color: aquamarine;">
                        <td>
                            <a href="/movie/details?id=<?= $nominant_item->movie->id ?>">
                                <img class="movie-image" src="/images/movie/<?= $nominant_item->movie->image ?>" alt="movie image" />
                            </a>
                        </td>
                        <td>
                            <h2>
                                <a href="/movie/details?id=<?= $nominant_item->movie->id ?>">
                                    <?= $nominant_item->movie->title ?>
                                </a>
                            </h2>
                            <h4><?= $nominant_item->movie->original_title ?></h4>
                            <p><?= $nominant_item->movie->year ?></p>
                        </td>
                        <td>
                            <?php if (!empty($nominant_item->movie->average_rating)) { ?>
                            <span style="color: gold; font-size: 36px;"><?= round($nominant_item->movie->average_rating, 2) ?></span>
                            <?php } else { ?>
                            <span style="color: grey; font-size: 36px;"></span>
                            <?php } ?>
                        </td>
                        <td>
                            <?php if (!empty($nominant_item->movie->views)) { ?>
                            <span style="color: grey;"><?= (new \DateTime($nominant_item->movie->views[0]->created_at))->format('d.m.Y') ?></span>
                            <?php } else { ?>
                            <span style="color: grey;"></span>
                            <?php } ?>
                        </td>
                        <td>

                        </td>
                    </tr>
                    <?php } 
                    unset($nominant_item, $movie_nominant_list); ?>

                    <?php foreach ($person_winner_list as $winner_item) {
                        if ($only_without_view && !empty($winner_item->movie->views)) {
                            continue;
                        }
                    ?>
                    <tr style="border: 5px solid gold; background-color: yellow;">
                        <td>
                            <a href="/person/details?id=<?= $winner_item->person->id ?>">
                                <img class="person-image" src="/images/person/<?= $winner_item->person->image ?>" alt="person image" />
                            </a>
                        </td>
                        <td>
                            <h2>
                                <a href="/person/details?id=<?= $winner_item->person->id ?>">
                                    <?= $winner_item->person->getFullName() ?>
                                </a>
                            </h2>
                        </td>
                        <td>
                            <h2>
                                (<a href="/movie/details?id=<?= $winner_item->movie->id ?>">
                                    <img class="movie-image-small" src="/images/movie/<?= $winner_item->movie->image ?>" alt="movie image" />
                                    <?= $winner_item->movie->title ?>
                                </a>
                                <?php if ($winner_item->movie->average_rating) { ?>
                                 - <span style="color: gold; font-size: 36px;"><?= round($winner_item->movie->average_rating, 2) ?></span>
                                <?php } ?>)
                            </h2>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <?php } 
                    unset($winner_item, $person_winner_list); ?>

                    <?php foreach ($person_nominant_list as $nominant_item) {
                        if ($only_without_view && !empty($nominant_item->movie->views)) {
                            continue;
                        }
                    ?>
                    <tr style="border: 5px solid aqua; background-color: aquamarine;">
                        <td>
                            <a href="/person/details?id=<?= $nominant_item->person->id ?>">
                                <img class="person-image" src="/images/person/<?= $nominant_item->person->image ?>" alt="person image" />
                            </a>
                        </td>
                        <td>
                            <h2>
                                <a href="/person/details?id=<?= $nominant_item->person->id ?>">
                                    <?= $nominant_item->person->getFullName() ?>
                                </a>
                            </h2>
                        </td>
                        <td>
                            <h2>
                                (<a href="/movie/details?id=<?= $nominant_item->movie->id ?>">
                                    <img class="movie-image-small" src="/images/movie/<?= $nominant_item->movie->image ?>" alt="movie image" />
                                    <?= $nominant_item->movie->title ?>
                                </a>
                                <?php if ($nominant_item->movie->average_rating) { ?>
                                 - <span style="color: gold; font-size: 36px;"><?= round($nominant_item->movie->average_rating, 2) ?></span>
                                <?php } ?>)
                            </h2>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <?php }
                    unset($nominant_item, $person_nominant_list); ?>
                </tbody>
            </table>
        </div>

        <hr />
    <?php }
    unset($year_item);
    ?>

    </div>
</div><!-- award-nomination-details -->
