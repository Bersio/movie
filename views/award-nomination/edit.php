<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Award;
use app\models\Movie;
use app\models\MovieAward;
use app\models\Person;
use app\models\PersonAward;

/* @var $this yii\web\View */

$this->title = $model->name . ' | Редактирование номинации';
?>
<div class="award-nomination-edit">

    <div class="jumbotron">
        <h1><?= $this->title ?></h1>
        <h2><?= $model->award->name ?></h2>
    </div>

    <div class="row">
        <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'name') ?>

            <?= $form->field($model, 'type')->dropDownList($type_list, [
                'prompt' => 'Выбрать тип'
            ]) ?>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>

    <div class="row">
        <a href="/award-nomination/delete?id=<?= $model->id ?>" class="btn btn-sm btn-danger" onclick="if (confirm('Точно удалить?')) { return true; } else { return false; }">Удалить номинацию</a>
    </div>

    <div class="row">

    <?php foreach ($years_list as $year_item) { ?>
        <h2><?= $year_item ?></h2>

        <?php
            $movie_winner_list = MovieAward::find()
                    ->where(['year' => $year_item])
                    ->andWhere(['award_nomination_id' => $model->id])
                    ->andWhere(['type' => Award::TYPE_WINNER])
                    ->orderBy(['id' => SORT_ASC])
                    ->all();

            $movie_nominant_list = MovieAward::find()
                    ->where(['year' => $year_item])
                    ->andWhere(['award_nomination_id' => $model->id])
                    ->andWhere(['type' => Award::TYPE_NOMINATION])
                    ->orderBy(['id' => SORT_ASC])
                    ->all();

            $person_winner_list = PersonAward::find()
                    ->where(['year' => $year_item])
                    ->andWhere(['award_nomination_id' => $model->id])
                    ->andWhere(['type' => Award::TYPE_WINNER])
                    ->orderBy(['id' => SORT_ASC])
                    ->all();

            $person_nominant_list = PersonAward::find()
                    ->where(['year' => $year_item])
                    ->andWhere(['award_nomination_id' => $model->id])
                    ->andWhere(['type' => Award::TYPE_NOMINATION])
                    ->orderBy(['id' => SORT_ASC])
                    ->all();
        ?>

        <div class="row">
            <table>
                <tbody>
                    <?php foreach ($movie_winner_list as $winner_item) {
                        $movie = Movie::findOne($winner_item->movie_id);
                    ?>
                    <tr style="background-color: gold;">
                        <td></td>
                        <td>
                            <h2>
                                <a href="/movie/details?id=<?= $movie->id ?>">
                                    <?= $movie->title ?>
                                </a>
                            </h2>
                        </td>
                        <td></td>
                        <td></td>
                        <td>
                            <a href="/award-nomination/delete-movie-award?movie_award_id=<?= $winner_item->id ?>" class="btn btn-sm btn-danger" onclick="if (confirm('Точно удалить?')) { return true; } else { return false; }">удалить</a>
                        </td>
                    </tr>
                    <?php } 
                    unset($winner_item, $movie_winner_list, $movie); ?>

                    <?php foreach ($movie_nominant_list as $nominant_item) {
                        $movie = Movie::findOne($nominant_item->movie_id);
                    ?>
                    <tr style="background-color: aqua;">
                        <td></td>
                        <td>
                            <h2>
                                <a href="/movie/details?id=<?= $movie->id ?>">
                                    <?= $movie->title ?>
                                </a>
                            </h2>
                        </td>
                        <td></td>
                        <td></td>
                        <td>
                            <a href="/award-nomination/delete-movie-award?movie_award_id=<?= $nominant_item->id ?>" class="btn btn-sm btn-danger" onclick="if (confirm('Точно удалить?')) { return true; } else { return false; }">удалить</a>
                        </td>
                    </tr>
                    <?php } 
                    unset($nominant_item, $movie_nominant_list, $movie); ?>

                    <?php foreach ($person_winner_list as $winner_item) {
                        $person = Person::findOne($winner_item->person_id);
                        $movie = Movie::findOne($winner_item->movie_id);
                    ?>
                    <tr style="background-color: gold;">
                        <td></td>
                        <td>
                            <h2>
                                <a href="/person/details?id=<?= $person->id ?>">
                                    <?= $person->getFullName() ?>
                                </a>
                            </h2>
                        </td>
                        <td>
                            <h2>
                                (<a href="/movie/details?id=<?= $movie->id ?>">
                                    <?= $movie->title ?>
                                </a>)
                            </h2>
                        </td>
                        <td></td>
                        <td>
                            <a href="/award-nomination/delete-person-award?person_award_id=<?= $winner_item->id ?>" class="btn btn-sm btn-danger" onclick="if (confirm('Точно удалить?')) { return true; } else { return false; }">удалить</a>
                        </td>
                    </tr>
                    <?php } 
                    unset($winner_item, $person_winner_list, $person, $movie); ?>

                    <?php foreach ($person_nominant_list as $nominant_item) {
                        $person = Person::findOne($nominant_item->person_id);
                        $movie = Movie::findOne($nominant_item->movie_id);
                    ?>
                    <tr style="background-color: aqua;">
                        <td></td>
                        <td>
                            <h2>
                                <a href="/person/details?id=<?= $person->id ?>">
                                    <?= $person->getFullName() ?>
                                </a>
                            </h2>
                        </td>
                        <td>
                            <h2>
                                (<a href="/movie/details?id=<?= $movie->id ?>">
                                    <?= $movie->title ?>
                                </a>)
                            </h2>
                        </td>
                        <td></td>
                        <td>
                            <a href="/award-nomination/delete-person-award?person_award_id=<?= $nominant_item->id ?>" class="btn btn-sm btn-danger" onclick="if (confirm('Точно удалить?')) { return true; } else { return false; }">удалить</a>
                        </td>
                    </tr>
                    <?php } 
                    unset($nominant_item, $nominant_item, $person, $movie); ?>
                </tbody>
            </table>
        </div>

        <hr />
    <?php }
    unset($year_item);
    ?>

    </div>
</div><!-- award-nomination-edit -->
