<?php

use app\models\image\ImageUploadForm;
use app\models\Series;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\widgets\ImageUploader;

/** @var yii\web\View $this */
/* @var $model Series */
/* @var $imageUploadForm ImageUploadForm */

$this->title = $model->title . ' | Редактирование сериала';
?>
<div class="series-edit">

    <div class="row">
        <div class="series-image-details-container">
            <a href="/series/details?id=<?= $model->id ?>">
                <img class="series-image-details" src="/images/series/<?= $model->image ?>" alt="series image" />
            </a>
        </div>
    </div>

    <div class="row">
        <?php $imageActiveForm = ActiveForm::begin([
            'action' => '/series/update-image?id=' . $model->id,
            'options' => ['enctype' => 'multipart/form-data']
        ]); ?>

        <?= ImageUploader::widget([
            'label' => 'Постер',
            'ImageUploadForm' => $imageUploadForm,
            'ActiveForm' => $imageActiveForm
        ]); ?>

        <div class="form-group">
            <?= Html::submitButton('Обновить картинку', ['class' => 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

    <hr />

    <div class="row">
        <?php $series_form = ActiveForm::begin(); ?>

        <?= $series_form->field($model, 'title') ?>
        <?= $series_form->field($model, 'original_title') ?>
        <?= $series_form->field($model, 'is_favorite')->checkbox() ?>

        <div class="form-group">
            <?= Html::dropDownList('Series[status]', $model->status, Series::getAllStatusesForFilters(), [
                'prompt' => 'Выбрать статус'
            ]); ?>
        </div>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

    <hr />

    <div class="row">
        <a href="/series/delete?id=<?= $model->id ?>" class="btn btn-sm btn-danger" onclick="return confirm('Точно удалить?');">Удалить сериал</a>
    </div>

</div><!-- series-edit -->
