<?php

use yii\helpers\Html;
use app\models\image\Thumb;

/** @var yii\web\View $this */
/* @var $model app\models\Movie */
/* @var $comment_list array */

$this->title = $model->title;

$this->registerCssFile('/css/image-viewer.css', ['depends' => 'app\assets\AppAsset']);
$this->registerJsFile('/js/image-viewer.min.js', ['depends' => 'app\assets\AppAsset']);
?>
<div class="series-details">

    <div class="row">
        <div class="series-image-details-container">
            <img class="series-image-details" src="/images/series/<?= $model->image ?>" alt="series image" />
        </div>
    </div>

    <div class="row">
        <h1><?= $model->title ?></h1>
        <h3><i style="color: grey;">(<?= $model->original_title ?>)</i></h3>
        <p><?= $model->getStatusName() ?></p>
    </div>

    <?php if (!Yii::$app->user->isGuest) { ?>
        <div class="row">
            <a href="/series/edit?id=<?= $model->id ?>" class="btn btn-sm btn-primary">Редактировать</a>
            <a href="<?= $model->getUrlForGoogleSearch() ?>" class="btn btn-sm btn-primary" target="_blank">Искать в google</a>
        </div>
    <?php } ?>

    <hr />

    <div class="row">
        <p><b>Комментарии</b></p>

        <?php foreach ($comment_list as $comment_item) { ?>
            <p class="col-xs-offset-1">
                <i style="color: grey;">
                    <?= $comment_item->created_at ?>
                </i>
            </p>
            <p class="col-xs-offset-1">
                <?= nl2br($comment_item->content) ?>
            </p>
            <?php if ($comment_item->images) { ?>
                <p class="col-xs-offset-1">
                    <?php foreach ($comment_item->images as $image) { ?>
                        <a class="nsbbox" title="Фото из комментариев" href="<?= Thumb::getBig($comment_item, $image) ?>" data-origin="<?= Thumb::getOriginUri($comment_item, $image) ?>">
                            <img class="comment-image" src="<?= Thumb::getMin($comment_item, $image) ?>" alt="comment image" />
                        </a>
                    <?php }
                    unset($image);
                    ?>
                </p>
            <?php } ?>
            <p class="col-xs-offset-1">
                <?php if (!Yii::$app->user->isGuest) { ?>
                    <a href="/comment/edit?id=<?= $comment_item->id ?>" class="btn btn-sm btn-primary">Редактировать</a>
                <?php } ?>
            </p>
            <hr class="col-xs-offset-1" />
        <?php }
        unset($comment_item);
        ?>
    </div>

    <?php if (!Yii::$app->user->isGuest) { ?>
        <div class="row">
            <div>
                <a href="/comment/create?series_id=<?= $model->id ?>" class="btn btn-sm btn-primary">Оставить комментарий</a>
            </div>
        </div>
    <?php } ?>

</div><!-- series-details -->
