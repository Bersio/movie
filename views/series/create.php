<?php

use app\widgets\ImageUploader;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Series $model */
/** @var app\models\image\ImageUploadForm $imageUploadForm */

$this->title = 'Создание сериала';
?>
<div class="series-create">
    <p>
        <a href="/series/create-by-imdb-url">Создание сериала по ссылке IMDB</a>
    </p>

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['autofocus' => true]) ?>
    <?= $form->field($model, 'original_title')->textInput() ?>

    <?= ImageUploader::widget([
        'label' => 'Постер',
        'ImageUploadForm' => $imageUploadForm,
        'ActiveForm' => $form
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
