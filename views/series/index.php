<?php

use yii\db\ActiveQuery;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/** @var yii\web\View $this */
/* @var $series_query ActiveQuery */
/* @var $series_total_count int */
/* @var $status int */
/* @var $status_list array */
/* @var $is_favorite int */
/* @var $sort string */
/* @var $sort_list array */

$this->title = Yii::t('app', 'Series');
?>
<div class="series-index">

    <div class="jumbotron">
        <h1><?= $this->title ?></h1>
        <h2>Всего сериалов: <?= $series_total_count ?></h2>
    </div>

    <div class="body-content">

        <?php ActiveForm::begin([
            'id' => 'queue-filters',
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-2 control-label'],
            ],
        ]); ?>

        <div class="row filter-row">

            <?php if (!Yii::$app->user->isGuest) { ?>
                <div class="col-lg-4">
                    <a href="/series/create" class="btn btn-success">Создать новый сериал</a>
                </div>
            <?php } ?>

            <div class="col-lg-4">
                <?= Html::dropDownList('sort', $sort, $sort_list, [
                    'onchange' => 'this.form.submit()',
                    'prompt' => Yii::t('app', 'Default sorting'),
                    'class' => 'form-control'
                ]); ?>
            </div>

            <div class="col-lg-4 filter-item">
                <?= Html::dropDownList('is_favorite', $is_favorite, ['1' => 'Избранное'], [
                    'onchange' => 'this.form.submit()',
                    'prompt' => [
                        'text' => 'Все сериалы',
                        'options' => [
                            'value' => '0'
                        ],
                    ],
                    'class' => 'form-control'
                ]); ?>
            </div>
        </div>

        <div class="row filter-row">

            <div class="col-lg-4 filter-item">
                <?= Html::dropDownList('status', $status, $status_list, [
                    'onchange' => 'this.form.submit()',
                    'prompt' => [
                        'text' => 'Выбрать статус',
                        'options' => [
                            'value' => '0'
                        ],
                    ],
                    'class' => 'form-control'
                ]); ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

        <hr />

        <div class="row">
            <table class="series-list">
                <tbody>
                <?php foreach ($series_query->each() as $series_item) { ?>
                    <tr>
                        <td>
                            <a href="/series/details?id=<?= $series_item->id ?>">
                                <img class="series-image" src="/images/series/<?= $series_item->image ?>" alt="series image" />
                            </a>
                        </td>
                        <td>
                            <h2>
                                <a href="/series/details?id=<?= $series_item->id ?>">
                                    <?= $series_item->title ?>
                                </a>
                            </h2>
                            <h4><?= $series_item->original_title ?></h4>
                            <div>
                                <?= $series_item->getStatusName() ?>
                            </div>
                        </td>
                        <td>

                        </td>
                    </tr>
                    <tr class="separator">
                        <td colspan="5"></td>
                    </tr>
                <?php }
                unset($series_item); ?>
                </tbody>
            </table>
        </div>

    </div>
</div>
