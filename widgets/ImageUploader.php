<?php

namespace app\widgets;

use Exception;
use Yii;

/**
 * Image uploader widget
 *
 * @author Maksym Krupko
 */
class ImageUploader extends \yii\base\Widget
{
    public $id;
    public $label = 'Image';
    public $ImageUploadForm;
    public $ActiveForm;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        if (empty($this->ActiveForm)) {
            throw new Exception('active form is empty');
        }

        if (empty($this->ImageUploadForm)) {
            throw new Exception('image upload form is empty');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return $this->render('image-uploader', []);
    }
}
