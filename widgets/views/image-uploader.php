<?php

use yii\helpers\Html;

?>
<div class="form-group image-uploader-widget">
    <label class="control-label" for="image-url"><?= Html::encode($this->context->label) ?></label>
    <div class="col-lg-12 image-uploader-widget-inner-wrapper">
        <div class="col-lg-10 image-uploader-widget-left">
            <div id="<?= $this->context->id ?>-image-container-url" class="image-container-url">
                <?= $this->context->ActiveForm
                    ->field(
                        $this->context->ImageUploadForm,
                        !empty($this->context->id) ? "[{$this->context->id}]imageUrl" : 'imageUrl'
                    )
                    ->textInput(['class' => 'form-control'])
                    ->label(false) ?>
                <div class="help-block"></div>
            </div>
            <div id="<?= $this->context->id ?>-image-container-file" class="image-container-file">
                <?= $this->context->ActiveForm
                    ->field(
                        $this->context->ImageUploadForm,
                        !empty($this->context->id) ? "[{$this->context->id}]imageFile" : 'imageFile'
                    )
                    ->fileInput(['class' => 'form-control'])
                    ->label(false) ?>
                <div class="help-block"></div>
            </div>
        </div>
        <div class="col-lg-2 image-uploader-widget-right">
            <?= $this->context->ActiveForm
                    ->field(
                        $this->context->ImageUploadForm,
                        !empty($this->context->id) ? "[{$this->context->id}]imageType" : 'imageType'
                    )
                    ->dropDownList($this->context->ImageUploadForm->typesList, [
                        'class' => 'form-control',
                        'onchange' => "
                            var selected_value = $(event.target).val();
                            if (selected_value == 'url') {
                                $('#{$this->context->id}-image-container-file').hide();
                                $('#{$this->context->id}-image-container-url').show();
                            } else if (selected_value == 'file') {
                                $('#{$this->context->id}-image-container-url').hide();
                                $('#{$this->context->id}-image-container-file').show();
                            }
                        ",
                    ])
                    ->label(false) ?>
        </div>
    </div>
</div>
