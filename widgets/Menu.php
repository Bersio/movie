<?php

namespace app\widgets;

use Yii;

class Menu extends \yii\bootstrap\Nav
{
    public const string MENU_VIEW = 'view';
    public const string MENU_CALENDAR = 'calendar';
    public const string MENU_MOVIE = 'movie';
    public const string MENU_GENRE = 'genre';
    public const string MENU_PERSON = 'person';
    public const string MENU_AWARD = 'award';
    public const string MENU_QUEUE = 'queue';
    public const string MENU_AUDIO = 'audio';
    public const string MENU_THEATER = 'theater';
    public const string MENU_SPECIAL_SHOWING = 'special_showing';
    public const string MENU_DRINK = 'drink';
    public const string MENU_COUNTRY = 'country';
    public const string MENU_SERIES = 'series';
    public const string MENU_OTHER = 'other';

    public static array $menuItems = [];

    public static function setItems(): void
    {
        self::$menuItems = [
            self::MENU_VIEW => ['label' => Yii::t('app', 'Views'), 'url' => ['/view/index']],
            self::MENU_CALENDAR => ['label' => Yii::t('app', 'Calendar'), 'url' => ['/calendar/index']],
            self::MENU_MOVIE => ['label' => Yii::t('app', 'Movies'), 'url' => ['/movie/index']],
            self::MENU_GENRE => ['label' => Yii::t('app', 'Genres'), 'url' => ['/genre/index']],
            self::MENU_PERSON => ['label' => Yii::t('app', 'Persons'), 'url' => ['/person/index']],
            self::MENU_AWARD => ['label' => Yii::t('app', 'Awards'), 'url' => ['/award/index']],
            self::MENU_OTHER => [
                'label' => Yii::t('app', 'Other'),
                'items' => [
                    self::MENU_SERIES => ['label' => Yii::t('app', 'Series'), 'url' => '/series/index'],
                    self::MENU_AUDIO => ['label' => Yii::t('app', 'Audio'), 'url' => '/audio/index'],
                    self::MENU_THEATER => ['label' => Yii::t('app', 'Theaters'), 'url' => '/theater/index'],
                    self::MENU_SPECIAL_SHOWING => ['label' => Yii::t('app', 'Special showing'), 'url' => '/special-showing/index'],
                    self::MENU_DRINK => ['label' => Yii::t('app', 'Drinks'), 'url' => '/drink/index'],
                    self::MENU_COUNTRY => ['label' => Yii::t('app', 'Countries'), 'url' => '/country/index'],
                ],
            ],
            self::MENU_QUEUE => ['label' => Yii::t('app', 'Queue'), 'url' => ['/queue/index']],
        ];
    }

    public static function setActive(string $key): void
    {
        if (isset(self::$menuItems[$key])) {
            self::$menuItems[$key]['active'] = true;
        } elseif (isset(self::$menuItems[self::MENU_OTHER]['items'][$key])) {
            self::$menuItems[self::MENU_OTHER]['active'] = true;
            self::$menuItems[self::MENU_OTHER]['items'][$key]['active'] = true;
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function widget($config = []): string
    {
        $config['items'] = self::$menuItems;

        return parent::widget($config);
    }
}
