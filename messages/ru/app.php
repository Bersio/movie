<?php

return [
    // primitive
    'Yes' => 'Да',
    'No' => 'Нет',

    // menu
    'My movies' => 'Мои фильмы',
    'Views' => 'Просмотры',
    'Calendar' => 'Календарь',
    'Movies' => 'Фильмы',
    'Genres' => 'Жанры',
    'Persons' => 'Персоны',
    'Awards' => 'Награды',
    'Series' => 'Сериалы',
    'Audio' => 'Озвучка',
    'Theaters' => 'Кинотеатры',
    'Special showing' => 'Спец. показы',
    'Drinks' => 'Напитки',
    'Countries' => 'Страны',
    'Queue' => 'Очередь',
    'Other' => 'Разное',

    // sorting
    'Default sorting' => 'Обычная сортировка',
    'By title ↑' => 'По названию ↑',
    'By title ↓' => 'По названию ↓',
    'By number of films watched ↑' => 'По количеству просмотренных фильмов ↑',
    'By number of films watched ↓' => 'По количеству просмотренных фильмов ↓',
    'By total films count ↑' => 'По общему количеству фильмов ↑',
    'By total films count ↓' => 'По общему количеству фильмов ↓',
    'Reverse' => 'Обратная',

    // common
    'Name' => 'Название',
    'Description' => 'Описание',
    'Type' => 'Тип',
    'Created at' => 'Дата создания',
    'Updated at' => 'Дата обновления',
    'Total Movies Count' => 'Всего фильмов',
    'Viewed Movies Count' => 'Просмотрено фильмов',
    'Movie Awards Winner Count' => 'Количество наград',
    'Movie Awards Nomination Count' => 'Количество номинаций',

    // other
    'Award' => 'Награда',
    'Nomination' => 'Номинант',
    'Winner' => 'Победитель',
    'Picture "winner"' => 'Картинка "победитель"',
    'Picture "nominee"' => 'Картинка "номинант"',
    'Film Nominations' => 'Номинация фильмов',
    'Nomination of persons' => 'Номинация персон',
    'Web Site' => 'Сайт',
    'Movie' => 'Фильм',
    'Person' => 'Персона',
    'Series(one)' => 'Сериал',
    'Comment' => 'Комментарий',
    'Photo' => 'Фото',
    'Code' => 'Код',
    'En Name' => 'Название на английском',
    'Ru Name' => 'Название на русском',
    'Alcohol' => 'Алкоголь',
];
