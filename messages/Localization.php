<?php

namespace app\messages;

use Yii;
use yii\web\Cookie;

class Localization
{
    private const string COOKIE_NAME = 'locale';

    private const string LOCALE_UK = 'uk-UA';
    private const string LOCALE_RU = 'ru-RU';
    private const string LOCALE_EN = 'en-US';

    private const array ALLOWED_LOCALES = [self::LOCALE_UK, self::LOCALE_RU, self::LOCALE_EN];

    private const string DEFAULT_LOCALE = self::LOCALE_UK;

    public static function init(): void
    {
        $locale = Yii::$app->request->cookies->getValue(self::COOKIE_NAME, self::DEFAULT_LOCALE);

        self::setLocale($locale);
    }

    public static function setLocale(string $locale): void
    {
        if (!in_array($locale, self::ALLOWED_LOCALES, true)) {
            $locale = self::DEFAULT_LOCALE;
        }

        Yii::$app->language = $locale;

        Yii::$app->response->cookies->add(new Cookie([
            'name' => self::COOKIE_NAME,
            'value' => $locale,
        ]));
    }
}
