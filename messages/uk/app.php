<?php

return [
    // primitive
    'Yes' => 'Так',
    'No' => 'Ні',

    // menu
    'My movies' => 'Мої фільми',
    'Views' => 'Перегляди',
    'Calendar' => 'Календар',
    'Movies' => 'Фільми',
    'Genres' => 'Жанри',
    'Persons' => 'Персони',
    'Awards' => 'Нагороди',
    'Series' => 'Серіали',
    'Audio' => 'Озвучка',
    'Theaters' => 'Кінотеатри',
    'Special showing' => 'Спец. покази',
    'Drinks' => 'Напої',
    'Countries' => 'Країни',
    'Queue' => 'Черга',
    'Other' => 'Різне',

    // sorting
    'Default sorting' => 'Звичайне сортування',
    'By title ↑' => 'За назвою ↑',
    'By title ↓' => 'За назвою ↓',
    'By number of films watched ↑' => 'За кількістю переглянутих фільмів ↑',
    'By number of films watched ↓' => 'За кількістю переглянутих фільмів ↓',
    'By total films count ↑' => 'За загальною кількістю фільмів ↑',
    'By total films count ↓' => 'За загальною кількістю фільмів ↓',
    'Reverse' => 'Зворотнє',

    // common
    'Name' => 'Назва',
    'Description' => 'Опис',
    'Type' => 'Тип',
    'Created at' => 'Дата створення',
    'Updated at' => 'Дата оновлення',
    'Total Movies Count' => 'Всього фільмів',
    'Viewed Movies Count' => 'Кількість переглянутих фільмів',
    'Movie Awards Winner Count' => 'Кількість нагород',
    'Movie Awards Nomination Count' => 'Кількість номінацій',

    // other
    'Award' => 'Нагорода',
    'Nomination' => 'Номінант',
    'Winner' => 'Переможець',
    'Picture "winner"' => 'Зображення "переможець"',
    'Picture "nominee"' => 'Зображення "номінант"',
    'Film Nominations' => 'Номінація фільмів',
    'Nomination of persons' => 'Номінація осіб',
    'Web Site' => 'Сайт',
    'Movie' => 'Фільм',
    'Person' => 'Персона',
    'Series(one)' => 'Серіал',
    'Comment' => 'Коментар',
    'Photo' => 'Фото',
    'Code' => 'Код',
    'En Name' => 'Назва англійською',
    'Ru Name' => 'Назва російською',
    'Alcohol' => 'Алкоголь',
];
