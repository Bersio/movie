<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%series}}`.
 */
class m240803_093145_create_series_table extends Migration
{
    private const string TABLE_NAME_SERIES = '{{%series}}';
    private const string TABLE_NAME_COMMENT = '{{%comment}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp(): void
    {
        $this->createTable(self::TABLE_NAME_SERIES, [
            'id' => $this->primaryKey()->unsigned(),
            'title' => $this->string()->notNull(),
            'original_title' => $this->string(),
            'image' => $this->string(),
            'is_favorite' => $this->boolean()->notNull()->defaultValue(0),
            'status' => $this->tinyInteger(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx_series_unique',
            self::TABLE_NAME_SERIES,
            [
                'title',
                'original_title',
            ],
            true
        );

        $this->addColumn(self::TABLE_NAME_COMMENT, 'series_id', $this->integer()->unsigned()->after('person_id'));
        $this->addForeignKey(
            'fk_comment_series_id',
            self::TABLE_NAME_COMMENT,
            'series_id',
            self::TABLE_NAME_SERIES,
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown(): void
    {
        $this->dropForeignKey('fk_comment_series_id', self::TABLE_NAME_COMMENT);
        $this->dropColumn(self::TABLE_NAME_COMMENT, 'series_id');
        $this->dropTable(self::TABLE_NAME_SERIES);
    }
}
