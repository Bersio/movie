<?php

use yii\db\Migration;

/**
 * Class m231202_093415_add_column_type_to_award_nomination_table
 */
class m231202_093415_add_column_type_to_award_nomination_table extends Migration
{
    private const TABLE_NAME = 'award_nomination';
    private const COLUMN_NAME = 'type';
    
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_NAME, $this->tinyInteger()->after('name'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);
    }
}
