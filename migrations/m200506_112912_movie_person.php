<?php

use yii\db\Migration;

/**
 * Class m200506_112912_movie_person
 */
class m200506_112912_movie_person extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('movie_actor', [
            'id' => $this->primaryKey(),
            'movie_id' => $this->integer()->notNull(),
            'person_id' => $this->integer()->notNull()
        ], 'ENGINE=InnoDB');

        $this->createTable('movie_director', [
            'id' => $this->primaryKey(),
            'movie_id' => $this->integer()->notNull(),
            'person_id' => $this->integer()->notNull()
        ], 'ENGINE=InnoDB');

        $this->createIndex('idx_movieactor_movieid', 'movie_actor', 'movie_id');
        $this->createIndex('idx_movieactor_personid', 'movie_actor', 'person_id');

        $this->createIndex('idx_moviedirector_movieid', 'movie_director', 'movie_id');
        $this->createIndex('idx_moviedirector_personid', 'movie_director', 'person_id');

        $this->addForeignKey('fk_movieactor_movie_id', 'movie_actor', 'movie_id', 'movie', 'id', 'CASCADE');
        $this->addForeignKey('fk_movieactor_person_id', 'movie_actor', 'person_id', 'person', 'id', 'CASCADE');

        $this->addForeignKey('fk_moviedirector_movie_id', 'movie_director', 'movie_id', 'movie', 'id', 'CASCADE');
        $this->addForeignKey('fk_moviedirector_person_id', 'movie_director', 'person_id', 'person', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('movie_actor');
        $this->dropTable('movie_director');
    }
}
