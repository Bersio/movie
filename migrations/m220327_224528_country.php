<?php

use yii\db\Migration;

/**
 * Class m220327_224528_country
 */
class m220327_224528_country extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('country', [
            'id' => $this->primaryKey(),
            'code' => $this->string(2)->notNull()->unique(),
            'en_name' => $this->string()->notNull()->unique(),
            'ru_name' => $this->string()->notNull()->unique(),
            'total_movies_count' => $this->integer()->defaultValue(0),
            'viewed_movies_count' => $this->integer()->defaultValue(0),
        ], 'ENGINE=InnoDB');

        $this->createTable('movie_country', [
            'id' => $this->primaryKey(),
            'movie_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull()
        ], 'ENGINE=InnoDB');

        $this->createIndex('idx_moviecountry_movieid', 'movie_country', 'movie_id');
        $this->createIndex('idx_moviecountry_countryid', 'movie_country', 'country_id');

        $this->addForeignKey('fk_moviecountry_movie_id', 'movie_country', 'movie_id', 'movie', 'id', 'CASCADE');
        $this->addForeignKey('fk_moviecountry_country_id', 'movie_country', 'country_id', 'country', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_moviecountry_country_id', 'movie_country');
        $this->dropForeignKey('fk_moviecountry_movie_id', 'movie_country');

        $this->dropIndex('idx_moviecountry_countryid', 'movie_country');
        $this->dropIndex('idx_moviecountry_movieid', 'movie_country');

        $this->dropTable('movie_country');
        $this->dropTable('country');
    }
}
