<?php

use yii\db\Migration;

/**
 * Class m200506_120305_genre
 */
class m200506_120305_genre extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('genre', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'created_at' => $this->dateTime()->notNull()
        ], 'ENGINE=InnoDB');

        $this->createTable('movie_genre', [
            'id' => $this->primaryKey(),
            'movie_id' => $this->integer()->notNull(),
            'genre_id' => $this->integer()->notNull()
        ], 'ENGINE=InnoDB');

        $this->createIndex('idx_moviegenre_movieid', 'movie_genre', 'movie_id');
        $this->createIndex('idx_moviegenre_genreid', 'movie_genre', 'genre_id');

        $this->addForeignKey('fk_moviegenre_movie_id', 'movie_genre', 'movie_id', 'movie', 'id', 'CASCADE');
        $this->addForeignKey('fk_moviegenre_genre_id', 'movie_genre', 'genre_id', 'genre', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('movie_genre');
        $this->dropTable('genre');
    }
}
