<?php

use yii\db\Migration;

/**
 * Class m200506_121204_queue
 */
class m200506_121204_queue extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('queue', [
            'id' => $this->primaryKey(),
            'movie_id' => $this->integer()->notNull(),
            'created_at' => $this->date()->notNull()
        ], 'ENGINE=InnoDB');

        $this->createIndex('idx_queue_movieid', 'queue', 'movie_id');

        $this->addForeignKey('fk_queue_movie_id', 'queue', 'movie_id', 'movie', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('queue');
    }
}
