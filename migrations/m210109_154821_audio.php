<?php

use yii\db\Migration;

/**
 * Class m210109_154821_audio
 */
class m210109_154821_audio extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('audio', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()
        ], 'ENGINE=InnoDB');

        $this->addColumn('view', 'audio_id', $this->integer());

        $this->createIndex('idx_view_audioid', 'view', 'audio_id');
        $this->addForeignKey('fk_view_audio_id', 'view', 'audio_id', 'audio', 'id', 'SET NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_view_audio_id', 'view');
        $this->dropIndex('idx_view_audioid', 'view');
        $this->dropColumn('view', 'audio_id');
        $this->dropTable('audio');
    }
}
