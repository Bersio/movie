<?php

use yii\db\Migration;

/**
 * Class m220209_152619_person_sex
 */
class m220209_152619_person_sex extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('person', 'sex', $this->integer()->after('birthday'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('person', 'sex');
    }
}
