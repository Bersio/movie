<?php

use yii\db\Migration;

/**
 * Class m231118_075835_special_showing
 */
class m231118_075835_special_showing extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('special_showing', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime(),
            'viewed_movies_count' => $this->integer()->defaultValue(0),
        ], 'ENGINE=InnoDB');

        $this->addColumn('view', 'special_showing_id', $this->integer()->after('theater_id'));

        $this->createIndex('idx_view_specialshowingid', 'view', 'special_showing_id');
        $this->addForeignKey('fk_view_specialshowing_id', 'view', 'special_showing_id', 'special_showing', 'id', 'SET NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_view_specialshowing_id', 'view');
        $this->dropIndex('idx_view_specialshowingid', 'view');
        $this->dropColumn('view', 'special_showing_id');
        $this->dropTable('special_showing');
    }
}
