<?php

use yii\db\Migration;

/**
 * Class m200922_074903_awards
 */
class m200922_074903_awards extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('award', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'image_winner' => $this->string(),
            'image_nomination' => $this->string()
        ], 'ENGINE=InnoDB');

        $this->createTable('award_nomination', [
            'id' => $this->primaryKey(),
            'award_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
        ], 'ENGINE=InnoDB');

        $this->createIndex('idx_awardnomination_awardid', 'award_nomination', 'award_id');
        $this->addForeignKey('fk_awardnomination_award_id', 'award_nomination', 'award_id', 'award', 'id', 'CASCADE');

        $this->createTable('movie_award', [
            'id' => $this->primaryKey(),
            'award_id' => $this->integer()->notNull(),
            'award_nomination_id' => $this->integer()->notNull(),
            'movie_id' => $this->integer()->notNull(),
            'type' => $this->integer()->notNull(),
            'year' => $this->integer()->notNull(),
        ], 'ENGINE=InnoDB');

        $this->createIndex('idx_movieaward_awardid', 'movie_award', 'award_id');
        $this->createIndex('idx_movieaward_awardnominationid', 'movie_award', 'award_nomination_id');
        $this->createIndex('idx_movieaward_movieid', 'movie_award', 'movie_id');

        $this->addForeignKey('fk_movieaward_award_id', 'movie_award', 'award_id', 'award', 'id', 'CASCADE');
        $this->addForeignKey('fk_movieaward_awardnomination_id', 'movie_award', 'award_nomination_id', 'award_nomination', 'id', 'CASCADE');
        $this->addForeignKey('fk_movieaward_movie_id', 'movie_award', 'movie_id', 'movie', 'id', 'CASCADE');

        $this->createTable('person_award', [
            'id' => $this->primaryKey(),
            'award_id' => $this->integer()->notNull(),
            'award_nomination_id' => $this->integer()->notNull(),
            'person_id' => $this->integer()->notNull(),
            'movie_id' => $this->integer(),
            'type' => $this->integer()->notNull(),
            'year' => $this->integer()->notNull(),
        ], 'ENGINE=InnoDB');

        $this->createIndex('idx_personaward_awardid', 'person_award', 'award_id');
        $this->createIndex('idx_personaward_awardnominationid', 'person_award', 'award_nomination_id');
        $this->createIndex('idx_personaward_personid', 'person_award', 'person_id');
        $this->createIndex('idx_personaward_movieid', 'person_award', 'movie_id');

        $this->addForeignKey('fk_personaward_award_id', 'person_award', 'award_id', 'award', 'id', 'CASCADE');
        $this->addForeignKey('fk_personaward_awardnomination_id', 'person_award', 'award_nomination_id', 'award_nomination', 'id', 'CASCADE');
        $this->addForeignKey('fk_personaward_person_id', 'person_award', 'person_id', 'person', 'id', 'CASCADE');
        $this->addForeignKey('fk_personaward_movie_id', 'person_award', 'movie_id', 'movie', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('person_award');
        $this->dropTable('movie_award');
        $this->dropTable('award_nomination');
        $this->dropTable('award');
    }
}
