<?php

use yii\db\Migration;

/**
 * Class m211116_101323_drinks
 */
class m211116_101323_drinks extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('drink', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'alcohol' => $this->boolean()->defaultValue(false)
        ], 'ENGINE=InnoDB');

        $this->addColumn('view', 'drink_id', $this->integer());

        $this->createIndex('idx_view_drinkid', 'view', 'drink_id');
        $this->addForeignKey('fk_view_drink_id', 'view', 'drink_id', 'drink', 'id', 'SET NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_view_drink_id', 'view');
        $this->dropIndex('idx_view_drinkid', 'view');
        $this->dropColumn('view', 'drink_id');
        $this->dropTable('drink');
    }
}
