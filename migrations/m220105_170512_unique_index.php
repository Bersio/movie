<?php

use yii\db\Migration;

/**
 * Class m220105_170512_unique_index
 */
class m220105_170512_unique_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex(
            'idx_audio_unique',
            'audio',
            [
                'name',
            ],
            true
        );

        $this->createIndex(
            'idx_award_unique',
            'award',
            [
                'name',
            ],
            true
        );

        $this->createIndex(
            'idx_awardnomination_unique',
            'award_nomination',
            [
                'award_id',
                'name',
            ],
            true
        );

        $this->createIndex(
            'idx_drink_unique',
            'drink',
            [
                'name',
            ],
            true
        );

        $this->createIndex(
            'idx_genre_unique',
            'genre',
            [
                'title',
            ],
            true
        );

        $this->createIndex(
            'idx_movie_unique',
            'movie',
            [
                'original_title',
                'title',
                'year',
            ],
            true
        );

        $this->createIndex(
            'idx_movieactor_unique',
            'movie_actor',
            [
                'movie_id',
                'person_id',
            ],
            true
        );

        $this->createIndex(
            'idx_movieaward_unique',
            'movie_award',
            [
                'award_id',
                'award_nomination_id',
                'movie_id',
                'type',
                'year',
            ],
            true
        );

        $this->createIndex(
            'idx_moviedirector_unique',
            'movie_director',
            [
                'movie_id',
                'person_id',
            ],
            true
        );

        $this->createIndex(
            'idx_moviegenre_unique',
            'movie_genre',
            [
                'movie_id',
                'genre_id',
            ],
            true
        );

        $this->createIndex(
            'idx_person_unique',
            'person',
            [
                'first_name',
                'last_name',
            ],
            true
        );

        $this->createIndex(
            'idx_personaward_unique',
            'person_award',
            [
                'award_id',
                'award_nomination_id',
                'person_id',
                'movie_id',
                'type',
                'year',
            ],
            true
        );

        $this->createIndex(
            'idx_queue_unique',
            'queue',
            [
                'movie_id',
            ],
            true
        );

        $this->createIndex(
            'idx_theater_unique',
            'theater',
            [
                'name',
            ],
            true
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx_audio_unique', 'audio');
        $this->dropIndex('idx_award_unique', 'award');
        $this->dropIndex('idx_awardnomination_unique', 'award_nomination');
        $this->dropIndex('idx_drink_unique', 'drink');
        $this->dropIndex('idx_genre_unique', 'genre');
        $this->dropIndex('idx_movie_unique', 'movie');
        $this->dropIndex('idx_movieactor_unique', 'movie_actor');
        $this->dropIndex('idx_movieaward_unique', 'movie_award');
        $this->dropIndex('idx_moviedirector_unique', 'movie_director');
        $this->dropIndex('idx_moviegenre_unique', 'movie_genre');
        $this->dropIndex('idx_person_unique', 'person');
        $this->dropIndex('idx_personaward_unique', 'person_award');
        $this->dropIndex('idx_queue_unique', 'queue');
        $this->dropIndex('idx_theater_unique', 'theater');
    }
}
