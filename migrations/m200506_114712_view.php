<?php

use yii\db\Migration;

/**
 * Class m200506_114712_view
 */
class m200506_114712_view extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('view', [
            'id' => $this->primaryKey(),
            'movie_id' => $this->integer()->notNull(),
            'created_at' => $this->date()->notNull()
        ], 'ENGINE=InnoDB');

        $this->createIndex('idx_view_movieid', 'view', 'movie_id');

        $this->addForeignKey('fk_view_movie_id', 'view', 'movie_id', 'movie', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('view');
    }
}
