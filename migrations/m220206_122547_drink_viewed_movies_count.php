<?php

use yii\db\Migration;

/**
 * Class m220206_122547_drink_viewed_movies_count
 */
class m220206_122547_drink_viewed_movies_count extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('audio', 'viewed_movies_count', $this->integer()->defaultValue(0));
        $this->addColumn('drink', 'viewed_movies_count', $this->integer()->defaultValue(0));
        $this->addColumn('theater', 'viewed_movies_count', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('audio', 'viewed_movies_count');
        $this->dropColumn('drink', 'viewed_movies_count');
        $this->dropColumn('theater', 'viewed_movies_count');
    }
}
