<?php

use yii\db\Migration;

/**
 * Class m240608_142556_add_column_is_favorite_to_movie_table
 */
class m240608_142556_add_column_is_favorite_to_movie_table extends Migration
{
    private const string TABLE_NAME = 'movie';
    private const string COLUMN_NAME = 'is_favorite';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_NAME, $this->boolean()->notNull()->defaultValue(0)->after('image'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);
    }
}
