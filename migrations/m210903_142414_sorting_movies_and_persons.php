<?php

use yii\db\Migration;

/**
 * Class m210903_142414_sorting_movies_and_persons
 */
class m210903_142414_sorting_movies_and_persons extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('queue', 'movie_awards_winner_count', $this->integer()->defaultValue(0));
        $this->addColumn('queue', 'movie_awards_nomination_count', $this->integer()->defaultValue(0));

        $this->addColumn('genre', 'total_movies_count', $this->integer()->defaultValue(0));
        $this->addColumn('genre', 'viewed_movies_count', $this->integer()->defaultValue(0));
        $this->addColumn('genre', 'movie_awards_winner_count', $this->integer()->defaultValue(0));
        $this->addColumn('genre', 'movie_awards_nomination_count', $this->integer()->defaultValue(0));

        $this->addColumn('person', 'total_movies_count', $this->integer()->defaultValue(0));
        $this->addColumn('person', 'viewed_movies_count', $this->integer()->defaultValue(0));
        $this->addColumn('person', 'awards_winner_count', $this->integer()->defaultValue(0));
        $this->addColumn('person', 'awards_nomination_count', $this->integer()->defaultValue(0));
        $this->addColumn('person', 'movie_awards_winner_count', $this->integer()->defaultValue(0));
        $this->addColumn('person', 'movie_awards_nomination_count', $this->integer()->defaultValue(0));
        $this->addColumn('person', 'comments_count', $this->integer()->defaultValue(0));

        $this->addColumn('movie', 'awards_winner_count', $this->integer()->defaultValue(0));
        $this->addColumn('movie', 'awards_nomination_count', $this->integer()->defaultValue(0));
        $this->addColumn('movie', 'actors_count', $this->integer()->defaultValue(0));
        $this->addColumn('movie', 'directors_count', $this->integer()->defaultValue(0));
        $this->addColumn('movie', 'person_awards_winner_count', $this->integer()->defaultValue(0));
        $this->addColumn('movie', 'person_awards_nomination_count', $this->integer()->defaultValue(0));
        $this->addColumn('movie', 'views_count', $this->integer()->defaultValue(0));
        $this->addColumn('movie', 'comments_count', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('queue', 'movie_awards_winner_count');
        $this->dropColumn('queue', 'movie_awards_nomination_count');

        $this->dropColumn('genre', 'total_movies_count');
        $this->dropColumn('genre', 'viewed_movies_count');
        $this->dropColumn('genre', 'movie_awards_winner_count');
        $this->dropColumn('genre', 'movie_awards_nomination_count');

        $this->dropColumn('person', 'total_movies_count');
        $this->dropColumn('person', 'viewed_movies_count');
        $this->dropColumn('person', 'awards_winner_count');
        $this->dropColumn('person', 'awards_nomination_count');
        $this->dropColumn('person', 'movie_awards_winner_count');
        $this->dropColumn('person', 'movie_awards_nomination_count');
        $this->dropColumn('person', 'comments_count');

        $this->dropColumn('movie', 'awards_winner_count');
        $this->dropColumn('movie', 'awards_nomination_count');
        $this->dropColumn('movie', 'actors_count');
        $this->dropColumn('movie', 'directors_count');
        $this->dropColumn('movie', 'person_awards_winner_count');
        $this->dropColumn('movie', 'person_awards_nomination_count');
        $this->dropColumn('movie', 'views_count');
        $this->dropColumn('movie', 'comments_count');
    }
}
