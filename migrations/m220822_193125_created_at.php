<?php

use yii\db\Migration;

/**
 * Class m220822_193125_createdat
 */
class m220822_193125_created_at extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('audio', 'created_at', $this->dateTime()->notNull()->after('name'));
        $this->addColumn('audio', 'updated_at', $this->dateTime()->after('created_at'));

        $this->addColumn('award', 'created_at', $this->dateTime()->notNull()->after('web_site'));
        $this->addColumn('award', 'updated_at', $this->dateTime()->after('created_at'));

        $this->addColumn('award_nomination', 'created_at', $this->dateTime()->notNull()->after('name'));
        $this->addColumn('award_nomination', 'updated_at', $this->dateTime()->after('created_at'));

        $this->addColumn('country', 'created_at', $this->dateTime()->notNull()->after('ru_name'));
        $this->addColumn('country', 'updated_at', $this->dateTime()->after('created_at'));

        $this->addColumn('drink', 'created_at', $this->dateTime()->notNull()->after('alcohol'));
        $this->addColumn('drink', 'updated_at', $this->dateTime()->after('created_at'));

        $this->addColumn('genre', 'updated_at', $this->dateTime()->after('created_at'));

        $this->addColumn('movie_actor', 'created_at', $this->dateTime()->notNull()->after('person_id'));
        $this->addColumn('movie_actor', 'updated_at', $this->dateTime()->after('created_at'));

        $this->addColumn('movie_award', 'created_at', $this->dateTime()->notNull()->after('year'));
        $this->addColumn('movie_award', 'updated_at', $this->dateTime()->after('created_at'));

        $this->addColumn('movie_country', 'created_at', $this->dateTime()->notNull()->after('country_id'));
        $this->addColumn('movie_country', 'updated_at', $this->dateTime()->after('created_at'));

        $this->addColumn('movie_director', 'created_at', $this->dateTime()->notNull()->after('person_id'));
        $this->addColumn('movie_director', 'updated_at', $this->dateTime()->after('created_at'));

        $this->addColumn('movie_genre', 'created_at', $this->dateTime()->notNull()->after('genre_id'));
        $this->addColumn('movie_genre', 'updated_at', $this->dateTime()->after('created_at'));

        $this->addColumn('person_award', 'created_at', $this->dateTime()->notNull()->after('year'));
        $this->addColumn('person_award', 'updated_at', $this->dateTime()->after('created_at'));

        $this->addColumn('theater', 'created_at', $this->dateTime()->notNull()->after('web_site'));
        $this->addColumn('theater', 'updated_at', $this->dateTime()->after('created_at'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('audio', 'created_at');
        $this->dropColumn('audio', 'updated_at');

        $this->dropColumn('award', 'created_at');
        $this->dropColumn('award', 'updated_at');

        $this->dropColumn('award_nomination', 'created_at');
        $this->dropColumn('award_nomination', 'updated_at');

        $this->dropColumn('country', 'created_at');
        $this->dropColumn('country', 'updated_at');

        $this->dropColumn('drink', 'created_at');
        $this->dropColumn('drink', 'updated_at');

        $this->dropColumn('genre', 'updated_at');

        $this->dropColumn('movie_actor', 'created_at');
        $this->dropColumn('movie_actor', 'updated_at');

        $this->dropColumn('movie_award', 'created_at');
        $this->dropColumn('movie_award', 'updated_at');

        $this->dropColumn('movie_country', 'created_at');
        $this->dropColumn('movie_country', 'updated_at');

        $this->dropColumn('movie_director', 'created_at');
        $this->dropColumn('movie_director', 'updated_at');

        $this->dropColumn('movie_genre', 'created_at');
        $this->dropColumn('movie_genre', 'updated_at');

        $this->dropColumn('person_award', 'created_at');
        $this->dropColumn('person_award', 'updated_at');

        $this->dropColumn('theater', 'created_at');
        $this->dropColumn('theater', 'updated_at');
    }
}
