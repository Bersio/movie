<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%theater}}`.
 */
class m240513_182633_add_status_column_to_theater_table extends Migration
{
    private const string TABLE_NAME = 'theater';
    private const string COLUMN_NAME = 'status';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(self::TABLE_NAME, self::COLUMN_NAME, $this->tinyInteger()->notNull()->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn(self::TABLE_NAME, self::COLUMN_NAME);
    }
}
