<?php

use yii\db\Migration;

/**
 * Class m200506_115154_comment
 */
class m200506_115154_comment extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('comment', [
            'id' => $this->primaryKey(),
            'movie_id' => $this->integer(),
            'person_id' => $this->integer(),
            'content' => $this->text()->notNull(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()
        ], 'ENGINE=InnoDB');

        $this->createIndex('idx_comment_movieid', 'comment', 'movie_id');
        $this->createIndex('idx_comment_personid', 'comment', 'person_id');

        $this->addForeignKey('fk_comment_movie_id', 'comment', 'movie_id', 'movie', 'id', 'CASCADE');
        $this->addForeignKey('fk_comment_person_id', 'comment', 'person_id', 'person', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('comment');
    }
}
