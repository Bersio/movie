<?php

use yii\db\Migration;

/**
 * Class m200506_111305_movie
 */
class m200506_111305_movie extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('movie', [
            'id' => $this->primaryKey(),
            'original_title' => $this->string(),
            'title' => $this->string()->notNull(),
            'year' => $this->integer(),
            'image' => $this->string(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()
        ], 'ENGINE=InnoDB');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('movie');
    }
}
