<?php

use yii\db\Migration;

/**
 * Class m210101_124726_award_description_site
 */
class m210101_124726_award_description_site extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%award}}', 'description', $this->text());
        $this->addColumn('{{%award}}', 'web_site', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%award}}', 'description');
        $this->dropColumn('{{%award}}', 'web_site');
    }
}
