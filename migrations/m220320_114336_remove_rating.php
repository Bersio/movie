<?php

use yii\db\Migration;
use app\models\Movie;
use app\models\Rating;
use app\models\View;

/**
 * Class m220320_114336_remove_rating
 */
class m220320_114336_remove_rating extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('view', 'rating', $this->integer()->after('movie_id')->notNull()->defaultValue(0));
        $this->addColumn('movie', 'average_rating', $this->float()->after('year'));

        $this->dropForeignKey('fk_rating_movie_id', 'rating');
        $this->dropIndex('idx_rating_movieid', 'rating');
        $this->dropTable('rating');

        $this->dropColumn('queue', 'movie_awards_winner_count');
        $this->dropColumn('queue', 'movie_awards_nomination_count');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('rating', [
            'id' => $this->primaryKey(),
            'movie_id' => $this->integer()->notNull()->unique(),
            'rating' => $this->integer()->notNull(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()
        ], 'ENGINE=InnoDB');

        $this->createIndex('idx_rating_movieid', 'rating', 'movie_id');
        $this->addForeignKey('fk_rating_movie_id', 'rating', 'movie_id', 'movie', 'id', 'CASCADE');

        $this->dropColumn('view', 'rating');
        $this->dropColumn('movie', 'average_rating');

        $this->addColumn('queue', 'movie_awards_winner_count', $this->integer()->defaultValue(0));
        $this->addColumn('queue', 'movie_awards_nomination_count', $this->integer()->defaultValue(0));
    }
}
