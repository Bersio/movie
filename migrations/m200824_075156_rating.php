<?php

use yii\db\Migration;

/**
 * Class m200824_075156_rating
 */
class m200824_075156_rating extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('rating', [
            'id' => $this->primaryKey(),
            'movie_id' => $this->integer()->notNull()->unique(),
            'rating' => $this->integer()->notNull(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()
        ], 'ENGINE=InnoDB');

        $this->createIndex('idx_rating_movieid', 'rating', 'movie_id');
        $this->addForeignKey('fk_rating_movie_id', 'rating', 'movie_id', 'movie', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('rating');
    }
}
