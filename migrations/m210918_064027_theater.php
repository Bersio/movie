<?php

use yii\db\Migration;

/**
 * Class m210918_064027_theater
 */
class m210918_064027_theater extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('theater', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'address' => $this->string(),
            'web_site' => $this->string(),
        ], 'ENGINE=InnoDB');

        $this->addColumn('view', 'theater_id', $this->integer());

        $this->createIndex('idx_view_theaterid', 'view', 'theater_id');
        $this->addForeignKey('fk_view_theater_id', 'view', 'theater_id', 'theater', 'id', 'SET NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_view_theater_id', 'view');
        $this->dropIndex('idx_view_theaterid', 'view');
        $this->dropColumn('view', 'theater_id');
        $this->dropTable('theater');
    }
}
