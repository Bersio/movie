<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%external_info}}`.
 */
class m241215_143425_create_external_info_table extends Migration
{
    private const string TABLE_NAME = '{{%external_info}}';

    private const string MOVIE_FK = 'fk_external_info_movie_id';
    private const string PERSON_FK = 'fk_external_info_person_id';
    private const string SERIES_FK = 'fk_external_info_series_id';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'movie_id' => $this->integer(),
            'person_id' => $this->integer(),
            'series_id' => $this->integer()->unsigned(),
            'imdb_id' => $this->string(),
            'imdb_rating' => $this->float(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()
        ]);

        $this->addForeignKey(
            self::MOVIE_FK,
            self::TABLE_NAME,
            'movie_id',
            'movie',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            self::PERSON_FK,
            self::TABLE_NAME,
            'person_id',
            'person',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            self::SERIES_FK,
            self::TABLE_NAME,
            'series_id',
            'series',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(self::MOVIE_FK, self::TABLE_NAME);
        $this->dropForeignKey(self::PERSON_FK, self::TABLE_NAME);
        $this->dropForeignKey(self::SERIES_FK, self::TABLE_NAME);
        $this->dropTable(self::TABLE_NAME);
    }
}
